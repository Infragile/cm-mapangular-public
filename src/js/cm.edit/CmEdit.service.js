/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
        .module('cm.edit')
        .factory('CmEdit', CmEdit);

CmEdit.$inject = ['CmUtilsGeoJSON', 'CmMapStorage'];

function CmEdit(CmUtilsGeoJSON, CmMapStorage) {

    var editLayer = null;

    return {
        cancel: cancel,
        disable: disable,
        enable: enable,
        reset: reset,
        save: save
    };

    function enable(mapId, geojson) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            hasEditLayer(map);

            geojson.leafletLayer = CmUtilsGeoJSON.toLeafletLayer(geojson.features[0]);
            editLayer.addLayer(geojson.leafletLayer);
            geojson.leafletLayer.enableEdit();
            return geojson;
        });
    }

    function disable(mapId, geojson) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            return save(map, geojson);
        });
    }

    function save(map, geojson) {
        if (!geojson.leafletLayer && !geojson.leafletLayer.editEnabled()) {
            return;
        }
        geojson.leafletLayer.disableEdit();
        return geojson;
    }

    function cancel(mapId, geojson) {
        editLayer.removeLayer(geojson.leafletLayer);
    }

    function reset(mapId, geojson) {
        editLayer.removeLayer(geojson.leafletLayer);
        enable(mapId, geojson);
    }

    function hasEditLayer(map) {
        if (!editLayer) {
            editLayer = new L.geoJson();
            editLayer.addTo(map);
        }
    }
}
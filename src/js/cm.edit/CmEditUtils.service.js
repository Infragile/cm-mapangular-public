/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
    .module('cm.edit')
    .factory('CmEditUtils', CmEditUtils);

CmEditUtils.$inject = [];

function CmEditUtils() {

    return {
        getFeature: getFeature
    };

    function getFeature(obj){
        return obj._layers ? obj._layers[Object.keys(obj._layers)[0]] : obj;
    }
}
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular.module('cm.draw', ['cm.common'])

    .constant('CM_DRAW_STYLES', {
        polygon: {
            clickable: false,
            color: '#ABE67E',
            weight: 2,
            opacity: 0.9,
            fill: false,
            lineCap: 'round',
            lineJoin: 'round',
            radius: 4
        },
        polyline: {
            clickable: false,
            color: '#ABE67E',
            weight: 2,
            opacity: 0.9,
            fill: false,
            lineCap: 'round',
            lineJoin: 'round',
            radius: 4
        }
    });
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular
        .module('cm.draw')
        .factory('CmDraw', CmDraw);

CmDraw.$inject = ['CM_DRAW_STYLES', 'CmMapStorage'];

function CmDraw(CM_DRAW_STYLES, CmMapStorage) {

    return {
        startPolyline: startPolyline,
        savePolyline: savePolyline,
        removePolyline: removePolyline
        // startPolygonDraw: startPolygonDraw,
        // savePolygonDraw: savePolygonDraw,
        // removePolygonDraw: removePolygonDraw
    };

/////////////////////////

    function startPolyline(mapId, opts) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            opts = opts ? opts : CM_DRAW_STYLES.polyline;
            var poly = map.editTools.startPolyline();
            poly.setStyle(opts);
            return poly;
        });
    }

    function savePolyline(poly) {
        poly.editor.disable();
        return poly;
    }

    function removePolyline(mapId, poly) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            return map.removeLayer(poly);
        });
    }

   /* function startPolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }

    function savePolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }

    function removePolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }*/
}

/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomIn', cmZoomIn);

cmZoomIn.$inject = ['CmNavigate'];

function cmZoomIn(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-in.html',
        link: function link(scope) {

            scope.zoomIn = zoomIn;
            
            function zoomIn(){
                CmNavigate.zoomIn(scope.mapId);
            }

        }
    };
    return directive;

}
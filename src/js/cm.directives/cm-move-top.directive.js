/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMoveTop', cmMoveTop);

cmMoveTop.$inject = ['CmNavigate'];

function cmMoveTop(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-top.html',
        link: function link(scope) {

            scope.moveTop = moveTop;

            function moveTop() {
                CmNavigate.move(scope.mapId, 0, -500);
            }

        }
    };
    return directive;

}

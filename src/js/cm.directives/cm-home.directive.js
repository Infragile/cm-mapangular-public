/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmHome', cmHome);

cmHome.$inject = ['CmNavigate'];

function cmHome(CmNavigate) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-home.html',
        link: function link(scope) {

            scope.goHome = goHome;

            function goHome(){
                CmNavigate.home(scope.mapId);
            }

        }
    };
    return directive;

}
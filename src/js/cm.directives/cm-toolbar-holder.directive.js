/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmToolbarHolder', cmToolbarHolder);

cmToolbarHolder.$inject = ['CmMapStorage'];

function cmToolbarHolder(CmMapStorage) {
    
    var directive = {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            mapId: '@'
        },
        template: '<div class="btn-toolbar toolbar" role="toolbar" ng-transclude></div>',
        link: function link(scope, element) {

            CmMapStorage.getMap(scope.mapId).then(function (map) {

                map.on('enterFullscreen', function(){
                    element.css('z-index', 99999999999999);
                });

                map.on('exitFullscreen', function(){
                    element.css('z-index', 0);
                });

            });

        }
    };
    return directive;

}
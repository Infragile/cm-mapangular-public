/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMeasureLine', cmMeasureLine);

cmMeasureLine.$inject = ['CmMeasure', '$filter'];

function cmMeasureLine(CmMeasure, $filter) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-measure-line.html',
        link: function link(scope, element) {

            var mapElement = element.parent().parent().parent();
            var holder = angular.element('<div id="coords-holder"></div>');
            mapElement.append(holder);
            holder.hide();

            scope.lineLength = 0;

            scope.isMeasuring = false;
            scope.tooltipText = "Start measure";

            scope.toggleMeasure = toggleMeasure;

            function toggleMeasure(){
                if(!scope.isMeasuring){

                    holder.show();
                    CmMeasure.startLineMeasure(scope.mapId, lineMeasureCallback);
                    mapElement.on('mousemove', onCursorMove);
                    scope.tooltipText = "Stop measure coords";

                } else {

                    holder.hide();
                    CmMeasure.stopLineMeasure(scope.mapId);
                    scope.tooltipText = "Start measure";
                    mapElement.off('mousemove', onCursorMove);
                    scope.lineLength = 0;
                }
                scope.isMeasuring = !scope.isMeasuring;
            }


            function lineMeasureCallback(distance, unit){
                console.log(distance);
                scope.lineLength = $filter('number')(distance, 3) + " " + unit.code;
            }

            function onCursorMove(e){
                scope.$apply(function() {
                    holder.css({
                        left: ( e.pageX + 35 + "px" ),
                        top: ( e.pageY - 15 + "px" )
                    });
                    var text = scope.lineLength;
                    holder.text(text);
                });
            }

        }
    };
    return directive;

}
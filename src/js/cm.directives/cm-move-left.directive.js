/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMoveLeft', cmMoveLeft);

cmMoveLeft.$inject = ['CmNavigate'];

function cmMoveLeft(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-left.html',
        link: function link(scope) {

            scope.moveLeft = moveLeft;

            function moveLeft(){
                CmNavigate.move(scope.mapId, -500, 0);
            }

        }
    };
    return directive;

}

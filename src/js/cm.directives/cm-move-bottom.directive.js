/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMoveBottom', cmMoveBottom);

cmMoveBottom.$inject = ['CmNavigate'];

function cmMoveBottom(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-bottom.html',
        link: function link(scope) {

            scope.moveBottom = moveBottom;

            function moveBottom() {
                CmNavigate.move(scope.mapId, 0, 500);
            }

        }
    };
    return directive;

}

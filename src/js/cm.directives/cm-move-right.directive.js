/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMoveRight', cmMoveRight);

cmMoveRight.$inject = ['CmNavigate'];

function cmMoveRight(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-right.html',
        link: function link(scope) {

            scope.moveRight = moveRight;

            function moveRight(){
                CmNavigate.move(scope.mapId, 500, 0);
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomOut', cmZoomOut);

cmZoomOut.$inject = ['CmNavigate'];

function cmZoomOut(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-out.html',
        link: function link(scope) {

            scope.zoomOut = zoomOut;

            function zoomOut(){
                CmNavigate.zoomOut(scope.mapId);
            }

        }
    };
    return directive;

}
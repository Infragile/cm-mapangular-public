/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomRectangle', cmZoomRectangle);

cmZoomRectangle.$inject = ['CmNavigate'];

function cmZoomRectangle(CmNavigate) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-rectangle.html',
        link: function link(scope) {

            scope.isRectangularlyZooming = false;
            scope.tooltipText = "Rectangular zoom - start";
            scope.toggleRectangularZoom = toggleRectangularZoom;

            function toggleRectangularZoom(){
                if(!scope.isRectangularlyZooming){
                    CmNavigate.zoomToRectangleStart(scope.mapId);
                    scope.tooltipText = "Rectangular zoom - stop";
                } else {
                    CmNavigate.zoomToRectangleStop(scope.mapId);
                    scope.tooltipText = "Rectangular zoom - start";
                }
                scope.isRectangularlyZooming = !scope.isRectangularlyZooming;
            }

        }
    };
    return directive;

}
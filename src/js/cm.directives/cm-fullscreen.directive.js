/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmFullscreen', cmFullscreen);

cmFullscreen.$inject = ['CmMap'];

function cmFullscreen(CmMap) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-fullscreen.html',
        link: function link(scope) {

            scope.isFullscreen = false;
            scope.tooltipText = "Start fullscreen";
            scope.toggleFullScreen = toggleFullScreen;

            function toggleFullScreen(){
                if(!scope.isFullscreen){
                    scope.tooltipText = "Stop fullscreen";
                } else {
                    scope.tooltipText = "Start fullscreen";
                }

                CmMap.toggleFullscreen(scope.mapId);
                scope.isFullscreen = !scope.isFullscreen;
            }

        }
    };
    return directive;

}

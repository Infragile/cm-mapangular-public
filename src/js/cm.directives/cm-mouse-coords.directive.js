/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMouseCoords', cmMouseCoords);

cmMouseCoords.$inject = ['CmNavigate', '$filter'];

function cmMouseCoords(CmNavigate, $filter) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@',
            crsId: '=?'
        },
        templateUrl: '../src/js/cm.directives/cm-mouse-coords.html',
        link: function link(scope, element) {

            var mapElement = element.parent().parent().parent();
            var holder = angular.element('<div id="coords-holder"></div>');
            mapElement.append(holder);
            holder.hide();

            scope.crsId = scope.crsId || 5514;
            scope.mouseCoords = {};

            scope.isTrackingCoords = false;
            scope.tooltipText = "Show coords";
            scope.toggleMouseCoords = toggleMouseCoords;

            function toggleMouseCoords(){
                if(!scope.isTrackingCoords){

                    holder.show();
                    CmNavigate.enableMouseCoords(scope.mapId, mouseCoordsCallback, scope.crsId);
                    mapElement.on('mousemove', onCursorMove);
                    scope.tooltipText = "Hide coords";

                } else {

                    holder.hide();
                    CmNavigate.disableMouseCoords(scope.mapId);
                    scope.tooltipText = "Show coords";
                    mapElement.off('mousemove', onCursorMove);
                }
                scope.isTrackingCoords = !scope.isTrackingCoords;
            }


            function mouseCoordsCallback(result){
                scope.mouseCoords = result;
            }

            function onCursorMove(e){
                scope.$apply(function() {
                    holder.css({
                        left: ( e.pageX + 35 + "px" ),
                        top: ( e.pageY - 15 + "px" )
                    });
                    var text = 'EPSG:' + scope.mouseCoords.crsId + ": " + $filter('number')(scope.mouseCoords.lat, 8) + " lat; " + $filter('number')(scope.mouseCoords.lng, 8)+ " lon;" ;
                    holder.text(text);
                });
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 11.5.16.
 */


angular
        .module('cm.navigate')
        .factory('CmNavigate', CmNavigate);

CmNavigate.$inject = ['CmMapStorage', 'CmUtilsCrs'];

function CmNavigate(CmMapStorage, CmUtilsCrs) {

    return {
        move: move,
        home: home,
        zoomIn: zoomIn,
        zoomOut: zoomOut,
        zoomToRectangleStart: zoomToRectangleStart,
        zoomToRectangleStop: zoomToRectangleStop,
        enableMouseCoords: enableMouseCoords,
        disableMouseCoords: disableMouseCoords
    };

    //////////////////////////////////////////

    function move(mapId, x, y) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.panBy([x, y]);
        });
    }

    function home(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.setView(map.options.initCenter, map.options.initZoom);
        });
    }

    function zoomIn(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.zoomIn();
        });
    }

    function zoomOut(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.zoomOut();
        });
    }

    function zoomToRectangleStart(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.dragging.disable();
            map.boxZoom.addHooks();
            map.on('mousedown', _zoomRectMouseDown);
            // map.boxZoom._container.style.cursor = 'zoom-in';
        });
    }

    function zoomToRectangleStop(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.boxZoom._container.style.cursor = '';
            map.off('mousedown', _zoomRectMouseDown);
            map.dragging.enable();
            map.boxZoom.removeHooks();
        });
    }

    function enableMouseCoords(mapId, callback, crsId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            if (crsId && typeof crsId !== 'number') {
                throw new Error('Invalid crsId provided.');
            }

            var mapCrsId = 3857;
            crsId = crsId ? crsId : mapCrsId;
            var crsTo = CmUtilsCrs.getCrs(crsId);

            var result = {};

            map.on('mousemove', function (evt) {
                var coords = [evt.latlng.lat, evt.latlng.lng];

                if (mapCrsId !== crsId) {
                    coords = crsTo.project(evt.latlng);
                    result.lat = coords.y;
                    result.lng = coords.x;
                } else {
                    result.lat = evt.latlng.lat;
                    result.lng = evt.latlng.lng;
                }

                // 5514 example: X: -902225, y: -992104
                result.crsId = crsId;
                callback.call(this, result, evt);
            });
        });
    }

    function disableMouseCoords(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.off('mousemove');
        });
    }
    
    ///////////////////////////////////////////////////////////////////////////
    
    function _zoomRectMouseDown(event) {
        if (event.target instanceof L.Map) {
            var map = event.target;
            map.boxZoom._onMouseDown.call(map.boxZoom, {clientX: event.originalEvent.clientX, clientY: event.originalEvent.clientY, which: 1, shiftKey: true});
            // ?? map.boxZoom._container.style.cursor = 'zoom-in';
        }
    }
}

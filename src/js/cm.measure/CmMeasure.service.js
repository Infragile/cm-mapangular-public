/**
 * Created by ondrejzvara on 16.5.16.
 */

angular
        .module('cm.measure')
        .factory('CmMeasure', CmMeasure);

CmMeasure.$inject = ['CmDraw', 'CM_MAP_UNIT', 'CmMapStorage'];

function CmMeasure(CmDraw, CM_MAP_UNIT, CmMapStorage) {

    var poly;

    return {
        startLineMeasure: startLineMeasure,
        stopLineMeasure: stopLineMeasure
    };

    function startLineMeasure(mapId, callback, unitCode) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            var unit = unitCode ? CM_MAP_UNIT[unitCode] : CM_MAP_UNIT.m;

            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            CmDraw.startPolyline(mapId).then(function(polyline){
                poly = polyline;
            });

            map.on('editable:drawing:click', onMeasuremenetEnabled);
            map.on('editable:vertex:drag', onMeasuremenetEnabled);
            map.on('editable:vertex:deleted', onMeasuremenetEnabled);


            function onMeasuremenetEnabled(evt) {
                var polyline = evt.layer.toGeoJSON();
                var measurement = turf.lineDistance(polyline, CM_MAP_UNIT.km.name);

                if (unit.code === 'm') {
                    measurement = measurement * 1000;
                }

                callback.call(this, measurement, unit, evt);
            }
        });
    }

    function stopLineMeasure(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

            map.off('editable:drawing:click');
            map.off('editable:vertex:drag');
            map.off('editable:vertex:deleted');
            CmDraw.removePolyline(mapId, poly);

        });
    }
}
/**
 * Created by ondrejzvara on 16.5.16.
 */

angular.module('cm.measure', ['cm.common', 'cm.draw'])

    .constant('CM_MAP_UNIT', {
        km: {
            code: 'km',
            name: 'kilometers'
        },
        m: {
            code: 'm',
            name: 'meters'
        }
    });
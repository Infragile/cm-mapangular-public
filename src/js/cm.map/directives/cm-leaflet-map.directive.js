angular
        .module('cm.map')
        .directive('cmLeafletMap', cmLeafletMap);

cmLeafletMap.$inject = ['CmLeafletMap', '$log', 'CmMap'];

function cmLeafletMap(CmLeafletMap, $log, CmMap) {
    var directive = {
        restrict: 'E',
        scope: {
            options: '=',
            config: '='
        },
        link: function (scope, element) {
            
            try {
                var options = scope.options,
                    config = scope.config,
                    mapElement = element[0],
                    mapId = options.id;

                mapElement.setAttribute('id', mapId);
                CmLeafletMap.setOptionsCrs(options);

                var map = new L.Map(mapElement, options);
                
                map.invalidateSize();
                map.whenReady(function () {
                    CmLeafletMap.enableLoadingWatch(mapId);
                    CmLeafletMap.initLayers(mapId, config);
                    CmLeafletMap.setBounds(mapId, options);
                    CmLeafletMap.setInitMapPos(map);
                    CmLeafletMap.setFullscreen(map, options);
                    CmLeafletMap.storeMap(map, mapId);
                    CmLeafletMap.storeOptions(options, mapId);
                    CmMap.setPositionListener(mapId, options);
                });


                // map.on('zoomend', function() {
                //     MapLayerVisibility.checkLayersVisibility(scope.config, map);
                // });

            } catch(e){
                $log.error(e);
            }

            scope.$on('$destroy', function() {
                map.remove();
                element.remove();
                CmLeafletMap.resetMap(mapId);
                CmLeafletMap.resetOptions(mapId);
                CmLeafletMap.resetLayers(mapId);
            });
        }
    };
    
    return directive;
}

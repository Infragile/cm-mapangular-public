angular
    .module('cm.map')
    .factory('CmLayer', CmLayer);

CmLayer.$inject = ['CmLayersStorage', 'CmLayerFactory', 'CmMapStorage'];

function CmLayer(CmLayersStorage, CmLayerFactory, CmMapStorage) {

    /**
     * Service CmLayer
     * @namespace CmLayer
     */
    return {
        addLayerToMap: addLayerToMap,
        addLayerGroupToMap : addLayerGroupToMap,
        forEachLayer: forEachLayer,
        initLayersForMap: initLayersForMap,
        removeLayerFromMap: removeLayerFromMap,
        removeLayerGroupFromMap: removeLayerGroupFromMap
    };

    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * Creates Leaflet layer from given configuration, adds it to map and
     * saves it to layers storage. Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerConf
     * @param {type} layerId
     * @returns {unresolved}
     */
    function addLayerToMap(mapId, layerConf, layerId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayerFactory.createLayer(layerConf, layerId);

            CmMapStorage.getMap(mapId).then(function(map) {
                layerLeaflet.addTo(map);
                
                // adds layer to immutable layer storage
                layers = CmLayersStorage.addLayerToLayers(layers, layerLeaflet);
                CmLayersStorage.setLayers(layers, mapId);
            });
        });
    }
    
    /**
     * Creates Leaflet layers group from given configuration, adds it to map and
     * saves all layers from group to layers storage. Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerGroupConfArray
     * @param {type} groupId
     * @returns {unresolved}
     */
    function addLayerGroupToMap(mapId, layerGroupConfArray, groupId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            if(_.isArray(layerGroupConfArray)) {
                var layerLeafletArray = [];
                
                _.forEach(layerGroupConfArray, function(layer) {
                    if(layer.type && layer.type === 'layer' && layer.id && layer.layer) {
                        var layerLeaflet = CmLayerFactory.createLayer(layer.layer, layer.id);
                        layerLeafletArray.push(layerLeaflet);
                    }
                });
                
                var layerLeafletGroup = CmLayerFactory.createLayerGroup(layerLeafletArray, groupId);
                
                CmMapStorage.getMap(mapId).then(function(map) {
                    layerLeafletGroup.addTo(map);
                    
                    // adds layer group to immutable layer storage
                    layers = CmLayersStorage.addLayerGroupToLayers(layers, layerLeafletGroup);
                    CmLayersStorage.setLayers(layers, mapId);
                });
            }
        });
    }
    
    /**
     * Applies func for every layer.
     * @memberof CmLayer
     * @param layersStorage
     * @param func
     */
    function forEachLayer(layersStorage, func) {
        if(_.isArray(layersStorage)) {
            _.forEach(layersStorage, function(value) {
                if(value instanceof L.LayerGroup) {
                    value.eachLayer(function(layer) {
                        func(layer);
                    });
                } else {
                    func(value);
                }
            });
        }
    }
    
    /**
     * Recursively adds new layers or layers groups to map from given 
     * configuration. Recursion keeps the promise chain.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerConfiguration
     * @returns {undefined}
     */
    function initLayersForMap(mapId, layerConfiguration) {
        if(_.isArray(layerConfiguration) && layerConfiguration.length > 0) {
            var layerConf = _.head(layerConfiguration);
            layerConfiguration = _.tail(layerConfiguration);

            if(layerConf.type && layerConf.type === 'layer' && layerConf.id && layerConf.layer) {
                addLayerToMap(mapId, layerConf.layer, layerConf.id).then(function() {
                    initLayersForMap(mapId, layerConfiguration);
                });
            } else if (layerConf.type && layerConf.type === 'layerGroup' && layerConf.id && layerConf.layers) {
                addLayerGroupToMap(mapId, layerConf.layers, layerConf.id).then(function() {
                    initLayersForMap(mapId, layerConfiguration);
                });
            }
        }
    }
    
    /**
     * Removes layer from map and layer storage by given layer id. 
     * Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerId
     * @returns {unresolved}
     */
    function removeLayerFromMap(mapId, layerId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayersStorage.getLayerById(layers, layerId);
            
            CmMapStorage.getMap(mapId).then(function(map) {
                if(map.hasLayer(layerLeaflet)) {
                    map.removeLayer(layerLeaflet);
                    
                    layers = CmLayersStorage.removeLayerFromLayers(layers, layerId);
                    CmLayersStorage.setLayers(layers, mapId);
                }
            });
        });
    }
    
    /**
     * Removes layer group from map and layer storage by given group id.
     * Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerGroupId
     * @returns {unresolved}
     */
    function removeLayerGroupFromMap(mapId, layerGroupId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayersStorage.getLayerById(layers, layerGroupId);
            
            CmMapStorage.getMap(mapId).then(function(map) {
                if(map.hasLayer(layerLeaflet)) {
                    map.removeLayer(layerLeaflet);
                    
                    layers = CmLayersStorage.removeLayerGroupFromLayers(layers, layerGroupId);
                    CmLayersStorage.setLayers(layers, mapId);
                }
            });
        });
    }
}
/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
    .module('cm.map')
    .factory('CmMap', CmMap);

CmMap.$inject = ['CmMapStorage', 'CmLayersStorage', 'CmLayer', 'CM_MAP_LAYER_TYPE'];

function CmMap(CmMapStorage, CmLayersStorage, CmLayer, CM_MAP_LAYER_TYPE) {

    /**
     * Service CmMap
     * @namespace CmMap
     */
    return {
        addPositionListener: addPositionListener,
        getMap: getMap,
        toggleFullscreen: toggleFullscreen,
        fitBounds: fitBounds,
        setPositionListener: setPositionListener,
        enableLoadingWatch: enableLoadingWatch
    };

    //////////////////////////////////////////
    
    /**
     * Registers listener called with center positions and zoom every move end.
     * @memberof CmMap
     * @param {String} mapId
     * @param {function} callback
     */
    function addPositionListener(mapId, callback) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            map.on('moveend', function (event) {
                if (event && event.target) {
                    var mapTarget = event.target,
                            center = mapTarget.getCenter(),
                            zoom = mapTarget.getZoom();

                    callback.call(this, center.lat, center.lng, zoom);
                }
            });
        });
    }
    
    function getMap(mapId) {
        return CmMapStorage.getMap(mapId);
    }
    
    function fitBounds(mapId, bounds){
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.fitBounds(bounds);
        });
    }

    /**
     * If positionListener option is setted, registers position listener.
     * @memberof CmMap
     * @param {String} mapId
     * @param {object} opts
     */
    function setPositionListener(mapId, opts) {
        if (opts.positionListener) {
            addPositionListener(mapId, opts.positionListener);
        }
    }

    function toggleFullscreen(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.fullscreenControl.toggleFullScreen(map);
        });
    }

    function enableLoadingWatch(mapId){
       /* return CmMapStorage.getMap(mapId).then(function(map) {

            map.on('layeradd', function (e) {

                if (e.layer.loading) console.log('start');

                if (typeof e.layer.on != 'function') return;

                e.layer.on('data:loading', function () { console.log('start'); });
                e.layer.on('data:loaded',  function () { console.log('stop'); });

            });


        });*/

        return CmLayersStorage.getLayers(mapId).then(function(config){

            var fnc = function(conf){

                if (conf.type === CM_MAP_LAYER_TYPE.wms) {


                    //var layer = config.layerObj;
                    // console.log(layer)
                    // console.log(layer)


                }


            };

            CmLayer.forEachLayer(config, fnc);

        });

       

        // this.on('layerremove', function (e) {
            // if (e.layer.loading) this.spin(false);
            // if (typeof e.layer.on != 'function') return;
            // e.layer.off('data:loaded');
            // e.layer.off('data:loading');
        // }, this);

    }

   
}

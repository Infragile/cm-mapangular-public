/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.map')
    .factory('CmLeafletMap', CmLeafletMap);

CmLeafletMap.$inject = ['CmUtilsCrs', 'CmMapStorage', 'CmOptionsStorage', 'CmLayersStorage', 'CmLayer', 'CmFullscreen', 'CmMap'];

function CmLeafletMap(CmUtilsCrs, CmMapStorage, CmOptionsStorage, CmLayersStorage, CmLayer, CmFullscreen, CmMap) {

    return {
        setOptionsCrs: setOptionsCrs,
        setFullscreen: setFullscreen,
        setBounds: setBounds,
        setInitMapPos: setInitMapPos,
        storeMap: storeMap,
        resetMap: resetMap,
        storeOptions: storeOptions,
        resetOptions: resetOptions,
        resetLayers: resetLayers,
        initLayers: initLayers,
        enableLoadingWatch: enableLoadingWatch
    };

    //////////////////////////////////////////

    function setOptionsCrs(options){
      if (options['srid']) {
          var crs = CmUtilsCrs.getCrs(options['srid']);
          if (crs === null) {
              throw new Error('CRS with srid ' + options['srid'] + ' not defined.');
          }
          options['crs'] = crs;
      }
    }

    function setFullscreen(map, opts) {
        if (opts.fullscreen) {
            map.fullscreenControl = CmFullscreen.getFullscreenControl({});
        }
    }

    function setBounds(mapId, opts) {
        if (opts.bounds && (opts.bounds instanceof L.LatLngBounds)) {
            CmMap.fitBounds(mapId, opts.bounds);
        }
    }

    function setInitMapPos(map) {
        map.options.initCenter = _.cloneDeep(map.getCenter());
        map.options.initZoom = _.cloneDeep(map.getZoom());
    }

    function storeMap(map, mapId) {
        CmMapStorage.setMap(map, mapId);
    }

    function resetMap(mapId) {
        CmMapStorage.resetMap(mapId);
    }

    function storeOptions(opts, mapId) {
        CmOptionsStorage.setOptions(opts, mapId);
    }

    function resetOptions(mapId) {
        CmOptionsStorage.resetOptions(mapId);
    }

    function resetLayers(mapId) {
        CmLayersStorage.resetLayers(mapId);
    }

    function initLayers(mapId, config) {
        CmLayersStorage.setLayers({}, mapId);
        CmLayer.initLayersForMap(mapId, config);
    }
    
    function enableLoadingWatch(mapId){
        CmMap.enableLoadingWatch(mapId);
    }
    

}

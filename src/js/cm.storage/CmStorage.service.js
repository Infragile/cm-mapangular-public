/**
 * Created by ondrejzvara on 26.5.16.
 */

angular
    .module('cm.storage')
    .factory('CmStorage', CmStorage);

CmStorage.$inject = ['CmMapStorage', 'CmLayersStorage', 'CmOptionsStorage'];

function CmStorage(CmMapStorage, CmLayersStorage, CmOptionsStorage) {

    return {
        getMap: getMap,
        getLayers: getLayers,
        getOptions: getOptions
    };

    //////////////////////////////////////////

    function getMap(mapId) {
        return CmMapStorage.getMap(mapId);
    }

    function getLayers(mapId) {
        return CmLayersStorage.getLayers(mapId);
    }

    function getOptions(mapId) {
        return CmOptionsStorage.getOptions(mapId);
    }
}
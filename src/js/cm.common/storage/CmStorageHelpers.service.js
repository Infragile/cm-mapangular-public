/**
 * Created by ondrejzvara on 10.5.16.
 */
angular
    .module('cm.common.storage')
    .factory('CmStorageHelpers', CmStorageHelpers);

CmStorageHelpers.$inject = ['$q'];

function CmStorageHelpers($q) {

    return {
        getDefer: getDefer,
        getUnresolvedDefer: getUnresolvedDefer,
        setResolvedDefer: setResolvedDefer
    };

/////////////////////////

    function getDefer(obj, id) {
        var defer;
        if (_.isEmpty(obj[id]) || obj[id].resolvedDefer === false) {
            defer = getUnresolvedDefer(obj, id);
        } else {
            defer = obj[id].defer;
        }
        return defer;
    }

    function getUnresolvedDefer(obj, id) {
        var defer;
        if (_.isEmpty(obj[id]) || obj[id].resolvedDefer === true) {
            defer = $q.defer();
            obj[id] = {
                defer: defer,
                resolvedDefer: false
            };
        } else {
            defer = obj[id].defer;
        }
        return defer;
    }

    function setResolvedDefer(obj, id) {
        obj[id].resolvedDefer = true;
    }

}


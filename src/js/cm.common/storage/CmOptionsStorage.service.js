/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.storage')
    .factory('CmOptionsStorage', CmOptionsStorage);

CmOptionsStorage.$inject = ['CmStorageHelpers'];

function CmOptionsStorage(CmStorageHelpers) {

    var opts = {};

    var service = {
        setOptions: setOptions,
        getOptions: getOptions,
        resetOptions: resetOptions
    };
    return service;

    /////////////////////////

    function setOptions(o, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(opts, id);
        defer.resolve(o);
        CmStorageHelpers.setResolvedDefer(opts, id);
    }
    
    function getOptions(mapId){
        return _getOptions(mapId).then(function (opts) {
            return _.cloneDeep(opts);
        });
    }

    function resetOptions(id){
        opts[id] = undefined;
    }
    
    ///////////////////////// PRIVATE ////////////////////////////

    function _getOptions(id) {
        var defer = CmStorageHelpers.getDefer(opts, id);
        return defer.promise;
    }

}
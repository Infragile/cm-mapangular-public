angular
        .module('cm.common.storage')
        .factory('CmLayersStorage', CmLayersStorage);

CmLayersStorage.$inject = ['CmStorageHelpers'];

function CmLayersStorage(CmStorageHelpers) {

    var layers = {};

    /**
     * Service CmLayersStorage
     * @namespace CmLayersStorage
     */
    var service = {
        addLayerToLayers: addLayerToLayers,
        addLayerGroupToLayers: addLayerGroupToLayers,
        getLayers: getLayers,
        getLayerById: getLayerById,
        removeLayerFromLayers: removeLayerFromLayers,
        removeLayerGroupFromLayers: removeLayerGroupFromLayers,
        resetLayers: resetLayers,
        setLayers: setLayers
    };
    
    return service;

    /////////////////////////
    
    /**
     * Adds Leaflet layer to given layers variable. Returns layers variable.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} leafletLayer
     * @returns {Array}
     */
    function addLayerToLayers(layers, leafletLayer) {
        if(!_.isArray(layers)) {
            layers = [];
        }
        
        layers.push(leafletLayer);
        
        return layers;
    }
    
    /**
     * Adds Leaflet group layer to given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} arrayOfLeafletLayers
     * @returns {Array}
     */
    function addLayerGroupToLayers(layers, arrayOfLeafletLayers) {
        return addLayerToLayers(layers, arrayOfLeafletLayers);
    }

    /**
     * Returns deep copy of stored layers.
     * @memberof CmLayersStorage
     * @param {type} mapId
     * @returns {unresolved}
     */
    function getLayers(mapId) {
        return _getLayers(mapId).then(function (layers) {
            return _.cloneDeep(layers);
        });
    }
    
    /**
     * Returns Leaflet layer object with given id.
     * @memberof CmLayersStorage
     * @param {Array} layers
     * @param {type} id
     * @returns {mixed}
     */
    function getLayerById(layers, id) {
        var layer;
        
        if(_.isArray(layers) && id) {
            _.forEach(layers, function(value) {
                if(value._id === id) {
                    layer = value;
                }
                
                if(value instanceof L.LayerGroup) {
                    value.eachLayer(function(l) {
                        if(l._id === id) {
                            layer = l;
                        }
                    });
                }
            });
        }
        
        return layer;
    }
    
    /**
     * Removes Leaflet layer from given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} layerId
     * @returns {unresolved}
     */
    function removeLayerFromLayers(layers, layerId) {
        if(_.isArray(layers) && layerId) {
            _.forEach(layers, function(value) {
                if(value instanceof L.LayerGroup) {
                    _.remove(value.getLayers(), function(v) {
                        return (v._id && v._id === layerId);
                    });
                }
            });
            
            _.remove(layers, function(v) {
                return (v._id && v._id === layerId);
            });
        }
        
        return layers;
    }
    
    /**
     * Removes Leaflet group layer from given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} layerGroupId
     * @returns {unresolved}
     */
    function removeLayerGroupFromLayers(layers, layerGroupId) {
        if(_.isArray(layers) && layerGroupId) {
            _.remove(layers, function(v) {
                return (v._id && v._id === layerGroupId);
            });
        }
        
        return layers;
    }
    
    /**
     * Resets layer storage for given map id.
     * @memberof CmLayersStorage
     * @param {type} mapId
     * @returns {undefined}
     */
    function resetLayers(mapId) {
        layers[mapId] = [];
    }
    
    /**
     * Sets layers to get layers promise.
     * @memberof CmLayersStorage
     * @param {type} c
     * @param {type} id
     * @returns {undefined}
     */
    function setLayers(c, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(layers, id);
        defer.resolve(c);
        CmStorageHelpers.setResolvedDefer(layers, id);
    }

    ///////////////////////// PRIVATE ////////////////////////////

    function _getLayers(id) {
        var defer = CmStorageHelpers.getDefer(layers, id);
        return defer.promise;
    }
}
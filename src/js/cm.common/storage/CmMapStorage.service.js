/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.storage')
    .factory('CmMapStorage', CmMapStorage);

CmMapStorage.$inject = ['CmStorageHelpers'];

function CmMapStorage(CmStorageHelpers) {

    var map = {};

    var service = {
        setMap: setMap,
        getMap: getMap,
        resetMap: resetMap
    };
    return service;

    /////////////////////////

    function setMap(m, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(map ,id);
        defer.resolve(m);
        CmStorageHelpers.setResolvedDefer(map, id);
    }

    function getMap(id) {
        var defer = CmStorageHelpers.getDefer(map, id);
        return defer.promise;
    }

    function resetMap(id){
        map[id] = undefined;
    }

}
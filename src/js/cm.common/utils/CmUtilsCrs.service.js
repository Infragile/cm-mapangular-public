/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.utils')
    .factory('CmUtilsCrs', CmUtilsCrs);

CmUtilsCrs.$inject = [];

function CmUtilsCrs() {

    var crsDefs = {
        5514: "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813975277778 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel" +
        "+units=m +towgs84=570.8,85.7,462.8,4.998,1.587,5.261,3.56"
    };

    registerProjection(5514);

    return {
        getCrs: getCrs
    };

    //////////////////////////////////////////

    function registerProjection(projId){
        try{
            proj4('urn:ogc:def:crs:EPSG::' + projId);
        } catch(e){
            proj4.defs('urn:ogc:def:crs:EPSG::' + projId, crsDefs[projId]);
        }
    }
    
    function getCrs(srid){
        switch (srid) {
            case 3857:
                return L.CRS.EPSG3857;
            case 4326:
                return L.CRS.EPSG4326;
            case 3395:
                return L.CRS.EPSG3395;
            case 5514:
                // aligned to settings of CMProxy
                var epsg5514 = new L.Proj.CRS('EPSG:5514', crsDefs[5514], {
                        resolutions: [ 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25, 0.125 ],
                        origin: [-925000, -920000],
                        bounds: L.bounds(
                            L.point(-920000, -1449288),
                            L.point(-400712, -925000))
                    });
                return epsg5514;
            default:
                return null;
        }
    }


}
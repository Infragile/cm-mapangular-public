/**
 * Created by ondrejzvara on 12.5.16.
 */

angular
    .module('cm.common.utils')
    .factory('CmUtilsGeoJSON', CmUtilsGeoJSON);

CmUtilsGeoJSON.$inject = [];

function CmUtilsGeoJSON() {

    return {
        toLeafletLayer: toLeafletLayer,
        getFeature: getFeature
    };

    //////////////////////////////////////////

    function toLeafletLayer(geojson){
        return L.GeoJSON.geometryToLayer(geojson);
    }

    function getFeature(layer){
        return layer._layers ? layer._layers[Object.keys(layer._layers)[0]] : layer;
    }
    

}
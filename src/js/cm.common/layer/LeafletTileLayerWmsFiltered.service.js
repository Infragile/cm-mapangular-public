angular
        .module('cm.common.layer')
        .factory('LeafletTileLayerWmsFiltered', LeafletTileLayerWmsFiltered);

LeafletTileLayerWmsFiltered.$inject = ['$http', 'OgcCommon', 'LeafletWmsCommon'];

/**
 * WMS layer extended with WFS-style filtering capabilities
 */
function LeafletTileLayerWmsFiltered($http, OgcCommon, LeafletWmsCommon) {
    var service = {
        getLayer: getLayer
    };

    var WmsTileLayer = L.TileLayer.WMS.extend({
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this._id = options.id;
            this.wmsParams = L.extend(options.wms);

            if (options.filter) {
                this.wmsParams['filter'] = OgcCommon.getFilter(options.filter, true);
            }

            this._httpService = $http;

            L.TileLayer.WMS.prototype.initialize.call(this, url, this.wmsParams);

            if (options.hasOwnProperty('removeWmsParams')) {
                for (var i = 0; i < options['removeWmsParams'].length; i++) {
                    this.removeWmsParam(options['removeWmsParams'][i]);
                }
            }
        },
        
        loadFeatureData: function (latlng) {
            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this.wmsParams['layers'], this.wmsParams['filter'], this.wmsParams['feature_count']).then(function (response) {
                return response;
            });
        },
        
        removeWmsParam: function (param) {
            if (this.wmsParams.hasOwnProperty(param)) {
                delete this.wmsParams[param];
            }
        }
    });

    return service;

    function getLayer(url, options) {
        return new WmsTileLayer(url, options);
    }
}
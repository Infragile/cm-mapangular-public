/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.common.layer', [])

    .constant('CM_MAP_LAYER_TYPE', {
        node: 'node',
        wms: 'wms',
        wfs: 'wfs'
    })

    .constant('CM_MAP_POPUP_TYPE', {
        none: 'none',
        static: 'static',
        dynamic: 'dynamic'
    });

angular
    .module('cm.common.layer')
    .factory('CmLayerFactory', CmLayerFactory);

CmLayerFactory.$inject = [
    'CM_MAP_LAYER_TYPE', 'LeafletProjGeoJsonWfs', 'LeafletTileLayerWfs', 
    'LeafletTileLayerWmsFiltered', 'LeafletLayerGroup'
];

function CmLayerFactory(
        CM_MAP_LAYER_TYPE, LeafletProjGeoJsonWfs, LeafletTileLayerWfs, 
        LeafletTileLayerWmsFiltered, LeafletLayerGroup
) {

    return {
        createLayer: createLayer,
        createLayerGroup: createLayerGroup
    };

    ///////////////////////////////////////////////////////////////////////////
    
    // create layer from configuration
    function createLayer(config, id) {
        switch (config['type']) {
            case CM_MAP_LAYER_TYPE.wms:
                config.id = id;
                return createWmsLayer(config);

            case CM_MAP_LAYER_TYPE.wfs:
                config.id = id;
                return createGeoJsonWfsLayer(config);

            case 'wfs-tiled':
                config.id = id;
                return createTileWfsLayer(config);

            default:
                throw new Error('Unknown layer type ' + config.type);
        }
    }
    
    /**
     * TODO check id param !!!
     */
    function createLayerGroup(layers, id) {
        return LeafletLayerGroup.getLayerGroup(layers, id);
    }
    
    ///////////////////////////////////////////////////////////////////////////

    // create GeoJSON layer
    function createGeoJsonWfsLayer(layerConfig) {
        return LeafletProjGeoJsonWfs.getLayer(
            layerConfig['url'],
            layerConfig);
    }

    // create tileds WFS layer
    function createTileWfsLayer(layerConfig) {
        return LeafletTileLayerWfs.getLayer(
            layerConfig['url'],
            layerConfig);
    }
    
    // create WMS layer
    function createWmsLayer(layerConfig) {
        return LeafletTileLayerWmsFiltered.getLayer(
                layerConfig['url'],
                layerConfig);
    }
}

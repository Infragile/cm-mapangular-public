angular
        .module('cm.common.layer')
        .factory('LeafletWmsCommon', LeafletWmsCommon);

LeafletWmsCommon.$inject = [];

function LeafletWmsCommon() {
    return {
        loadFeatureData: loadFeatureData
    };

    function loadFeatureData(latlng, url, uppercase, httpservice, map, crs, layers, filter, featureCount) {
        // get location of features in pixels, map size and displayed map bounds
        // note: we can't use map.getBounds() - it doesn't returns bounds
        // of visible area
        var featurePoint = map.latLngToContainerPoint(latlng);
        var mapSize = map.getSize();
        var nwPoint = [0, 0];
        var sePoint = [mapSize.x, mapSize.y];
        var nwLatlng = map.containerPointToLatLng(nwPoint);
        var seLatlng = map.containerPointToLatLng(sePoint);
        var nw = crs.project(nwLatlng);
        var se = crs.project(seLatlng);
        // TODO this._wmsVersion ??
        var bbox = (this._wmsVersion >= 1.3 && crs === L.CRS.EPSG4326 ?
                [se.y, nw.x, nw.y, se.x] :
                [nw.x, se.y, se.x, nw.y]).join(',');

        var reqParams = [];

        reqParams['service'] = 'WMS';
        reqParams['version'] = '1.1.1';
        reqParams['request'] = 'GetFeatureInfo';
        reqParams['info_format'] = 'text/javascript';
        reqParams['x'] = featurePoint.x;
        reqParams['y'] = featurePoint.y;
        reqParams['srs'] = crs.code;
        reqParams['bbox'] = bbox;
        reqParams['width'] = mapSize.x;
        reqParams['height'] = mapSize.y;
        reqParams['layers'] = layers;
        reqParams['query_layers'] = layers;
        reqParams['feature_count'] = (featureCount ? featureCount : 1);
        if (filter) {
            reqParams['filter'] = filter;
        }

        var reqUrl = url + L.Util.getParamString(reqParams, url, uppercase) +
                (uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + "callback:JSON_CALLBACK";

        return httpservice.jsonp(reqUrl);
    }
}
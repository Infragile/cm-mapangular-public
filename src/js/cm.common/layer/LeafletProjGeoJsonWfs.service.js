angular
        .module('cm.common.layer')
        .factory('LeafletProjGeoJsonWfs', LeafletProjGeoJsonWfs);

LeafletProjGeoJsonWfs.$inject = ['$http', 'LeafletWmsCommon', 'OgcCommon'];

/**
 * GeoJSON WFS layer
 *
 * Based on code from CAN by Martin Tesar
 * Modified by Petr Sobeslavsky to work with L.Proj.GeoJSON - GeoJSON layer with custom CRS support
 */
function LeafletProjGeoJsonWfs($http, LeafletWmsCommon, OgcCommon) {
    var service = {
        getLayer: getLayer
    };

    var ProjGeoJsonWfs = L.Proj.GeoJSON.extend({
        defaultWfsParams: {
            service: 'WFS',
            request: 'GetFeature',
            version: '2.0.0',
            typeNames: '',
            outputFormat: 'text/javascript',
            count: 100
        },
        defaultWfsFormatParams: {
            callback: 'JSON_CALLBACK'
        },
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this.state = {
                dataBounds: null,
                newBounds: null,
                zoom: null,
                newZoom: null,
                featuresFetched: 0,
                limitReached: false,
                currentDownloadChain: 0
            };

            this._id = options.id;
            this._url = url;
            this._wfsParams = L.extend(this.defaultWfsParams, options.wfs);
            this._filter = options.filter;
            this._wfsFormatParams = L.extend(this.defaultWfsFormatParams, options.format);
            this._geometryColumn = options.geometryColumn || 'geom';
            this._limit = options.limit || 2000;
            this._httpService = $http;
            this._chunked = false;
            this._unbounded = false;

            if (options.unbounded) {
                this._unbounded = true;
            }

            if (options.chunked) {
                // only set startIndex if we are downloading data in chunks
                // it requires layers to provide natural ordering, so we cannot set it by default
                // for all layers
                this._wfsParams.startIndex = 0;
                this._chunked = true;
            }

            if (options.dataCallback) {
                this._dataCallback = options.dataCallback;
            }

            L.GeoJSON.prototype.initialize.call(this, null, options.geojson);
        },
        loadFeatureData: function (latlng) {
            var filter = null;
            if (this._filter) {
                filter = OgcCommon.getFilter(this._filter, true);
            }

            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this._wfsParams['typeNames'], filter, this._wfsParams['feature_count']);
        },
        onAdd: function (map) {
            L.GeoJSON.prototype.onAdd.call(this, map);

            this._crs = this.options.crs || map.options.crs;
            this._uppercase = this.options.uppercase || false;

            this._map = map;
            map.on({
                moveend: this._updateDisplayedMap
            }, this);
            this._updateDisplayedMap();
        },
        _updateDisplayedMap: function () {
            if (!this._map) {
                return;
            }
            var bounds = this._map.getBounds();
            var zoom = this._map.getZoom();

            this.update(bounds, zoom);
        },
        getExtendedBounds: function (bounds) {
            // extend bounds by 10 % in all directions
            var sw = bounds.getSouthWest();
            var ne = bounds.getNorthEast();
            var latSize = ne.lat - sw.lat;
            var lngSize = ne.lng - sw.lng;
            ne.lat = ne.lat + (latSize * 0.05);
            sw.lat = sw.lat - (latSize * 0.05);
            ne.lng = ne.lng + (lngSize * 0.05);
            sw.lng = sw.lng - (lngSize * 0.05);
            var extendedBounds = L.latLngBounds(sw, ne);
            return extendedBounds;
        },
        update: function (viewBounds, newZoom) {
            if (!this._map) {
                return;
            }
            if (this._needsUpdate(viewBounds, newZoom)) {
                this.state.newBounds = viewBounds;
                this.state.newZoom = newZoom;
                this._getFeatures(viewBounds);
            }
        },
        refresh: function() {
            this._getFeatures(this.state.newBounds);
        },
        _getFormatString: function (obj) {
            var params = [];
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    params.push(i + ':' + obj[i]);
                }
            }
            return params.join(';');
        },
        _getRequestUrl: function (_wfsParams, bounds) {
            var extendedBounds = this.getExtendedBounds(bounds);
            var nw = this._crs.project(extendedBounds.getNorthWest());
            var se = this._crs.project(extendedBounds.getSouthEast());
            var bbox = nw.x + ',' + se.y + ' ' + se.x + ',' + nw.y;

            // we need to supply BBOX as part of filter, because we cannot specify both bbox and filter
            var requestFilter;

            if (!this._unbounded) {
                var bboxFilter = {
                    type: 'BBOX',
                    propertyName: this._geometryColumn,
                    coordinates: bbox
                };

                if (this._filter) {
                    requestFilter = {
                        type: 'And',
                        arg1: bboxFilter,
                        arg2: this._filter
                    };
                } else {
                    requestFilter = bboxFilter;
                }
            } else {
                requestFilter = this._filter;
            }
            _wfsParams['filter'] = OgcCommon.getFilter(requestFilter, true);

            // prepare format params
            var fo = this._getFormatString(this._wfsFormatParams);

            return this._url +
                    L.Util.getParamString(_wfsParams, this._url, this._uppercase) +
                    (this._uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + fo;
        },
        _getFeatures: function (bounds) {
            var _wfsParams = _.cloneDeep(this._wfsParams);

            this.state.currentDownloadChain += 1;
            var downloadChain = this.state.currentDownloadChain;

            this.state.featuresFetched = 0;
            this.state.limitReached = false;
            this._fetchFeatures(_wfsParams, bounds, downloadChain);
        },
        _fetchFeatures: function (_wfsParams, bounds, downloadChain) {
            var url = this._getRequestUrl(_wfsParams, bounds);

            var _this = this;
            this._httpService.jsonp(url)
                    .success(function (data) {
                        _this._FeaturesFetchedCallback(data, _wfsParams, bounds, downloadChain);
                    })
                    .error(function () {
                        _this._updateDisplayedMapFailCallback();
                    });
        },
        _FeaturesFetchedCallback: function (data, _wfsParams, bounds, downloadChain) {
            // if we received data from an old call, discard them
            if (downloadChain < this.state.currentDownloadChain) {
                return;
            }

            if (this.state.featuresFetched === 0) {
                // delete old data upon reception of the first batch of new data
                this.clearLayers();
            }

            this.state.featuresFetched += data.features.length;
            //this.addData(data);

            if (this._dataCallback) {
                this._dataCallback(data);
            }

            if (this.state.featuresFetched >= data.totalFeatures) {
                this._updateDisplayedMapDoneFinish(data);
                return;
            }

            if (this.state.featuresFetched >= this._limit) {
                this.state.limitReached = true;
                this._updateDisplayedMapDoneFinish(data);
                return;
            }

            if (this._chunked) {
                _wfsParams.startIndex += _wfsParams.count;
                this._fetchFeatures(_wfsParams, bounds, downloadChain);
            }
        },
        _updateDisplayedMapFailCallback: function () {

        },
        _updateDisplayedMapDoneFinish: function () {
            this.state.dataBounds = this.state.newBounds;
            this.state.zoom = this.state.newZoom;
        },
        _needsUpdate: function (viewBounds, newZoom) {
            var oldBounds = this.state.dataBounds;
            var oldZoom = this.state.zoom;

            var needs = false;
            if ((newZoom > oldZoom) && this.state.limitReached) {
                needs = true;
            }
            if (!(oldBounds && oldBounds.contains(viewBounds))) {
                needs = true;
            }

            return needs;
        }
    });

    return service;

    function getLayer(url, options) {
        return new ProjGeoJsonWfs(url, options);
    }
}
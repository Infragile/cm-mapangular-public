angular
    .module('cm.common.layer')
    .factory('LeafletLayerGroup', LeafletLayerGroup);

LeafletLayerGroup.$inject = [];

/**
 * Extension of LeafletGroup, added id attribute.
 * @returns {LeafletLayerGroup.service}
 */
function LeafletLayerGroup() {
    var service = {
        getLayerGroup: getLayerGroup
    };

    var LayerGroup = L.LayerGroup.extend({
        initialize: function (layers, id) {
            if(!id) {
                throw new Error("Missing layer group id option in initialization.");
            }
            
            this._id = id;
            
            L.LayerGroup.prototype.initialize.call(this, layers);
        }
    });

    return service;

    function getLayerGroup(layers, id) {
        return new LayerGroup(layers, id);
    }
}
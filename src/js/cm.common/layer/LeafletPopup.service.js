angular
    .module('cm.common.layer')
    .factory('LeafletPopup', LeafletPopup);

LeafletPopup.$inject = ['$compile', '$timeout'];

/**
 * Leaflet popup extend with ability to compile angular template
 */
function LeafletPopup($compile, $timeout) {
    var service = {
        get: getPopup
    };

    var Popup = L.Popup.extend({
        options: {
            scope: null
        },
        onAdd: function (map) {
            // create an empty element a jQuery object
            var contentElement = angular.element('<div/>');

            // read Angular template code with structured HTML to jQuery object
            contentElement.append(this._content);

            // compile Angular template and apply on scope
            $compile(contentElement)(this.options.scope);

            // convert jQuery object to DOM object and stock it
            this._content = contentElement[0];

            // apply scope
            var scope = this.options.scope;
            $timeout(function () {
                scope.$apply();
            });

            L.Popup.prototype.onAdd.call(this, map);
        }
    });

    return service;

    function getPopup(options, source) {
        return new Popup(options, source);
    }
}
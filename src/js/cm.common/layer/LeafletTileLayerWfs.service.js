angular
        .module('cm.common.layer')
        .factory('LeafletTileLayerWfs', LeafletTileLayerWfs);

LeafletTileLayerWfs.$inject = ['$http', 'OgcCommon', 'LeafletWmsCommon'];

function LeafletTileLayerWfs($http, OgcCommon, LeafletWmsCommon) {
    var service = {
        getLayer: getLayer
    };

    var WfsTileLayer = L.TileLayer.extend({
        defaultWfsParams: {
            service: 'WFS',
            request: 'GetFeature',
            version: '2.0.0',
            typeNames: '',
            outputFormat: 'text/javascript',
            startIndex: 0,
            count: 100
        },
        defaultWfsFormatParams: {
            callback: 'JSON_CALLBACK'
        },
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this._id = options.id;
            this._url = url;
            this._wfsParams = L.extend(this.defaultWfsParams, options.wfs);
            this._filter = options.filter;
            this._geoJson = options.geojson;
            this._wfsFormatParams = L.extend(this.defaultWfsFormatParams, options.format);
            this._geometryColumn = options.geometryColumn || 'geom';
            this._idColumn = options.idColumn || 'id';
            this._limit = options.limit || 2000;

            L.setOptions(this, options);
            this._httpService = $http;
            this._features = {};
        },
        onAdd: function (map) {
            this._crs = this.options.crs || map.options.crs;

            L.TileLayer.prototype.onAdd.call(this, map);
        },
        _getFormatString: function (obj) {
            var params = [];
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    params.push(i + ':' + obj[i]);
                }
            }
            return params.join(';');
        },
        _getRequestUrl: function (_wfsParams, coords) {
            // we need to supply BBOX as part of filter, because we cannot specify both bbox and filter
            var nw = coords.getNorthWest();
            var ne = coords.getNorthEast();
            var se = coords.getSouthEast();
            var sw = coords.getSouthWest();

            var polygon = nw.lng + ',' + nw.lat + ' '
                    + ne.lng + ',' + ne.lat + ' '
                    + se.lng + ',' + se.lat + ' '
                    + sw.lng + ',' + sw.lat + ' '
                    + nw.lng + ',' + nw.lat;

            var bboxFilter = {
                type: 'XML',
                value: '<Intersects><PropertyName>' + this._geometryColumn + '</PropertyName>'
                        + '<gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>'
                        + polygon
                        + '</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></Intersects>'
            };
            var requestFilter;

            if (this._filter) {
                requestFilter = {
                    type: 'And',
                    arg1: bboxFilter,
                    arg2: this._filter
                };
            } else {
                requestFilter = bboxFilter;
            }
            _wfsParams['filter'] = OgcCommon.getFilter(requestFilter, true);

            // prepare format params
            var fo = this._getFormatString(this._wfsFormatParams);

            return this._url +
                    L.Util.getParamString(_wfsParams, this._url, this._uppercase) +
                    (this._uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + fo;
        },
        getTileUrl: function (tilePoint) {
            var map = this._map;
            var tileSize = this.options.tileSize;
            var nwPoint = tilePoint.multiplyBy(tileSize);
            var sePoint = nwPoint.add([tileSize, tileSize]);
            var nw = this._crs.project(map.unproject(nwPoint, tilePoint.z));
            var se = this._crs.project(map.unproject(sePoint, tilePoint.z));
            var sw = L.latLng(nw.y, se.x);
            var ne = L.latLng(se.y, nw.x);
            var coords = L.latLngBounds(sw, ne);

            var _wfsParams = _.cloneDeep(this._wfsParams);

            var url = this._getRequestUrl(_wfsParams, coords);

            return url;
        },
        _loadTile: function (tile, tilePoint) {
            var url = this.getTileUrl(tilePoint);

            var _this = this;
            this._httpService.jsonp(url)
                    .success(function (data) {
                        //                    var featureLayer = L.Proj.geoJson(data, this._geoJson);
                        //                            tile.addLayer(featureLayer);
                        //                    tile.addData(data);
                        // TODO: refactor to separate function
                        for (var i = 0; i < data.features.length; i++) {
                            // TODO: replace id with idColumn
                            if (!(data.features[i].id in _this._features)) {
                                _this._features[data.features[i].id] = true;
                                // add crs to feature data
                                if ("crs" in data) {
                                    data.features[i].crs = data.crs;
                                }
                                var featureLayer = L.Proj.geoJson(data.features[i], _this._geoJson);
                                tile.addLayer(featureLayer);
                            }
                        }

                        // TODO: record feature id in tile
                    });

            this.fire('tileloadstart', {
                tile: tile,
                url: url
            });
        },
        _addTile: function (tilePoint) {
            // create new tile
            // skip _getTail and _createTile since we don't reuse tiles
            var tile = L.layerGroup(); //L.Proj.geoJson([], this._geoJson);

            this._tiles[tilePoint.x + ':' + tilePoint.y] = tile;

            this._loadTile(tile, tilePoint);

            this._map.addLayer(tile);
        },
        loadFeatureData: function (latlng) {
            var filter = null;
            if (this._filter) {
                filter = OgcCommon.getFilter(this._filter, true);
            }

            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this._wfsParams['typeNames'], filter, this._wfsParams['feature_count']);
        }
    });

    return service;

    function getLayer(url, options) {
        return new WfsTileLayer(url, options);
    }
}
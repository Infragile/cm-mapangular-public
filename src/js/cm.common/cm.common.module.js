/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.common', [
    'cm.common.storage',
    'cm.common.layer',
    'cm.common.utils'
]);
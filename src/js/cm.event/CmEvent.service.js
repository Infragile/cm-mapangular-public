/**
 * Created on 12.11.15.
 */

angular
    .module('cm.event')
    .factory('CmEvent', CmEvent);

CmEvent.$inject = [ 'CM_MAP_LAYER_TYPE', 'CmLayersStorage', 'CmMapStorage' ];

function CmEvent( CM_MAP_LAYER_TYPE, CmLayersStorage, CmMapStorage ) {

    return {
        queryLayer: queryLayer,
        removeClickEvent: removeClickEvent
    };

    /////////////////////////


    function queryLayer(mapId, layerId, callback) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            CmLayersStorage.getLayers(mapId).then(function(conf) {
                
                var layer = CmLayersStorage.getLayerById(conf, layerId);

                if(!layer){
                    throw new Error('Layre with ID: ' + layerId + ' not found.');
                }

                if(typeof callback !== 'function') {
                    throw new Error('No callback function provided.');
                }

                map.off('click');
                map.on('click', function(evt) {
                    layer.loadFeatureData(evt.latlng).then(function(data){
                        callback.call(this, data, evt);
                    });
                });
            });
        });
    }

    function removeClickEvent(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.off('click');
        });
    }

}

/************************************************* common *******************************/
/*


function registerMapevt() {
    Map.getMap(config.id).then(function(map) {
        map.on('click', function(e) {
            var options = MapOptions.getMapOptions(config.id);
            onMapClick(e, options, config, map);
        });
    });
}

function onMapClick(evt, options, config, map) {
    var scope = $rootScope.$new();

    // TODO remove if it's not necessary
    _.assignIn(scope, {
        options: options,
        config: config
    });

    if (options['popup'] === CM_MAP_POPUP_TYPE.static) {

        // display static popup
        LeafletPopup.get({scope: scope}, this)
            .setLatLng(evt.latlng)
            .setContent(options['popupTemplate'])
            .openOn(map);
    }

    if (options['popup'] === CM_MAP_POPUP_TYPE.dynamic) {
        // display dynamic popup
        // prepare template
        var tplComponents = [];
        var index = 0;

        scope.featureDataLoaded = [];
        scope.featureData = [];

        var someDataLoaded = false;

        var popup = LeafletPopup.get({scope: scope}, this);
        popup.setLatLng(evt.latlng);

        var highestprimaryId = MapprimaryVisibility.getHighestPriorityprimaryId(config);

        // iterate through primarys
        MapConfig.forEachprimary(config, function (primary) {
            if ((primary.type === CM_MAP_primary_TYPE.wms) || (primary.type === CM_MAP_primary_TYPE.wfs)) {

                if (primary.onprimaryClick && primary.visible) {
                    if(options['loadingSpin']) {
                        MapSpinner.initSpinner(config.id, map.getContainer());
                    }

                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            primary.onprimaryClick(evt, data.data.features, primary);
                        }
                        if(options['loadingSpin']) {
                            MapSpinner.stop(config.id);
                        }
                    });
                }

                if ((primary.featureSummaryTemplate) && primary.visible) {
                    var newTplComponent = '<div ng-show="featureDataLoaded[' + index + ']" ng-repeat="feature in featureData[' + index + ']">'
                        + primary.featureSummaryTemplate.replace(new RegExp('feature', 'g'), "feature.properties")
                        + '</div>';
                    tplComponents.push(newTplComponent);

                    var primaryIndex = index;
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            scope.featureDataLoaded[primaryIndex] = true;
                            scope.featureData[primaryIndex] = data.data.features;

                            if (!someDataLoaded) {
                                popup.openOn(map);
                                someDataLoaded = true;
                            }
                        }
                    });

                    index++;
                }

                if (primary.infoboxTemplate && primary.visible &&
                    ((highestprimaryId > -1 && primary.infoboxTemplatePriority === highestprimaryId) ||
                    highestprimaryId === -1)
                ) {
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        scope.$root.$broadcast(CM_MAP_evt.openInfobox, {
                            type: primary.infoboxTemplate,
                            features: data.data.features
                        });
                    });
                }
            }
        });

        tplComponents.reverse();
        var template = options['popupTemplate'] + tplComponents.join("") + "{{featureData[" + index + "].parcis}}";

        popup.setContent(template);
    }
}

*/

/*
    //TODO: logic below belongs to different services

    // temporary disable popup
    var disablePopup = function () {
        options['popup-origvalue'] = options['popup'];
        options['popup'] = CM_MAP_POPUP_TYPE.none;
    };

    // reenable popup
    var enablePopup = function () {
        options['popup'] = options['popup-origvalue'];
    };




    map.on('draw:drawstart', disablePopup);
    map.on('edit:editstart', disablePopup);
    map.on('delete:deletestart', disablePopup);

    map.on('draw:drawstop', enablePopup);
    map.on('edit:editstop', enablePopup);
    map.on('delete:deletestop', enablePopup);
*/

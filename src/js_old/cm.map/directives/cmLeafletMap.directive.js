angular
        .module('cm.map')
        .directive('cmLeafletMap', CmLeafletMap);

CmLeafletMap.$inject = ['Map', 'MapOptions', 'MapConfig',  'MapLayer', 'MapLayerVisibility'];

function CmLeafletMap(Map, MapOptions, MapConfig, MapLayer, MapLayerVisibility) {
    return {
        scope: {
            options: '=',
            config: '='
        },
        restrict: 'E',
        link: link
    };

    function link(scope, element) {

        Map.registerProjection(5514);

        var options = MapOptions.initMapOptions(scope.options);
        if (options['srid']) {
            // get CRS object
            var crs = Map.getCRS(scope.options['srid']);
            if (crs === null) {
                throw new Error('CRS with srid ' + scope.options['srid'] + ' not defined.');
            }
            options['crs'] = crs;
        }

        // init map
        var mapElement = element[0],
            mapId = scope.config.id;
        mapElement.setAttribute('id', mapId);

        // after map options initialization
        // save map options to service
        MapOptions.setMapOptions(mapId, options);

        var map = new L.Map(mapElement, options);

        if(scope.config.bounds && scope.config.bounds.coordinates){
            scope.config.bounds.crs = {
                "type": "name",
                    "properties": {
                    "name": "urn:ogc:def:crs:EPSG::5514"
                }
            };
            var geoJsonLayer = new L.Proj.geoJson();
            geoJsonLayer.addData(scope.config.bounds);
            var bounds = geoJsonLayer.getBounds();
            map.fitBounds(bounds);
        }

        //stock map object in shareable service
        map.whenReady(function() {
            Map.setMap(map, mapId);
        });

         // create top layer (if it's of type group, it will recursively load other layers)
         var topLayer = MapLayer.createLayer(scope.config);
         scope.config.layerObj = topLayer;
         topLayer.addTo(map);

         MapConfig.setConfig(scope.config, mapId);

         map.on('zoomend', function() {
            MapLayerVisibility.checkLayersVisibility(scope.config, map);
         });

        // init controls
        if (options['controls'] && options['controls']['drawControl']) {
            var drawControl = new L.Control.Draw(options['controls']['drawControl']);
            map.addControl(drawControl);
        }

        // broadcast some map events
        var broadcastEventNames = ['moveend'];
        broadcastEventNames.forEach(function (eventName) {
            map.on(eventName, Map.dispatchMapEvent(scope, eventName));
        });

        scope.$on('$destroy', function() {
            map.remove();
            Map.resetMap(scope.config.id);
        });
    }
}

/**
 * Created on 12.11.15.
 */

angular.module('cm.map', ['cm.common'])


    .constant('CM_MAP_EVENT', {
        updateAll: 'cm-map-update-all',
        updateNode: 'cm-map-update-node',
        openInfobox: 'cm-map-open-infobox'
    })

    .constant('CM_MAP_EDIT_CHANNEL', {
        layerToEdit: 'cm-map-layer-to-edit',
        availableActions: 'cm-map-available-actions',
        loadedFeaturesToEdit: 'cm-map-loaded-to-edit',
        uiEvents: 'cm-map-ui-events'
    })

    .constant('CM_MAP_EDIT_ACTION_TYPE', {
        drawPolygon: 'cm-map-action-type-draw-polygon',
        editPolygon: 'cm-map-action-type-edit-polygon',
        mergePolygons: 'cm-map-action-type-merge-polygons',
        splitPolygon: 'cm-map-action-type-split-polygon',
        removeFeature: 'cm-map-action-type-remove-feature'
    })

    .constant('CM_MAP_EDIT_EVENT_TYPE', {
        start: 'cm-map-event-type-start',
        save: 'cm-map-event-type-save',
        storno: 'cm-map-event-type-storno',
        reset: 'cm-map-event-type-reset'
    })

    .constant('CM_MAP_LAYER_TYPE', {
        node: 'node',
        wms: 'wms',
        wfs: 'wfs'
    })

    .constant('CM_MAP_POPUP_TYPE', {
        none: 'none',
        static: 'static',
        dynamic: 'dynamic'
    });

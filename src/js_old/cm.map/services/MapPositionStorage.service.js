angular
    .module('cm.map')
    .factory('MapPositionStorage', MapPositionStorage);

MapPositionStorage.$inject = ['sessionStorage'];

function MapPositionStorage(sessionStorage) {
    
    return {
        getSavedPosition: getSavedPosition,
        resetSavedPosition: resetSavedPosition,
        savePosition: savePosition
    };
    
    function getSavedPosition() {
        var lat = parseFloat(sessionStorage.cmLeafletMapLat),
                lon = parseFloat(sessionStorage.cmLeafletMapLon),
                zoom = parseFloat(sessionStorage.cmLeafletMapZoom);
        if (isNaN(lat) || isNaN(lon) || isNaN(zoom)) {
            // some value was undefined
            return;
        }
        if (angular.isNumber(lat) && angular.isNumber(lon) && angular.isNumber(zoom)) {
            return {
                center: L.latLng(lat, lon),
                zoom: zoom
            };
        }
    }
    
    function resetSavedPosition() {
        sessionStorage.cmLeafletMapLat = undefined;
        sessionStorage.cmLeafletMapLon = undefined;
        sessionStorage.cmLeafletMapZoom = undefined;
    }
    
    function savePosition(center, zoom) {
        sessionStorage.cmLeafletMapLat = center.lat;
        sessionStorage.cmLeafletMapLon = center.lng;
        sessionStorage.cmLeafletMapZoom = zoom;
    }
}
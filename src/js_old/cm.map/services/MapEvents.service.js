/**
 * Created on 12.11.15.
 */

angular
    .module('cm.map')
    .factory('MapEvents', MapEvents);

MapEvents.$inject = [
    'CM_MAP_LAYER_TYPE',
    'Map'
];

function MapEvents(
    CM_MAP_LAYER_TYPE,
    Map
) {

    var config;
    var unsubscribers = [];

    return {
        init: init,
        removeAllMapEvents: removeAllMapEvents,
        getLocationData: getLocationData,
        enableGetFeatureInfo: enableGetFeatureInfo
    };

/////////////////////////

    function init(conf){
        config = conf;
    }

    //primary and secondary layers to query
    function getLocationData(layer, callback) {
        return Map.getMap(config.id).then(function(map) {
            map.off('click');
            map.on('click', function(evt) {
                queryLayer(layer, evt).then(callback);
            });
        });
    }

    function queryLayer(layer, evt){
        if ((layer.type === CM_MAP_LAYER_TYPE.wms) || (layer.type === CM_MAP_LAYER_TYPE.wfs)) {
            if (layer.visible && layer.queryable) {
                return layer.layerObj.loadFeatureData(evt.latlng);
            }
        }
    }

    function removeAllMapEvents() {
        Map.getMap(config.id).then(function(map) {
            map.closePopup();
            map.off('click');
        });

        _.each(unsubscribers, function(unsubscribe){
            unsubscribe();
        });
        unsubscribers = [];
    }

    function enableGetFeatureInfo(primary, secondary, callback){

        Map.getMap(config.id).then(function(map) {

            map.on('click', function(evt) {
                if ((primary.type === CM_MAP_LAYER_TYPE.wms) || (primary.type === CM_MAP_LAYER_TYPE.wfs)) {
                    return primary.layerObj.loadFeatureData(evt.latlng).then(function(primaryData){
                        if(primaryData.data.features && primaryData.data.features.length > 0){
                            return secondary.layerObj.loadFeatureData(evt.latlng).then(function(secondaryData){
                                callback(primaryData.data, secondaryData.data, primary, secondary, map, evt);
                            });
                        }
                    });
                }
            });
        });
    }
}



























/************************************************* common *******************************/


function registerMapevt() {
    Map.getMap(config.id).then(function(map) {
        map.on('click', function(e) {
            var options = MapOptions.getMapOptions(config.id);
            onMapClick(e, options, config, map);
        });
    });
}

function onMapClick(evt, options, config, map) {
    var scope = $rootScope.$new();

    // TODO remove if it's not necessary
    angular.extend(scope, {
        options: options,
        config: config
    });

    if (options['popup'] === CM_MAP_POPUP_TYPE.static) {

        // display static popup
        LeafletPopup.get({scope: scope}, this)
            .setLatLng(evt.latlng)
            .setContent(options['popupTemplate'])
            .openOn(map);
    }

    if (options['popup'] === CM_MAP_POPUP_TYPE.dynamic) {
        // display dynamic popup
        // prepare template
        var tplComponents = [];
        var index = 0;

        scope.featureDataLoaded = [];
        scope.featureData = [];

        var someDataLoaded = false;

        var popup = LeafletPopup.get({scope: scope}, this);
        popup.setLatLng(evt.latlng);

        var highestprimaryId = MapprimaryVisibility.getHighestPriorityprimaryId(config);

        // iterate through primarys
        MapConfig.forEachprimary(config, function (primary) {
            if ((primary.type === CM_MAP_primary_TYPE.wms) || (primary.type === CM_MAP_primary_TYPE.wfs)) {

                if (primary.onprimaryClick && primary.visible) {
                    if(options['loadingSpin']) {
                        MapSpinner.initSpinner(config.id, map.getContainer());
                    }

                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            primary.onprimaryClick(evt, data.data.features, primary);
                        }
                        if(options['loadingSpin']) {
                            MapSpinner.stop(config.id);
                        }
                    });
                }

                if ((primary.featureSummaryTemplate) && primary.visible) {
                    var newTplComponent = '<div ng-show="featureDataLoaded[' + index + ']" ng-repeat="feature in featureData[' + index + ']">'
                        + primary.featureSummaryTemplate.replace(new RegExp('feature', 'g'), "feature.properties")
                        + '</div>';
                    tplComponents.push(newTplComponent);

                    var primaryIndex = index;
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            scope.featureDataLoaded[primaryIndex] = true;
                            scope.featureData[primaryIndex] = data.data.features;

                            if (!someDataLoaded) {
                                popup.openOn(map);
                                someDataLoaded = true;
                            }
                        }
                    });

                    index++;
                }

                if (primary.infoboxTemplate && primary.visible &&
                    ((highestprimaryId > -1 && primary.infoboxTemplatePriority === highestprimaryId) ||
                    highestprimaryId === -1)
                ) {
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        scope.$root.$broadcast(CM_MAP_evt.openInfobox, {
                            type: primary.infoboxTemplate,
                            features: data.data.features
                        });
                    });
                }
            }
        });

        tplComponents.reverse();
        var template = options['popupTemplate'] + tplComponents.join("") + "{{featureData[" + index + "].parcis}}";

        popup.setContent(template);
    }
}


/*
    //TODO: logic below belongs to different services

    // temporary disable popup
    var disablePopup = function () {
        options['popup-origvalue'] = options['popup'];
        options['popup'] = CM_MAP_POPUP_TYPE.none;
    };

    // reenable popup
    var enablePopup = function () {
        options['popup'] = options['popup-origvalue'];
    };




    map.on('draw:drawstart', disablePopup);
    map.on('edit:editstart', disablePopup);
    map.on('delete:deletestart', disablePopup);

    map.on('draw:drawstop', enablePopup);
    map.on('edit:editstop', enablePopup);
    map.on('delete:deletestop', enablePopup);
*/

angular
    .module('cm.map')
    .factory('MapLayerVisibility', MapLayerVisibility);

MapLayerVisibility.$inject = ['CM_MAP_LAYER_TYPE', 'MapLayer', 'MapConfig'];

function MapLayerVisibility(CM_MAP_LAYER_TYPE, MapLayer, MapConfig) {
    return {
        fixedLayersVisibilityFromMapZoom: fixedLayersVisibilityFromMapZoom,
        getHighestPriorityLayerId: getHighestPriorityLayerId,
        getVisibleLayersFromZoom: getVisibleLayersFromZoom,
        getUnvisibleLayersFromZoom: getUnvisibleLayersFromZoom,
        checkLayersVisibility: checkLayersVisibility,
        visibleLayersFromZoom: visibleLayersFromZoom,
        unvisibleLayersFromZoom: unvisibleLayersFromZoom
    };

    //----------------------------------------------------------------------

    /**
     * Sets visibility configuration of layers with minZoomVisibility.
     * @param map
     * @param config
     * @returns config
     */
    function fixedLayersVisibilityFromMapZoom(map, config) {
        var currentZoom = map.getZoom();

        config = visibleLayersFromZoom(config, currentZoom);
        config = unvisibleLayersFromZoom(config, currentZoom);

        return config;
    }



    /**
     * Gets number of priority from layer with the highest priority.
     * @param config
     * @returns {Number}
     */
    function getHighestPriorityLayerId(config) {
        var result = -1;

        angular.forEach(config, function (value) {
            result = _mergePriority(_getHighestPriorityLayerIdRecursive(value), result);
        });

        return result;
    }

    function _getHighestPriorityLayerIdRecursive(config) {
        var result = -1;

        if (angular.isArray(config)) {
            angular.forEach(config, function (value) {
                if (value && value.type === CM_MAP_LAYER_TYPE.node) {
                    for (var i = 0; i < value.layers.length; i++) {
                        result = _mergePriority(_getHighestPriorityLayerIdRecursive(value.layers[i]), result);
                    }
                }
            });
        } else if (config && config.infoboxTemplatePriority && config.visible) {
            result = _mergePriority(config.infoboxTemplatePriority, result);
        }

        return result;
    }

    /**
     * Return ids of visible layers for this zoom.
     * @param {Array} config
     * @param {Number} zoom
     * @returns {Array}
     */
    function getVisibleLayersFromZoom(config, zoom) {
        var result = [];

        MapConfig.forEachLayer(config, function (layer) {
            if (angular.isNumber(layer.minZoomVisibility) && layer.minZoomVisibility <= zoom && layer.visible === false) {
                result.push(layer.id);
            }
        });

        return result;
    }

    /**
     * Return ids of unvisible layers for this zoom.
     * @param {Array} config
     * @param {Number} zoom
     * @returns {Array}
     */
    function getUnvisibleLayersFromZoom(config, zoom) {
        var result = [];

        MapConfig.forEachLayer(config, function (layer) {
            if (angular.isNumber(layer.minZoomVisibility) && layer.minZoomVisibility > zoom && layer.visible) {
                result.push(layer.id);
            }
        });

        return result;
    }

    /**
     * When zooming, some layers could have visibility turn off.
     * They need to be reloaded.
     * @param {type} config
     * @param {type} map
     * @returns {undefined}
     */
    function checkLayersVisibility(config, map) {
        var toVisible = getVisibleLayersFromZoom(config, map.getZoom());
        var toUnvisible = getUnvisibleLayersFromZoom(config, map.getZoom());
        var ids = angular.extend([], toVisible, toUnvisible);

        if (ids.length > 0) {
            config = fixedLayersVisibilityFromMapZoom(map, config);
            MapLayer.updateNode(config, MapConfig.getLayerPathById(config, ids[0]));
        }
    }

    function _mergePriority(p1, p2) {
        if (p1 > p2) {
            return p1;
        } else {
            return p2;
        }
    }

    /**
     * Sets visible config of layers visible for the zoom.
     * @param {Array} config
     * @param {Number} zoom
     * @returns {Array}
     */
    function visibleLayersFromZoom(config, zoom) {
        MapConfig.forEachLayer(config, function (layer) {
            if (angular.isNumber(layer.minZoomVisibility) && layer.minZoomVisibility <= zoom) {
                layer.visible = true;
            }
        });

        return config;
    }

    /**
     * Sets unvisible config of layers visible for the zoom.
     * @param {Array} config
     * @param {Number} zoom
     * @returns {Array}
     */
    function unvisibleLayersFromZoom(config, zoom) {
        MapConfig.forEachLayer(config, function (layer) {
            if (angular.isNumber(layer.minZoomVisibility) && layer.minZoomVisibility > zoom) {
                layer.visible = false;
            }
        });

        return config;
    }
}
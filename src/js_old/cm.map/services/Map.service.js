/**
 * Created on 12.11.15.
 */

angular
    .module('cm.map')
    .factory('Map', MapFunction);

MapFunction.$inject = ['MapHelpers'];

function MapFunction(MapHelpers) {
    var maps = {};

    // definition of CRS in proj4js format
    /*var crsDefs = {
        5514: "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222" +
        " +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=589,76,480,0,0,0,0 +units=m +no_defs"
    };*/

    //def from confluence
    var crsDefs = {
        5514: "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813975277778 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel" +
        "+units=m +towgs84=570.8,85.7,462.8,4.998,1.587,5.261,3.56"
    };

    /**
     * Helper function to safely run angular scope $digest/$eval/$apply loop.
     * @param {Object} $scope Any scope object that has reference $root to $rootScope
     * @param {Function} fn Function to be executed in $digest loop.
     */
    var safeApply = function($scope, fn) {
        var phase = $scope.$root.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            $scope.$eval(fn);
        } else {
            $scope.$apply(fn);
        }
    };

    return {
        setMap: setMap,
        getMap: getMap,
        resetMap: resetMap,
        registerProjection: registerProjection,
        getCRS: getCRS,
        dispatchMapEvent: dispatchMapEvent
    };

/////////////////////////

    // set Leaflet map object
    function setMap(map, id) {
        var defer = MapHelpers.getUnresolvedDefer(maps, id);
        defer.resolve(map);
        MapHelpers.setResolvedDefer(maps, id);
    }

    // get Leaflet map object based on id
    function getMap(id) {
        var defer = MapHelpers.getDefer(maps, id);
        return defer.promise;
    }

    function resetMap(mapId){
        maps[mapId] = undefined;
    }

    // register projections with proj4 - necessary for Leaflet GeoJSON layers 5514
    function registerProjection(projId){
        proj4.defs('urn:ogc:def:crs:EPSG::' + projId, crsDefs[projId]);
    }

    // get Leaflet CRS object for projection
    function getCRS(srid) {
        switch (srid) {
            case 3857:
                return L.CRS.EPSG3857;

            case 4326:
                return L.CRS.EPSG4326;

            case 3395:
                return L.CRS.EPSG3395;

            case 5514:
                var epsg5514 = new L.Proj.CRS('EPSG:5514', crsDefs[5514],
                        {
                            // aligned to settings of CMProxy
                            resolutions:
                                    [
                                        1024,
                                        512,
                                        256,
                                        128,
                                        64,
                                        32,
                                        16,
                                        8,
                                        4,
                                        2,
                                        1,
                                        0.5,
                                        0.25,
                                        0.125
                                    ],
                            origin: [-925000, -920000],
                            bounds: L.bounds(
                                    L.point(-920000, -1449288),
                                    L.point(-400712, -925000))
                        });

                return epsg5514;

            default:
                return null;
        }
    }

    /**
     * Broadcast leaflet map events, original events are wrapped in angular event
     * prefixed with 'cmLeafletMap.'. Leaflet event is accessible like this (moveend
     * example):
     *   $scope.$on('cmLeafletMap.moveend', function(angularEvent, data) {
     *     var leafletEvent = data.leafletEvent;
     *     var map = leafletEvent.target;
     *   }
     * To register dispatcher, use this:
     *   map.on('moveend', CmLeafletService.dispatchMapEvent(scope, 'moveend');
     *
     * @param {Object} scope
     * @param {String} eventName
     * @returns {Function}
     */
    function dispatchMapEvent(scope, eventName) {
        return function (e) {
            var broadcastName = 'cmLeafletMap.' + eventName;
            safeApply(scope, function (scope) {
                scope.$root.$broadcast(broadcastName, {
                    leafletEvent: e
                });
            });
        };
    }
}
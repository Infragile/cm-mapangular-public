/**
 * Created on 12.11.15.
 */

angular
    .module('cm.map')
    .factory('MapLayer', MapLayer);

MapLayer.$inject = ['CM_MAP_LAYER_TYPE', 'LeafletProjGeoJsonWfs', 'LeafletTileLayerWfs', 'LeafletTileLayerWmsFiltered'];

function MapLayer(CM_MAP_LAYER_TYPE, LeafletProjGeoJsonWfs, LeafletTileLayerWfs, LeafletTileLayerWmsFiltered) {

    return {
        isLayerEdit: isLayerEdit,
        updateNode: updateNode,
        reloadNode: reloadNode,
        createLayer: createLayer,
        createNode: createNode
    };

    /////////////////////////

    // find a node in config tree and update it,
    // also update all nodes on top of the layer
    function updateNode(nodeConfig, path, pathFound) {
        pathFound = pathFound || false;

        if (nodeConfig.id === path[0]) {
            path.shift();
        }

        if (path.length === 0) {
            pathFound = true;
        }

        if (pathFound) {
            reloadNode(nodeConfig);
            return true;
        }

        // search for child layer with given id
        for (var i = 0; i < nodeConfig.layers.length; i++) {
            if (nodeConfig.layers[i]['type'] === CM_MAP_LAYER_TYPE.node) {
                if (pathFound) {
                    reloadNode(nodeConfig.layers[i]);
                }

                pathFound = updateNode(nodeConfig.layers[i], path, pathFound);
            }
        }

        return pathFound;
    }

    // create node
    function createNode(nodeConfig) {
        var nodeObj = new L.layerGroup();

        createNodeChildren(nodeConfig, nodeObj);

        return nodeObj;
    }

    // reload existing node
    function reloadNode(nodeConfig) {
        var nodeObj = nodeConfig['layerObj'];

        nodeObj.clearLayers();

        createNodeChildren(nodeConfig, nodeObj);
    }

    // create children of a node
    function createNodeChildren(nodeConfig, nodeObj) {
        if (nodeConfig && nodeConfig.layers) {
            for (var i = 0; i < nodeConfig.layers.length; i++) {
                if (nodeConfig.layers[i]['visible']) {
                    var layerObj = createLayer(nodeConfig.layers[i]);
                    nodeConfig.layers[i]['layerObj'] = layerObj;
                    nodeObj.addLayer(layerObj);
                }
                else {
                    nodeConfig.layers[i]['layerObj'] = null;
                }
            }
        }
    }

    // create WMS layer
    function createWmsLayer(layerConfig) {
        return LeafletTileLayerWmsFiltered.getLayer(
                layerConfig['url'],
                layerConfig);
    }

    // create GeoJSON layer
    function createGeoJsonWfsLayer(layerConfig) {
        return LeafletProjGeoJsonWfs.getLayer(
            layerConfig['url'],
            layerConfig
        );
    }

    // create tileds WFS layer
    function createTileWfsLayer(layerConfig) {
        return LeafletTileLayerWfs.getLayer(
            layerConfig['url'],
            layerConfig);
    }

    // create layer from configuration
    function createLayer(config) {
        switch (config['type']) {
            case CM_MAP_LAYER_TYPE.node:
                return createNode(config);

            case CM_MAP_LAYER_TYPE.wms:
                return createWmsLayer(config);

            case CM_MAP_LAYER_TYPE.wfs:
                return createGeoJsonWfsLayer(config);

            case 'wfs-tiled':
                return createTileWfsLayer(config);

            default:
                console.log('Unknown layer type ' + config.type);
                return null;
        }
    }

    function isLayerEdit(layer) {
        return layer.edit === true;
    }
}

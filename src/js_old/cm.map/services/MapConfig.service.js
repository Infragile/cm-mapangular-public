
angular
        .module('cm.map')
        .factory('MapConfig', MapConfig);

MapConfig.$inject = ['CM_MAP_LAYER_TYPE', 'MapHelpers'];

function MapConfig(CM_MAP_LAYER_TYPE, MapHelpers) {

    var configs = [];

    var service = {
        setConfig: setConfig,
        getConfig: getConfig,
        forEach: forEach,
        forEachLayer: forEachLayer,
        getLayerConfigById: getLayerConfigById,
        getLayerPathById: getLayerPathById
    };

    return service;

    /////////////////////////

    function setConfig(conf, id){
        var defer = MapHelpers.getUnresolvedDefer(configs, id);
        defer.resolve(conf);
        MapHelpers.setResolvedDefer(configs, id);
        config = conf;
    }

    function getConfig(id){
        var defer = MapHelpers.getDefer(configs, id);
        return defer.promise;
    }

    function forEach(config, func) {
        func(config);

        if (angular.isArray(config.layers)) {
            for (var i = 0; i < config.layers.length; i++) {
                forEach(config.layers[i], func);
            }
        }
    }
    
    /**
     * Applies func recursively for every layer in config.
     * @param layerConfig
     * @param func
     */
    function forEachLayer(layerConfig, func) {
        func(layerConfig);

        if (layerConfig.type === CM_MAP_LAYER_TYPE.node) {
            for (var i = 0; i < layerConfig.layers.length; i++) {
                forEachLayer(layerConfig.layers[i], func);
            }
        }
    }

    /**
     * Get config of layer with given id
     * 
     * @param {type} config
     * @param {type} id
     * @returns {LayerSelectorLayers.getLayerConfigById.config}
     */
    function getLayerConfigById(config, id) {
        if (config.id === id) {
            return config;
        }

        if (angular.isArray(config.layers)) {
            for (var i = 0; i < config.layers.length; i++) {
                var configFound = getLayerConfigById(config.layers[i], id);

                if (configFound !== null) {
                    return configFound;
                }
            }
        }

        return null;
    }

    /**
     * Get path to a layer with given id.
     * Path is a list of ids of all parent nodes from root to parent. Path does not include
     * id of the layer
     * 
     * @param {type} config
     * @param {type} id
     * @returns {undefined}
     */
    function getLayerPathById(config, id, path) {
        if (!path) {
            path = [];
        }

        if (config.id === id) {
            console.log('Found ' + id);
            return path;
        }

        if (angular.isArray(config.layers)) {
            path.push(config.id);

            for (var i = 0; i < config.layers.length; i++) {
                var pathFound = getLayerPathById(config.layers[i], id, path);

                if (pathFound !== null) {
                    return pathFound;
                }
            }
        }

        return null;
    }

}

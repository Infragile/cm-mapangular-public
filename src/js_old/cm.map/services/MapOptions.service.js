angular
    .module('cm.map')
    .factory('MapOptions', MapOptions);

MapOptions.$inject = ['CM_MAP_POPUP_TYPE'];

function MapOptions(CM_MAP_POPUP_TYPE) {
    var optionsStorage = [];
    
    return {
        getMapOptions: getMapOptions,
        initMapOptions: initMapOptions,
        setMapOptions: setMapOptions
    };
    
    /*************************************************************************/
    
    function getMapOptions(mapId) {
        return optionsStorage[mapId];
    }
    
    function initMapOptions(scopeOptions) {
        var options = {
            srid: 0,
            popup: CM_MAP_POPUP_TYPE.none,
            popupTemplate: ''
        };
        
        angular.extend(options, scopeOptions);
        
        return options;
    }
    
    function setMapOptions(mapId, options) {
        optionsStorage[mapId] = options;
    }
}
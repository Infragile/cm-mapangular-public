
angular
        .module('cm.layerSelector')
        .factory('LayerSelectorStorage', LayerSelectorStorage);

LayerSelectorStorage.$inject = ['localStorageService', 'MapConfig', 'LayerSelectorOptions'];

function LayerSelectorStorage(localStorageService, MapConfig, LayerSelectorOptions) {

    var service = {
        saveState: saveState,
        loadState: loadState
    };

    return service;

    /////////////////////////

    function getConfigStorageId(config) {
        return 'layerSelectorState' + config.id;
    }

    /**
     * Get config state to save
     * 
     * @param {type} config
     * @returns Array
     */
    function getStateToSave(config) {
        var result = {};

        MapConfig.forEach(config, function (config) {
            var item = {
                id: config.id
            };

            if (angular.isNumber(config.minZoomVisibility)) {
                item.visible = true;
            } else {
                item.visible = config.visible;
            }

            if (config.comboBox && config.selected !== undefined) {
                item.selected = config.selected;
            }

            if (config.isCollapsed === true) {
                item.isCollapsed = 'true';
            } else {
                item.isCollapsed = 'false';
            }

            result[config.id] = item;
        });

        return result;
    }

    function updateConfigWithState(scopeConfig, loadedState, options) {
        if (! loadedState) {
            return false;
        }
        
        var mapUpdateNeeded = false;

        MapConfig.forEach(scopeConfig, function (config) {
            var state = loadedState[config.id];

            if (!state) {
                return;
            }

            if (LayerSelectorOptions.getSaveLayersVisibility(options)) {
                if (config.id !== undefined && config.visible !== undefined) {
                    var newVisible = state.visible;

                    if ((config.visible !== newVisible) && (newVisible !== null)) {
                        config.visible = newVisible;
                        mapUpdateNeeded = true;
                    }

                    var newSelected = state.selected;
                    if (config.comboBox && config.selected !== newSelected) {
                        config.selected = newSelected;
                        mapUpdateNeeded = true;
                    }
                }
            }

            if (LayerSelectorOptions.getSaveGroupCollapseStatus(options)) {
                if (state.isCollapsed === 'true') {
                    config.isCollapsed = true;
                }
                if (state.isCollapsed === 'false') {
                    config.isCollapsed = false;
                }
            }
        });

        return mapUpdateNeeded;
    }

    /**
     * Save state to local storage
     * 
     * @param {type} config
     * @returns {undefined}
     */
    function saveState(config) {
        localStorageService.set(getConfigStorageId(config), getStateToSave(config));
    }

    /**
     * Load config from local storage and update config
     * 
     * @returns True if map update is needed
     */
    function loadState(scopeConfig, options) {
        var loadedState = localStorageService.get(getConfigStorageId(scopeConfig));
        
        return updateConfigWithState(scopeConfig, loadedState, options);
    }
}

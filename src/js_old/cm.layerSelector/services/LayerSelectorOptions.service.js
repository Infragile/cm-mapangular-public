
angular
        .module('cm.layerSelector')
        .factory('LayerSelectorOptions', LayerSelectorOptions);

LayerSelectorOptions.$inject = [];

function LayerSelectorOptions() {

    var service = {
        getDefaultOptions: getDefaultOptions,
        getSaveLayersVisibility: getSaveLayersVisibility,
        getSaveGroupCollapseStatus: getSaveGroupCollapseStatus,
        getShowCollapsedVisibleLayersCount: getShowCollapsedVisibleLayersCount
    };

    return service;

    /////////////////////////

    function getDefaultOptions() {
        return {
            saveLayersVisibility: false,
            saveGroupCollapseStatus: false,
            showCollapsedVisibleLayersCount: false
        };
    }

    function getSaveLayersVisibility(options) {
        return options.saveLayersVisibility === true;
    }

    function getSaveGroupCollapseStatus(options) {
        return options.saveGroupCollapseStatus === true;
    }
    
    function getShowCollapsedVisibleLayersCount(options) {
        return options.showCollapsedVisibleLayersCount === true;
    }

}

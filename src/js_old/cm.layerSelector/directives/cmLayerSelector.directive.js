angular
        .module('cm.layerSelector')
        .directive('cmLayerSelector', cmLayerSelector);

function cmLayerSelector() {

    var directive = {
        restrict: 'E',
        scope: {},
        bindToController: {
            options: '=',
            config: '='
        },
        templateUrl: function(element, attrs) {
            return attrs.templateUrl || 'cm.layerSelector/templates/cmLayerSelector.html';
        },
        controller: CmLayerSelectorController,
        controllerAs: 'CmLayerSelectorCtrl'
    };

    return directive;
}

CmLayerSelectorController.$inject = [
    'MapConfig',
    'LayerSelectorOptions',
    'LayerSelectorStorage',
    'CM_MAP_EVENT',
    'CM_MAP_EDIT_CHANNEL',
    'pubSub',
    'Map',
    'MapLayer',
    'MapLayerVisibility'
];

function CmLayerSelectorController(
        MapConfig,
        LayerSelectorOptions,
        LayerSelectorStorage,
        CM_MAP_EVENT,
        CM_MAP_EDIT_CHANNEL,
        pubSub,
        Map,
        MapLayer,
        MapLayerVisibility
) {
    var vm = this;
    vm.activeLayer = null;

    vm.hideLayerObjects = hideLayerObjects;
    vm.unselectLayerObject = unselectLayerObject;
    vm.showCollapsedVisibleLayersCount = showCollapsedVisibleLayersCount;
    vm.getSelectorLayers = getSelectorLayers;
    vm.getDisplayedLayersCount = getDisplayedLayersCount;
    vm.switchComboBox = switchComboBox;
    vm.switchLayer = switchLayer;
    vm.switchGroup = switchGroup;
    vm.filterUnselectedSwitch = filterUnselectedSwitch;
    vm.layerToEdit = layerToEdit;
    vm.setActiveLayer = setActiveLayer;

    activate();

    function activate() {
        var defaultOptions = LayerSelectorOptions.getDefaultOptions();

        // merge provided options to default
        vm.options = angular.extend(defaultOptions, vm.options);

        MapConfig.getConfig(vm.config.id).then(function(conf) {
            vm.config = conf;
            setActiveLayer(vm.config.layers[0].layers[0])
        });

    }

    function hideLayerObjects(config, layerIds) {
        if (!angular.isArray(layerIds)) {
            return;
        }
        layerIds.forEach(function (id) {
            var config = MapConfig.getLayerConfigById(config, id);
            if (config.selector === 'group') {
                hideGroup(config);
            } else {
                hideLayer(config);
            }
        });
    }

    function hideLayer(config, layerConfig) {
        if (angular.isString(layerConfig)) {
            layerConfig = MapConfig.getLayerConfigById(config, layerConfig);
        }
        if (layerConfig.visible) {
            switchLayer(layerConfig);
        }
    }

    function hideGroup(config, groupConfig) {
        if (angular.isString(groupConfig)) {
            groupConfig = MapConfig.getLayerConfigById(config, groupConfig);
        }
        if (groupConfig.selector !== 'group') {
            return;
        }
        groupConfig.layers.forEach(hideLayer);
    }

    function unselectLayerObject(config, layerId) {
        var configToUnselect = MapConfig.getLayerConfigById(config, layerId);

        if (configToUnselect && configToUnselect.selected) {
            configToUnselect.selected = false;
        }
    }

    function showCollapsedVisibleLayersCount() {
        return LayerSelectorOptions.getShowCollapsedVisibleLayersCount(vm.options);
    }

    function layerToEdit(layer) {
        if(layer.edit === true || layer.edit === false) {
            layer.edit = !layer.edit;
        } else {
            layer.edit = true;
        }

        pubSub.publish(CM_MAP_EVENT.layerToEdit, {
            layer: layer
        });
    }

    function getSelectorLayers(layerConfig) {
        return (layerConfig.comparator ? layerConfig.layers.slice().sort(layerConfig.comparator) : layerConfig.layers.slice().reverse());
    }

    function getDisplayedLayersCount(layer) {
        var current = 0;

        if (layer.visible && layer.type !== 'node') {
            current += 1;
        }

        if (angular.isArray(layer.layers)) {
            angular.forEach(layer.layers, function (value) {
                current += getDisplayedLayersCount(value);
            });
        }

        return current;
    }

    function switchComboBox(layerId) {
        var config = MapConfig.getLayerConfigById(vm.config, layerId);

        angular.forEach(config.comboBox, function (value, key) {
            if (key !== config.id) {
                hideLayerObjects(vm.config, [key]);
                unselectLayerObject(vm.config, key);
            }
        });

        config.selected = true;

        if (config.selector === 'switch') {
            config.visible = true;

            updateLayer(config);
        }

        if (config.visible && config.hideIfVisible) {
            hideLayerObjects(vm.config, config.hideIfVisible);
        }

        LayerSelectorStorage.saveState(vm.config);
    }

    function setActiveLayer(layerConfig){
        if(!vm.activeLayer || (vm.activeLayer.id !== layerConfig.id)){
            vm.activeLayer = layerConfig;
            pubSub.publish(CM_MAP_EDIT_CHANNEL.layerToEdit, vm.activeLayer);
        }
    }

    function switchLayer(layerConfig) {
        layerConfig.visible = !layerConfig.visible;

        updateLayer(layerConfig);

        if (layerConfig.visible && layerConfig.hideIfVisible) {
            hideLayerObjects(vm.config, layerConfig.hideIfVisible);
        }

        LayerSelectorStorage.saveState(vm.config);
    }

    function switchGroup(layerConfig) {
        layerConfig.collapsed = !layerConfig.collapsed;

        LayerSelectorStorage.saveState(vm.config);
    }

    function filterUnselectedSwitch(layer) {
        if (layer.comboBox && layer.selected === false) {
            return false;
        } else {
            return true;
        }
    }

    function updateLayer(layerConfig) {
        Map.getMap(vm.config.id).then(function(map) {
            vm.config = MapLayerVisibility.fixedLayersVisibilityFromMapZoom(map, vm.config);
            MapLayer.updateNode(vm.config, angular.copy(layerConfig.path));
        });
    }
}

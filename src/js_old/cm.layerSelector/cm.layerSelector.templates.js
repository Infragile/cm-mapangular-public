angular.module('cm.layerSelector').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('cm.layerSelector/templates/cmLayerSelector.html',
    "<div class=\"cmLayerSelector\">\n" +
    "    <ng-include src=\"'cm.layerSelector/templates/layerTree.html'\" ng-init=\"layer = CmLayerSelectorCtrl.config\"></ng-include>\n" +
    "</div>\n"
  );


  $templateCache.put('cm.layerSelector/templates/layerTree.html',
    "<span ng-if=\"layer.selector === 'switch' && layer.selected != false\">\n" +
    "    <div class=\"switch\" ng-class=\"{'layer-visible': layer.visible}\" ng-if=\"!layer.comboBox\">\n" +
    "        <span ng-click=\"switchLayer(layer)\">\n" +
    "            <span ng-if=\"layer.selectorStyle !== 'none'\" class=\"selector\">\n" +
    "                <span ng-if=\"layer.icon\" ng-show=\"layer.visible\" class=\"icon\">\n" +
    "                    <img ng-src=\"{{layer.icon}}\" />\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-if=\"layer.selectorStyle === 'class'\" ng-show=\"layer.visible\" class=\"indicator\">\n" +
    "                    <div style=\"width: 100%; height: 100%;\" class=\"{{layer.class}}\">\n" +
    "                        &nbsp;\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-if=\"layer.selectorStyle === 'fill'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                      ng-style=\"{'background-color': layer.fillColor}\">\n" +
    "                    &nbsp;\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-if=\"layer.selectorStyle === 'border'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                      ng-style=\"{'border-width': layer.borderWidth,\n" +
    "                                       'border-style': layer.borderStyle,\n" +
    "                                       'border-color': layer.borderColor\n" +
    "                                      }\">\n" +
    "                    &nbsp;\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-if=\"layer.selectorStyle === 'border-fill'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                      ng-style=\"{'border-width': layer.borderWidth,\n" +
    "                                       'border-style': layer.borderStyle,\n" +
    "                                       'border-color': layer.borderColor\n" +
    "                                      }\">\n" +
    "                    <div style=\"width: 100%; height: 100%;\"\n" +
    "                         ng-style=\"{'background-color': layer.fillColor,\n" +
    "                                            'opacity': layer.fillOpacity}\">\n" +
    "                        &nbsp;\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </span>\n" +
    "\n" +
    "            <span class=\"title\" title=\"{{ layer.titleTooltip}}\">\n" +
    "                {{layer.title}}\n" +
    "            </span>\n" +
    "\n" +
    "            <span class=\"fa\" ng-class=\"{'fa-plus': !layer.visible, 'fa-check': layer.visible}\">\n" +
    "            </span>\n" +
    "        </span>\n" +
    "        \n" +
    "        <span ng-click=\"layerToEdit(layer)\" ng-if=\"layer.editable && !layer.comboBox\" class=\"fa\" ng-class=\"{'fa-plus': !layer.edit, 'fa-pencil': layer.edit}\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    \n" +
    "    <div class=\"switch\" ng-if=\"layer.comboBox\">\n" +
    "        <span ng-if=\"layer.selectorStyle !== 'none'\" class=\"selector\">\n" +
    "            <span ng-if=\"layer.icon\" ng-show=\"layer.visible\" class=\"icon\">\n" +
    "                <img ng-src=\"{{layer.icon}}\" />\n" +
    "            </span>\n" +
    "\n" +
    "            <span ng-if=\"layer.selectorStyle === 'fill'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                  ng-style=\"{'background-color': layer.fillColor}\">\n" +
    "                &nbsp;\n" +
    "            </span>\n" +
    "\n" +
    "            <span ng-if=\"layer.selectorStyle === 'border'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                  ng-style=\"{'border-width': layer.borderWidth,\n" +
    "                                   'border-style': layer.borderStyle,\n" +
    "                                   'border-color': layer.borderColor\n" +
    "                                  }\">\n" +
    "                &nbsp;\n" +
    "            </span>\n" +
    "\n" +
    "            <span ng-if=\"layer.selectorStyle === 'border-fill'\" ng-show=\"layer.visible\" class=\"indicator\"\n" +
    "                  ng-style=\"{'border-width': layer.borderWidth,\n" +
    "                                   'border-style': layer.borderStyle,\n" +
    "                                   'border-color': layer.borderColor\n" +
    "                                  }\">\n" +
    "                <div style=\"width: 100%; height: 100%;\"\n" +
    "                     ng-style=\"{'background-color': layer.fillColor,\n" +
    "                                        'opacity': layer.fillOpacity}\">\n" +
    "                    &nbsp;\n" +
    "                </div>\n" +
    "            </span>\n" +
    "        </span>\n" +
    "\n" +
    "        <div class=\"component-dropdownSingleselect\" ng-class=\"{'popupTop': layer.comboBoxReverse}\" ng-if=\"layer.comboBox\">\n" +
    "            <span class=\"fa fa-caret-down title\">\n" +
    "                {{layer.title}}\n" +
    "            </span>\n" +
    "\n" +
    "            <ul>\n" +
    "                <li ng-repeat=\"(id, name) in layer.comboBox\" ng-class=\"{'selected': layer.selected && layer.id == id}\">\n" +
    "                    <a ng-click=\"switchComboBox(id)\">{{ name}}</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</span>\n" +
    "\n" +
    "<span ng-if=\"layer.selector === 'hidden'\">\n" +
    "    <ul ng-if=\"layer.layers\">\n" +
    "        <li ng-repeat=\"layer in getSelectorLayers(layer)\" ng-include=\"'layerTree'\">\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</span>\n" +
    "\n" +
    "<span ng-if=\"layer.selector === 'group' && layer.selected != false\">\n" +
    "    <div class=\"group\">\n" +
    "        <div class=\"group-title\" ng-click=\"switchGroup(layer)\" ng-if=\"!layer.comboBox\">\n" +
    "            {{layer.title}} <span ng-show=\"showCollapsedVisibleLayersCount() && layer.collapsed\" title=\"Počet zobrazených vrstev\" style=\"cursor: help;\">({{ getDisplayedLayersCount(layer)}})</span>\n" +
    "        </div>\n" +
    "        <section class=\"component-dropdownSingleselect\" ng-if=\"layer.comboBox\">\n" +
    "            <span class=\"fa fa-caret-down group-title\">\n" +
    "                {{layer.comboBox[layer.id]}}\n" +
    "            </span>\n" +
    "\n" +
    "            <ul>\n" +
    "                <li ng-repeat=\"(id, name) in layer.comboBox\" ng-class=\"{'selected': layer.selected && layer.id == id}\">\n" +
    "                    <a ng-click=\"switchComboBox(id)\">{{ name}}</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </section>\n" +
    "\n" +
    "        <ul ng-if=\"layer.layers\" ng-show=\"!layer.collapsed\">\n" +
    "            <li ng-repeat=\"layer in layer.layers.slice().reverse()| filter:filterUnselectedSwitch\" ng-include=\"'layerTree'\">\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</span>\n"
  );

}]);

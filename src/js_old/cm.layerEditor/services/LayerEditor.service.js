/**
 * Created on 13.11.15.
 */

angular
    .module('cm.layerEditor')
    .factory('LayerEditor', LayerEditor);

LayerEditor.$inject = ['Map', 'pubSub', 'SplitFeature', 'CM_MAP_EDIT_CHANNEL', 'CM_MAP_EDIT_ACTION_TYPE', 'CM_MAP_EDIT_EVENT_TYPE', 'RemoveFeature', 'FeatureVertexEdit', 'MergeFeatures', 'DrawFeatures'];

function LayerEditor(Map, pubSub, SplitFeature, CM_MAP_EDIT_CHANNEL, CM_MAP_EDIT_ACTION_TYPE, CM_MAP_EDIT_EVENT_TYPE, RemoveFeature, FeatureVertexEdit, MergeFeatures, DrawFeatures) {
    var map = null;
    var currentLayer = null;
    var editTmpLayer = null;
    var serverObj = null;
    var curLeafLayer = null;
    var actionType = null;

    var crs = {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:EPSG::5514"
        }
    };

    return {
        init: init
    };

/////////////////////////

    function init(conf, obj){
        Map.getMap(conf.id).then(function(m) {
            map = m;
            setCurrentLayer(obj);
            pubSub.subscribe(CM_MAP_EDIT_CHANNEL.uiEvents, processUiEvent, false);
        });
    }

    function setCurrentLayer(obj) {
        if(_.isObject(obj) && !_.isEmpty(obj)){
            console.info('LayerEditor currentLayer updated.');
            currentLayer = obj;
            publishAvailableActions();
        } else {
            currentLayer = null;
            publishAvailableActions([]);
            console.info('LayerEditor currentLayer deleted.');
        }
    }

    function publishAvailableActions(actionsList){
        actionsList = actionsList || getAvailableActions();
        pubSub.publish(CM_MAP_EDIT_CHANNEL.availableActions, actionsList);
    }

    function getAvailableActions(){
        var actions = [];
        if(currentLayer && currentLayer.actions){
            for(var key in currentLayer.actions) {
                if(currentLayer.actions[key]){
                    switch(key){
                        case 'edit':
                            actions.push(CM_MAP_EDIT_ACTION_TYPE.editPolygon);
                            break;
                        case 'merge':
                            actions.push(CM_MAP_EDIT_ACTION_TYPE.mergePolygons);
                            break;
                        case 'delete':
                            actions.push(CM_MAP_EDIT_ACTION_TYPE.removeFeature);
                            break;
                        case 'draw':
                            actions.push(CM_MAP_EDIT_ACTION_TYPE.drawPolygon);
                            break;
                        case 'split':
                            actions.push(CM_MAP_EDIT_ACTION_TYPE.splitPolygon);
                            break;
                    }
                }
            }
        }
        return actions;
    }

    function processUiEvent(eventType, actionType, callback){
        switch(eventType){
            case CM_MAP_EDIT_EVENT_TYPE.start:
                enableAction(actionType);
                break;
            case CM_MAP_EDIT_EVENT_TYPE.save:
                saveAction(callback);
                break;
            case CM_MAP_EDIT_EVENT_TYPE.storno:
                stornAction();
                break;
            case CM_MAP_EDIT_EVENT_TYPE.reset:
                resetAction();
                break;
            default:

        }
    }

    function enableAction(actType){
        actionType = actType;
        currentLayer.edit = true;
        switch(actType){
            case CM_MAP_EDIT_ACTION_TYPE.editPolygon:
                FeatureVertexEdit.enableFeatureVertexEdit(map, currentLayer);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.mergePolygons:
                MergeFeatures.enableMergeFeatures(map, currentLayer);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.splitPolygon:
                SplitFeature.enableSplitFeature(map, currentLayer);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.drawPolygon:
                DrawFeatures.enableDrawFeatures(map, currentLayer);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.removeFeature:
                RemoveFeature.enableRemoveFeature(map, currentLayer);
                break;
            default:
                console.warn('Action type not recognized: ' + actionType);
        }
    }

    function saveAction(callback){
        currentLayer.edit = false;
        switch(actionType){
            case CM_MAP_EDIT_ACTION_TYPE.editPolygon:
                FeatureVertexEdit.saveFeatureVertexEdit(callback);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.mergePolygons:
                MergeFeatures.saveMergeFeatures(callback);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.drawPolygon:
                DrawFeatures.saveDrawFeatures(callback);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.removeFeature:
                RemoveFeature.saveRemoveFeature(callback);
                break;
            case CM_MAP_EDIT_ACTION_TYPE.splitPolygon:
                SplitFeature.saveSplitFeature(callback);
                break;
            default:
                console.warn('Action type not recognized: ' + actionType);
        }
    }

    function stornAction(){
        if(currentLayer){
            currentLayer.edit = false;
        }
        if(actionType){
            switch(actionType){
                case CM_MAP_EDIT_ACTION_TYPE.editPolygon:
                    FeatureVertexEdit.stornFeatureVertexEdit();
                    break;
                case CM_MAP_EDIT_ACTION_TYPE.mergePolygons:
                    MergeFeatures.stornMergeFeatures();
                    break;
                case CM_MAP_EDIT_ACTION_TYPE.drawPolygon:
                    DrawFeatures.stornDrawFeatures();
                    break;
                case CM_MAP_EDIT_ACTION_TYPE.removeFeature:
                    RemoveFeature.stornRemoveFeature();
                    break;
                case CM_MAP_EDIT_ACTION_TYPE.splitPolygon:
                    SplitFeature.stornSplitFeature();
                    break;
                default:
                    console.error('Action type not recognized or not present: ' + actionType);
            }
            actionType = null;
        }
    }

    function resetAction(){
        switch(actionType){
            case CM_MAP_EDIT_ACTION_TYPE.editPolygon:
                FeatureVertexEdit.resetFeatureVertexEdit();
                break;
            case CM_MAP_EDIT_ACTION_TYPE.mergePolygons:
                MergeFeatures.resetMergeFeatures();
                break;
            case CM_MAP_EDIT_ACTION_TYPE.drawPolygon:
                DrawFeatures.resetDrawFeatures();
                break;
            case CM_MAP_EDIT_ACTION_TYPE.removeFeature:
                RemoveFeature.resetRemoveFeature();
                break;
            case CM_MAP_EDIT_ACTION_TYPE.splitPolygon:
                SplitFeature.resetSplitFeature();
                break;
            default:
                console.warn('Action type not recognized: ' + actionType);
        }
    }
}

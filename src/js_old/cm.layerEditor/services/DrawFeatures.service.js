/**
 * Created by ondrejzvara on 27.11.15.
 */

angular
    .module('cm.layerEditor')
    .factory('DrawFeatures', DrawFeatures);

DrawFeatures.$inject = ['pubSub', 'CM_MAP_EDIT_CHANNEL', 'MapEvents', 'EditorUtils'];

function DrawFeatures(pubSub, CM_MAP_EDIT_CHANNEL, MapEvents, EditorUtils) {
    var map = null;
    var layer = null;
    var drawer = null;
    var drawLayer = null;
    var backupArr = [];
    var currentLayer = null;
    var polygonDrawer = null;
    var referenceLayer;
    var unsubscribe;
    var drawOptions = {
        allowIntersection: false,
        repeatMode: false,
        drawError: {
            color: '#b00b00',
            timeout: 3000
        },
        guidelineDistance: 20,
        maxGuideLineLength: 4000,
        shapeOptions: {
            stroke: true,
            color: '#f06eaa',
            weight: 5,
            opacity: 0.5,
            fill: true,
            clickable: true,
            fillColor: null, //same as color by default
            fillOpacity: 0.5
        },
        zIndexOffset: 5000
    };

    return {
        enableDrawFeatures: enableDrawFeatures,
        saveDrawFeatures: saveDrawFeatures,
        stornDrawFeatures: stornDrawFeatures,
        resetDrawFeatures: resetDrawFeatures
    };

/////////////////////////

    function enableDrawFeatures(mapObj, currentL) {
        if(!unsubscribe){
            map = mapObj;
            currentLayer = currentL;

            drawLayer = new L.geoJson();
            drawLayer.addTo(map);

            referenceLayer = new L.geoJson();
            referenceLayer.addTo(map);

            unsubscribe = pubSub.subscribe(CM_MAP_EDIT_CHANNEL.loadedFeaturesToEdit, featureDraw, false);
            console.info('Enabling DrawFeatures()');
        }
    }

    function featureDraw(obj) {
        if(EditorUtils.inCollection(backupArr, obj )){
            if(backupArr.length < 1){
                createLayerDraw(obj);
                backupArr.push(obj);
            }
        }
    }

    function saveDrawFeatures(callback) {
        if (typeof callback !== "function"){
            console.warn("Invalid callback function.");
            return;
        }

        if(!backupArr.length) {
            console.warn('Nothing to save!');
            return;
        }

        callback.call(this, prepareForSave(), backupArr[0]);
        console.info('Saving DrawFeature() result.');
        stornDrawFeatures();
    }

    function stornDrawFeatures() {
        if(unsubscribe){
            unsubscribe();
        }
        reset();
        MapEvents.removeAllMapEvents();
        console.warn('Cancelling DrawFeature().');
    }

    function resetDrawFeatures() {
        if (backupArr.length) {
            drawLayer.clearLayers();
            referenceLayer.clearLayers();
        }
        if(drawer){
            drawer.disable();
        }
        backupArr = [];
        console.info('Reseting action.');
    }

    /******************************** helpers ****************************************/

    function createLayerDraw(obj) {

        obj.parcelLeafletLayer = EditorUtils.toLeafletLayer(obj.parcel.geometry);
        obj.parcelLeafletLayer.setStyle({
            "color": "#00ff00",
            "fill": false,
            "dashArray": "5, 10",
            "weight": 5,
            "opacity": 0.5
        });
        referenceLayer.addLayer(obj.parcelLeafletLayer);

        drawOptions.reference = obj.parcel.geometry;
        drawer = new L.Draw.Polygon.Cf(map, drawOptions);
        drawer.enable();

        map.on('draw:created', function (e) {
            var layer = e.layer || null;
            if(layer) { drawLayer.addLayer(layer)}
        });
    }

    function prepareForSave(){
        var geojson = drawLayer.toGeoJSON();
        return geojson;
    }

    function reset() {
        if (drawLayer) {
            drawLayer.clearLayers();
            referenceLayer.clearLayers();
            map.removeLayer(drawLayer);
        }
        if(drawer){
            //drawer.disable();
        }
        referenceLayer = null;
        drawLayer = null;
        map = null;
        layer = null;
        backupArr = [];
        currentLayer = null;
        unsubscribe = null;
    }
}

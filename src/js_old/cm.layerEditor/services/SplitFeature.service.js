/**
 * Created by ondrejzvara on 14.12.15.
 */

angular
    .module('cm.layerEditor')
    .factory('SplitFeature', SplitFeature);

SplitFeature.$inject = ['pubSub', 'CM_MAP_EDIT_CHANNEL', 'MapEvents', 'EditorUtils'];

function SplitFeature(pubSub, CM_MAP_EDIT_CHANNEL, MapEvents, EditorUtils) {
    var map = null;
    var editLayer = null;
    var drawLayer = null;
    var unsubscribe = null;
    var currentLayer = null;
    var featuresToSplit = [];
    var drawer = null;

    return {
        enableSplitFeature: enableSplitFeature,
        saveSplitFeature: saveSplitFeature,
        stornSplitFeature: stornSplitFeature,
        resetSplitFeature: resetSplitFeature
    };

/////////////////////////

    function enableSplitFeature(mapObj, currentL) {
        if (!unsubscribe) {
            map = mapObj;
            currentLayer = currentL;

            editLayer = new L.geoJson();
            editLayer.addTo(map);

            drawLayer = new L.geoJson();

            unsubscribe = pubSub.subscribe(CM_MAP_EDIT_CHANNEL.loadedFeaturesToEdit, splitFeatures, false);
            console.info('Enabling SplitFeatures()');
        }
    }

    function splitFeatures(obj) {
        if(featuresToSplit.length <= 0){
            createLayerToSplit(obj);
            featuresToSplit.push(obj);
            startSplitLineDraw(obj);
        }
    }

    function saveSplitFeature(callback) {
        if (typeof callback === "function" && featuresToSplit.length) {
            callback.call(this, currentLayer, getLinestring(), getParcelId());
            stornSplitFeature();
            console.info('Saving SplitFeatures() result.');
        } else {
            console.warn("Invalid callback function.");
        }
    }

    function stornSplitFeature() {
        if(unsubscribe){
            unsubscribe();
        }
        reset();
        MapEvents.removeAllMapEvents();
        console.warn('Cancelling SplitFeatures().');
    }

    function resetSplitFeature() {
        if (featuresToSplit.length) {
            editLayer.clearLayers();
            console.warn('Reseting SplitFeatures().');
        }
        reset();
    }

    function reset() {
        if(drawer){
            drawer.disable();
            drawer.clearAllLayers();
        }
        editLayer.clearLayers();
        featuresToSplit = [];
        unsubscribe = null;
    }

    function createLayerToSplit(obj){
        var ref = obj.parcel.geometry;
        obj.parcelLeafletLayer = EditorUtils.toLeafletLayer(ref);
        editLayer.addLayer(obj.parcelLeafletLayer);
        obj.reference = ref;
    }

    function startSplitLineDraw(obj){

        drawer = new L.Draw.Polyline.Cf(map, {
            reference: obj.reference,
            allowIntersection: false
        });
        drawer.enable();

        map.on('draw:created', function (e) {
            drawLayer.addLayer(e.layer);
            drawer.disable();
        });
    }

    function getLinestring(){
        var layer = drawLayer.getLayers();
        return layer[0].toGeoJSON();
    }

    function getParcelId(){
       return featuresToSplit[0].parcel.id
    }
}

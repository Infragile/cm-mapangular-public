/**
 * Created by ondrejzvara on 27.11.15.
 */

angular
    .module('cm.layerEditor')
    .factory('MergeFeatures', MergeFeatures);

MergeFeatures.$inject = ['pubSub', 'CM_MAP_EDIT_CHANNEL', 'MapEvents', 'EditorUtils'];

function MergeFeatures(pubSub, CM_MAP_EDIT_CHANNEL, MapEvents, EditorUtils) {
    var map = null;
    var editLayer = null;
    var unsubscribe = null;
    var currentLayer = null;
    var primaryFeatureId = null;
    var featuresToMerge = [];

    return {
        enableMergeFeatures: enableMergeFeatures,
        saveMergeFeatures: saveMergeFeatures,
        stornMergeFeatures: stornMergeFeatures,
        resetMergeFeatures: resetMergeFeatures
    };

/////////////////////////

    function enableMergeFeatures(mapObj, currentL) {
        if (!unsubscribe) {
            map = mapObj;
            currentLayer = currentL;

            editLayer = new L.geoJson();
            editLayer.addTo(map);

            unsubscribe = pubSub.subscribe(CM_MAP_EDIT_CHANNEL.loadedFeaturesToEdit, mergeFeatures, false);
            console.info('Enabling MergeFeatures()');
        }
    }

    function mergeFeatures(obj) {

        if(featuresToMerge && featuresToMerge.length > 0){
            if(!isFromSameLpis(obj)){
                console.warn('The parcel doesnt belong to the same LPIS block.');
                return;
            }
        }

        if(EditorUtils.inCollection(featuresToMerge, obj)){
            createLayerToMerge(obj);

            if(featuresToMerge.length < 1){
                primaryFeatureId = obj.parcel.id || null;
            }

            featuresToMerge.push(obj);
        }
    }

    function saveMergeFeatures(callback) {
        var pass = true;
        if (typeof callback !== "function"){
            console.warn("Invalid callback function.");
            pass = false;
        }

        if (featuresToMerge.length < 2){
            console.warn("Not enough items to merge.");
            pass = false;
        }

        if(pass){
            callback.call(this, currentLayer, primaryFeatureId, prepareForMerge(featuresToMerge));
            console.info('Saving MergeFeatures() result.');
        }

        stornMergeFeatures();
    }

    function stornMergeFeatures() {
        if(unsubscribe){
            unsubscribe();
        }
        reset();
        MapEvents.removeAllMapEvents();
        console.warn('Cancelling MergeFeatures().');
    }

    function resetMergeFeatures() {
        if (featuresToMerge.length) {
            editLayer.clearLayers();
            console.warn('Reseting MergeFeatures().');
        }
        reset();
    }

    function reset() {
        editLayer.clearLayers();
        featuresToMerge = [];
        unsubscribe = null;
    }

    function createLayerToMerge(obj){
        obj.parcelLeafletLayer = EditorUtils.toLeafletLayer(obj.parcel.geometry);
        editLayer.addLayer(obj.parcelLeafletLayer);

        obj.parcelLeafletLayer.on('click', function(e){
            var feature = e.layer;

            var toRemove = _.find(featuresToMerge, function(obj){
                return L.stamp(feature) === L.stamp(EditorUtils.getFeature(obj.parcelLeafletLayer));
            });

            if(!_.isEmpty(toRemove)){
                var index = featuresToMerge.indexOf(toRemove);
                featuresToMerge.splice(index, 1);
                console.log(featuresToMerge.length);
                editLayer.removeLayer(toRemove.parcelLeafletLayer);
            }
        });
    }

    function prepareForMerge(array){
        var result = [];
        _.each(array, function(obj){
            result.push(obj.parcel);
        });
        return result;
    }

    function isFromSameLpis(obj){
        return obj.parcel.lpis_id === featuresToMerge[0].parcel.lpis_id;
    }
}

/**
 * Created on 13.11.15.
 */

angular
    .module('cm.layerEditor')
    .factory('RemoveFeature', RemoveFeature);

RemoveFeature.$inject = ['pubSub', 'CM_MAP_EDIT_CHANNEL', 'MapEvents', 'EditorUtils'];

function RemoveFeature(pubSub, CM_MAP_EDIT_CHANNEL, MapEvents, EditorUtils) {
    var map = null;
    var editLayer = null;
    var unsubscribe = null;
    var currentLayer = null;
    var featuresToDelete = [];

    return {
        enableRemoveFeature: enableRemoveFeature,
        saveRemoveFeature: saveRemoveFeature,
        stornRemoveFeature: stornRemoveFeature,
        resetRemoveFeature: resetRemoveFeature
    };

/////////////////////////

    function enableRemoveFeature(mapObj, currentL) {
        if(!unsubscribe){
            map = mapObj;
            currentLayer = currentL;

            editLayer = new L.geoJson();
            editLayer.addTo(map);

            MapEvents.getLocationData(currentL);
            unsubscribe = pubSub.subscribe(CM_MAP_EDIT_CHANNEL.loadedFeaturesToEdit, removeFeature, false);
            console.info('Enabling RemoveFeature()');
        }
    }

    function removeFeature(obj) {
        if(EditorUtils.inCollection(featuresToDelete, obj)){
            createLayerToDelete(obj);
            featuresToDelete.push(obj);
            console.log(featuresToDelete.length)
        }
    }

    function saveRemoveFeature(callback) {
        if (typeof callback === "function" && featuresToDelete.length) {
            callback.call(this, currentLayer, prepareForDelete(featuresToDelete));
            stornRemoveFeature();
            console.info('Saving RemoveFeature() result.');
        } else {
            console.warn("Invalid callback function.");
        }
    }

    function stornRemoveFeature() {
        if(unsubscribe){
            unsubscribe();
        }
        reset();
        MapEvents.removeAllMapEvents();
        console.warn('Cancelling RemoveFeature().');
    }

    function resetRemoveFeature(){
        reset();
        console.warn('Reseting RemoveFeature().');
    }

    function reset(){
        editLayer.clearLayers();
        featuresToDelete = [];
        unsubscribe = null;
    }

    function createLayerToDelete(obj){

        obj.primLeafletLayer = EditorUtils.toLeafletLayer(obj.primGeoJson);
        editLayer.addLayer(obj.primLeafletLayer);

        obj.primLeafletLayer.on('click', function(e){
            var feature = e.layer;

            var toRemove = _.find(featuresToDelete, function(obj){
                return L.stamp(feature) === L.stamp(EditorUtils.getFeature(obj.primLeafletLayer));
            });

            if(!_.isEmpty(toRemove)){
                var index = featuresToDelete.indexOf(toRemove);
                featuresToDelete.splice(index, 1);
                console.log(featuresToDelete.length);
                editLayer.removeLayer(toRemove.primLeafletLayer);
            }
        });
    }

    function prepareForDelete(array){
        var result = [];
        _.each(array, function(obj){
            result.push(obj.primGeoJson);
        });
        return result;
    }
}
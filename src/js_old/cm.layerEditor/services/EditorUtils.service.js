/**
 * Created by ondrejzvara on 7.12.15.
 */

angular
    .module('cm.layerEditor')
    .factory('EditorUtils', EditorUtils);

EditorUtils.$inject = [];

function EditorUtils() {

    return {
        inCollection: inCollection,
        toLeafletLayer: toLeafletLayer,
        getFeature: getFeature
    };

/////////////////////////

    function inCollection(srcArr, obj){
        return _.isEmpty(_.find(srcArr, function(o){ return o.properties.id === obj.properties.id }));
    }

    function toLeafletLayer(geoJson){
        return L.GeoJSON.geometryToLayer(geoJson);
    }

    function getFeature(obj){
        return obj._layers ? obj._layers[Object.keys(obj._layers)[0]] : obj;
    }

}
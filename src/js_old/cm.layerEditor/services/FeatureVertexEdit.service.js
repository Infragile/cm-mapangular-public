/**
 * Created on 13.11.15.
 */

angular
    .module('cm.layerEditor')
    .factory('FeatureVertexEdit', FeatureVertexEdit);

FeatureVertexEdit.$inject = ['pubSub', 'CM_MAP_EDIT_CHANNEL', 'MapEvents', 'EditorUtils'];

function FeatureVertexEdit(pubSub, CM_MAP_EDIT_CHANNEL, MapEvents, EditorUtils) {
    var map = null;
    var editLayer = null;
    var referenceLayer = null;
    var backupArr = [];
    var currentLayer = null;
    var unsubscribe;

    return {
        enableFeatureVertexEdit: enableFeatureVertexEdit,
        saveFeatureVertexEdit: saveFeatureVertexEdit,
        stornFeatureVertexEdit: stornFeatureVertexEdit,
        resetFeatureVertexEdit: resetFeatureVertexEdit
    };

/////////////////////////

    function enableFeatureVertexEdit(mapObj, currentL, callback) {
        if(!unsubscribe){
            map = mapObj;
            currentLayer = currentL;

            editLayer = new L.geoJson();
            editLayer.addTo(map);

            referenceLayer = new L.geoJson();
            referenceLayer.addTo(map);

            unsubscribe = pubSub.subscribe(CM_MAP_EDIT_CHANNEL.loadedFeaturesToEdit, featureVertexEdit, false);
            console.info('Enabling FeatureVertexEdit()');
        }
    }

    function featureVertexEdit(obj) {
        if(EditorUtils.inCollection(backupArr, obj)){
            createLayerToEdit(obj);
            backupArr.push(obj);
        }
    }

    function saveFeatureVertexEdit(callback) {
        if (typeof callback !== "function"){
            console.warn("Invalid callback function.");
            return;
        }

        if(!backupArr.length) {
            console.warn('Nothing to save!');
            return;
        }

        if(!validateGeometries(backupArr)) {
            console.warn('Invalid geometries!');
            return;
        }

        callback.call(this, currentLayer, prepareForSave(backupArr));
        console.info('Saving FeatureVertexEdit() result.');
        stornFeatureVertexEdit();
    }

    function stornFeatureVertexEdit() {
        if(unsubscribe){
            unsubscribe();
        }
        reset();
        MapEvents.removeAllMapEvents();
        console.warn('Cancelling FeatureVertexEdit().');
    }

    function resetFeatureVertexEdit() {
        if (backupArr.length) {
            editLayer.clearLayers();
            referenceLayer.clearLayers();
            _.each(backupArr, function (obj) {
                createLayerToEdit(obj);
            });
            console.info('Reseting action.');
        }
    }

    /******************************** helpers ****************************************/

    function createLayerToEdit(obj) {
        obj.parcelLeafletLayer = EditorUtils.toLeafletLayer(obj.parcel.geometry);
        editLayer.addLayer(obj.parcelLeafletLayer);
        var primaryFeature = EditorUtils.getFeature(obj.parcelLeafletLayer);

        obj.lpisLeafletLayer =  EditorUtils.toLeafletLayer(obj.lpis.geometry);
        obj.lpisLeafletLayer.setStyle({
            "color": "#00ff00",
            "fill": false,
            "dashArray": "5, 10",
            "weight": 5,
            "opacity": 0.5
        });
        referenceLayer.addLayer(obj.lpisLeafletLayer);
        var referenceFeature = EditorUtils.getFeature(obj.lpisLeafletLayer);

        primaryFeature.editing = new L.Edit.Poly.Cf(primaryFeature, {
            withinPolygon: {
                apply: true,
                referencePoly: referenceFeature,
                alertStyle: {
                    color: '#ed0000',
                    fillColor: "#ed0000"
                }
            }
        });
        primaryFeature.editing.enable();
    }

    function validateGeometries(array){
        var pass = true;
        var invalidItems = [];
        _.each(array, function(obj){
            var primaryFeature = EditorUtils.getFeature(obj.parcelLeafletLayer);
            if(!primaryFeature.editing.isValid()){
                invalidItems.push(obj);
            }
        });
        if(!_.isEmpty(invalidItems)){
            pass = false;
        }
        return pass;
    }

    function reset() {
        if (editLayer && map) {
            editLayer.clearLayers();
            referenceLayer.clearLayers();
            map.removeLayer(editLayer);
        }
        referenceLayer = null;
        editLayer = null;
        map = null;
        backupArr = [];
        currentLayer = null;
        unsubscribe = null;
    }

    function prepareForSave(array){
        return array;
    }


}
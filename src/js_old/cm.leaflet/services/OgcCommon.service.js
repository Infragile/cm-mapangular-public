angular
        .module('cm.leaflet')
        .factory('OgcCommon', OgcCommon);

OgcCommon.$inject = [];

function OgcCommon() {
    return {
        getFilter: getFilter
    };

    function getFilter(filterDef, toplevel) {
        var filter = '';

        if (toplevel) {
            filter += '<Filter xmlns:gml="http://www.opengis.net/gml">';
        }

        switch (filterDef.type) {
            case 'Or':
                filter += '<Or>';
                if (filterDef.args && angular.isArray(filterDef.args)) {
                    angular.forEach(filterDef.args, function (value) {
                        filter += getFilter(value, false);
                    });
                } else {
                    filter += getFilter(filterDef.arg1, false);
                    filter += getFilter(filterDef.arg2, false);
                }
                filter += '</Or>';
                break;
            case 'And':
                filter += '<And>';
                if (filterDef.args && angular.isArray(filterDef.args)) {
                    angular.forEach(filterDef.args, function (value) {
                        filter += getFilter(value, false);
                    });
                } else {
                    filter += getFilter(filterDef.arg1, false);
                    filter += getFilter(filterDef.arg2, false);
                }
                filter += '</And>';
                break;
            case 'Not':
                filter += '<Not>';
                filter += getFilter(filterDef.arg, false);
                filter += '</Not>';
                break;
            case 'PropertyIsEqualTo':
            case 'PropertyIsGreaterThan':
                filter += '<' + filterDef.type + '>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '<Literal>';
                filter += filterDef.literal;
                filter += '</Literal>';
                filter += '</' + filterDef.type + '>';
                break;
            case 'PropertyIsNull':
                filter += '<PropertyIsNull>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '</PropertyIsNull>';
                break;
            case 'BBOX':
                filter += '<BBOX>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '<gml:Box><gml:coordinates>';
                filter += filterDef.coordinates;
                filter += '</gml:coordinates></gml:Box>';
                filter += '</BBOX>';
                break;
            case 'XML':
                filter += filterDef.value;
                break;
            default:
                console.log('Unsupported filter type: ' + filterDef.type);
        }

        if (toplevel) {
            filter += '</Filter>';
        }

        return filter;
    }
}
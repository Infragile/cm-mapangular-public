/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.common', [
    'cm.common.storage',
    'cm.common.layer',
    'cm.common.utils'
]);
/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.common.layer', [])

    .constant('CM_MAP_LAYER_TYPE', {
        node: 'node',
        wms: 'wms',
        wfs: 'wfs'
    })

    .constant('CM_MAP_POPUP_TYPE', {
        none: 'none',
        static: 'static',
        dynamic: 'dynamic'
    });

/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.common.storage', []);
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular.module('cm.common.utils', []);
/**
 * Created by ondrejzvara on 27.5.16.
 */

angular.module('cm.directives', ['cm.common']);
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular.module('cm.draw', ['cm.common'])

    .constant('CM_DRAW_STYLES', {
        polygon: {
            clickable: false,
            color: '#ABE67E',
            weight: 2,
            opacity: 0.9,
            fill: false,
            lineCap: 'round',
            lineJoin: 'round',
            radius: 4
        },
        polyline: {
            clickable: false,
            color: '#ABE67E',
            weight: 2,
            opacity: 0.9,
            fill: false,
            lineCap: 'round',
            lineJoin: 'round',
            radius: 4
        }
    });
/**
 * Created by ondrejzvara on 11.5.16.
 */

angular.module('cm.edit', ['cm.common']);
/**
 * Created by ondrejzvara on 10.5.16.
 */

angular.module('cm.event', ['cm.common']);

/**
 * Created on 12.11.15.
 */

angular.module('cm.map', ['cm.common']);

/**
 * Created by ondrejzvara on 16.5.16.
 */

angular.module('cm.measure', ['cm.common', 'cm.draw'])

    .constant('CM_MAP_UNIT', {
        km: {
            code: 'km',
            name: 'kilometers'
        },
        m: {
            code: 'm',
            name: 'meters'
        }
    });
/**
 * Created by ondrejzvara on 11.5.16.
 */

angular.module('cm.navigate', ['cm.common']);
/**
 * Created by ondrejzvara on 26.5.16.
 */

angular.module('cm.storage', ['cm.common']);

angular
    .module('cm.common.layer')
    .factory('CmFullscreen', CmFullscreen);

CmFullscreen.$inject = [];

function CmFullscreen() {
    
    var service = {
        getFullscreenControl: getFullscreenControl
    };

    var FullscreenControl = L.Control.FullScreen.extend({

        toggleFullScreen: function (map) {

            map._exitFired = false;
            if (map._isFullscreen) {
                
                if (fullScreenApi.supportsFullScreen && !this.options.forcePseudoFullscreen) {
                    fullScreenApi.cancelFullScreen(map._container);
                } else {
                    L.DomUtil.removeClass(map._container, 'leaflet-pseudo-fullscreen');
                }
                map.invalidateSize();
                map.fire('exitFullscreen');
                map._exitFired = true;
                map._isFullscreen = false;
            }
            else {
                if (fullScreenApi.supportsFullScreen && !this.options.forcePseudoFullscreen) {
                    fullScreenApi.requestFullScreen(map._container);
                } else {
                    L.DomUtil.addClass(map._container, 'leaflet-pseudo-fullscreen');
                }
                map.invalidateSize();
                map.fire('enterFullscreen');
                map._isFullscreen = true;
            }
        }
        
    });

    return service;

    function getFullscreenControl(opts) {
        return new FullscreenControl(opts);
    }
}
angular
    .module('cm.common.layer')
    .factory('CmLayerFactory', CmLayerFactory);

CmLayerFactory.$inject = [
    'CM_MAP_LAYER_TYPE', 'LeafletProjGeoJsonWfs', 'LeafletTileLayerWfs', 
    'LeafletTileLayerWmsFiltered', 'LeafletLayerGroup'
];

function CmLayerFactory(
        CM_MAP_LAYER_TYPE, LeafletProjGeoJsonWfs, LeafletTileLayerWfs, 
        LeafletTileLayerWmsFiltered, LeafletLayerGroup
) {

    return {
        createLayer: createLayer,
        createLayerGroup: createLayerGroup
    };

    ///////////////////////////////////////////////////////////////////////////
    
    // create layer from configuration
    function createLayer(config, id) {
        switch (config['type']) {
            case CM_MAP_LAYER_TYPE.wms:
                config.id = id;
                return createWmsLayer(config);

            case CM_MAP_LAYER_TYPE.wfs:
                config.id = id;
                return createGeoJsonWfsLayer(config);

            case 'wfs-tiled':
                config.id = id;
                return createTileWfsLayer(config);

            default:
                throw new Error('Unknown layer type ' + config.type);
        }
    }
    
    /**
     * TODO check id param !!!
     */
    function createLayerGroup(layers, id) {
        return LeafletLayerGroup.getLayerGroup(layers, id);
    }
    
    ///////////////////////////////////////////////////////////////////////////

    // create GeoJSON layer
    function createGeoJsonWfsLayer(layerConfig) {
        return LeafletProjGeoJsonWfs.getLayer(
            layerConfig['url'],
            layerConfig);
    }

    // create tileds WFS layer
    function createTileWfsLayer(layerConfig) {
        return LeafletTileLayerWfs.getLayer(
            layerConfig['url'],
            layerConfig);
    }
    
    // create WMS layer
    function createWmsLayer(layerConfig) {
        return LeafletTileLayerWmsFiltered.getLayer(
                layerConfig['url'],
                layerConfig);
    }
}

angular
    .module('cm.common.layer')
    .factory('LeafletLayerGroup', LeafletLayerGroup);

LeafletLayerGroup.$inject = [];

/**
 * Extension of LeafletGroup, added id attribute.
 * @returns {LeafletLayerGroup.service}
 */
function LeafletLayerGroup() {
    var service = {
        getLayerGroup: getLayerGroup
    };

    var LayerGroup = L.LayerGroup.extend({
        initialize: function (layers, id) {
            if(id) {
                throw new Error("Missing layer group id option in initialization.");
            }
            
            this._id = id;
            
            L.LayerGroup.prototype.initialize.call(this, layers);
        }
    });

    return service;

    function getLayerGroup(layers, id) {
        return new LayerGroup(layers, id);
    }
}
angular
    .module('cm.common.layer')
    .factory('LeafletPopup', LeafletPopup);

LeafletPopup.$inject = ['$compile', '$timeout'];

/**
 * Leaflet popup extend with ability to compile angular template
 */
function LeafletPopup($compile, $timeout) {
    var service = {
        get: getPopup
    };

    var Popup = L.Popup.extend({
        options: {
            scope: null
        },
        onAdd: function (map) {
            // create an empty element a jQuery object
            var contentElement = angular.element('<div/>');

            // read Angular template code with structured HTML to jQuery object
            contentElement.append(this._content);

            // compile Angular template and apply on scope
            $compile(contentElement)(this.options.scope);

            // convert jQuery object to DOM object and stock it
            this._content = contentElement[0];

            // apply scope
            var scope = this.options.scope;
            $timeout(function () {
                scope.$apply();
            });

            L.Popup.prototype.onAdd.call(this, map);
        }
    });

    return service;

    function getPopup(options, source) {
        return new Popup(options, source);
    }
}
angular
        .module('cm.common.layer')
        .factory('LeafletProjGeoJsonWfs', LeafletProjGeoJsonWfs);

LeafletProjGeoJsonWfs.$inject = ['$http', 'LeafletWmsCommon', 'OgcCommon'];

/**
 * GeoJSON WFS layer
 *
 * Based on code from CAN by Martin Tesar
 * Modified by Petr Sobeslavsky to work with L.Proj.GeoJSON - GeoJSON layer with custom CRS support
 */
function LeafletProjGeoJsonWfs($http, LeafletWmsCommon, OgcCommon) {
    var service = {
        getLayer: getLayer
    };

    var ProjGeoJsonWfs = L.Proj.GeoJSON.extend({
        defaultWfsParams: {
            service: 'WFS',
            request: 'GetFeature',
            version: '2.0.0',
            typeNames: '',
            outputFormat: 'text/javascript',
            count: 100
        },
        defaultWfsFormatParams: {
            callback: 'JSON_CALLBACK'
        },
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this.state = {
                dataBounds: null,
                newBounds: null,
                zoom: null,
                newZoom: null,
                featuresFetched: 0,
                limitReached: false,
                currentDownloadChain: 0
            };

            this._id = options.id;
            this._url = url;
            this._wfsParams = L.extend(this.defaultWfsParams, options.wfs);
            this._filter = options.filter;
            this._wfsFormatParams = L.extend(this.defaultWfsFormatParams, options.format);
            this._geometryColumn = options.geometryColumn || 'geom';
            this._limit = options.limit || 2000;
            this._httpService = $http;
            this._chunked = false;
            this._unbounded = false;

            if (options.unbounded) {
                this._unbounded = true;
            }

            if (options.chunked) {
                // only set startIndex if we are downloading data in chunks
                // it requires layers to provide natural ordering, so we cannot set it by default
                // for all layers
                this._wfsParams.startIndex = 0;
                this._chunked = true;
            }

            if (options.dataCallback) {
                this._dataCallback = options.dataCallback;
            }

            L.GeoJSON.prototype.initialize.call(this, null, options.geojson);
        },
        loadFeatureData: function (latlng) {
            var filter = null;
            if (this._filter) {
                filter = OgcCommon.getFilter(this._filter, true);
            }

            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this._wfsParams['typeNames'], filter, this._wfsParams['feature_count']);
        },
        onAdd: function (map) {
            L.GeoJSON.prototype.onAdd.call(this, map);

            this._crs = this.options.crs || map.options.crs;
            this._uppercase = this.options.uppercase || false;

            this._map = map;
            map.on({
                moveend: this._updateDisplayedMap
            }, this);
            this._updateDisplayedMap();
        },
        _updateDisplayedMap: function () {
            if (!this._map) {
                return;
            }
            var bounds = this._map.getBounds();
            var zoom = this._map.getZoom();

            this.update(bounds, zoom);
        },
        getExtendedBounds: function (bounds) {
            // extend bounds by 10 % in all directions
            var sw = bounds.getSouthWest();
            var ne = bounds.getNorthEast();
            var latSize = ne.lat - sw.lat;
            var lngSize = ne.lng - sw.lng;
            ne.lat = ne.lat + (latSize * 0.05);
            sw.lat = sw.lat - (latSize * 0.05);
            ne.lng = ne.lng + (lngSize * 0.05);
            sw.lng = sw.lng - (lngSize * 0.05);
            var extendedBounds = L.latLngBounds(sw, ne);
            return extendedBounds;
        },
        update: function (viewBounds, newZoom) {
            if (!this._map) {
                return;
            }
            if (this._needsUpdate(viewBounds, newZoom)) {
                this.state.newBounds = viewBounds;
                this.state.newZoom = newZoom;
                this._getFeatures(viewBounds);
            }
        },
        refresh: function() {
            this._getFeatures(this.state.newBounds);
        },
        _getFormatString: function (obj) {
            var params = [];
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    params.push(i + ':' + obj[i]);
                }
            }
            return params.join(';');
        },
        _getRequestUrl: function (_wfsParams, bounds) {
            var extendedBounds = this.getExtendedBounds(bounds);
            var nw = this._crs.project(extendedBounds.getNorthWest());
            var se = this._crs.project(extendedBounds.getSouthEast());
            var bbox = nw.x + ',' + se.y + ' ' + se.x + ',' + nw.y;

            // we need to supply BBOX as part of filter, because we cannot specify both bbox and filter
            var requestFilter;

            if (!this._unbounded) {
                var bboxFilter = {
                    type: 'BBOX',
                    propertyName: this._geometryColumn,
                    coordinates: bbox
                };

                if (this._filter) {
                    requestFilter = {
                        type: 'And',
                        arg1: bboxFilter,
                        arg2: this._filter
                    };
                } else {
                    requestFilter = bboxFilter;
                }
            } else {
                requestFilter = this._filter;
            }
            _wfsParams['filter'] = OgcCommon.getFilter(requestFilter, true);

            // prepare format params
            var fo = this._getFormatString(this._wfsFormatParams);

            return this._url +
                    L.Util.getParamString(_wfsParams, this._url, this._uppercase) +
                    (this._uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + fo;
        },
        _getFeatures: function (bounds) {
            var _wfsParams = _.cloneDeep(this._wfsParams);

            this.state.currentDownloadChain += 1;
            var downloadChain = this.state.currentDownloadChain;

            this.state.featuresFetched = 0;
            this.state.limitReached = false;
            this._fetchFeatures(_wfsParams, bounds, downloadChain);
        },
        _fetchFeatures: function (_wfsParams, bounds, downloadChain) {
            var url = this._getRequestUrl(_wfsParams, bounds);

            var _this = this;
            this._httpService.jsonp(url)
                    .success(function (data) {
                        _this._FeaturesFetchedCallback(data, _wfsParams, bounds, downloadChain);
                    })
                    .error(function () {
                        _this._updateDisplayedMapFailCallback();
                    });
        },
        _FeaturesFetchedCallback: function (data, _wfsParams, bounds, downloadChain) {
            // if we received data from an old call, discard them
            if (downloadChain < this.state.currentDownloadChain) {
                return;
            }

            if (this.state.featuresFetched === 0) {
                // delete old data upon reception of the first batch of new data
                this.clearLayers();
            }

            this.state.featuresFetched += data.features.length;
            //this.addData(data);

            if (this._dataCallback) {
                this._dataCallback(data);
            }

            if (this.state.featuresFetched >= data.totalFeatures) {
                this._updateDisplayedMapDoneFinish(data);
                return;
            }

            if (this.state.featuresFetched >= this._limit) {
                this.state.limitReached = true;
                this._updateDisplayedMapDoneFinish(data);
                return;
            }

            if (this._chunked) {
                _wfsParams.startIndex += _wfsParams.count;
                this._fetchFeatures(_wfsParams, bounds, downloadChain);
            }
        },
        _updateDisplayedMapFailCallback: function () {

        },
        _updateDisplayedMapDoneFinish: function () {
            this.state.dataBounds = this.state.newBounds;
            this.state.zoom = this.state.newZoom;
        },
        _needsUpdate: function (viewBounds, newZoom) {
            var oldBounds = this.state.dataBounds;
            var oldZoom = this.state.zoom;

            var needs = false;
            if ((newZoom > oldZoom) && this.state.limitReached) {
                needs = true;
            }
            if (!(oldBounds && oldBounds.contains(viewBounds))) {
                needs = true;
            }

            return needs;
        }
    });

    return service;

    function getLayer(url, options) {
        return new ProjGeoJsonWfs(url, options);
    }
}
angular
        .module('cm.common.layer')
        .factory('LeafletTileLayerWfs', LeafletTileLayerWfs);

LeafletTileLayerWfs.$inject = ['$http', 'OgcCommon', 'LeafletWmsCommon'];

function LeafletTileLayerWfs($http, OgcCommon, LeafletWmsCommon) {
    var service = {
        getLayer: getLayer
    };

    var WfsTileLayer = L.TileLayer.extend({
        defaultWfsParams: {
            service: 'WFS',
            request: 'GetFeature',
            version: '2.0.0',
            typeNames: '',
            outputFormat: 'text/javascript',
            startIndex: 0,
            count: 100
        },
        defaultWfsFormatParams: {
            callback: 'JSON_CALLBACK'
        },
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this._id = options.id;
            this._url = url;
            this._wfsParams = L.extend(this.defaultWfsParams, options.wfs);
            this._filter = options.filter;
            this._geoJson = options.geojson;
            this._wfsFormatParams = L.extend(this.defaultWfsFormatParams, options.format);
            this._geometryColumn = options.geometryColumn || 'geom';
            this._idColumn = options.idColumn || 'id';
            this._limit = options.limit || 2000;

            L.setOptions(this, options);
            this._httpService = $http;
            this._features = {};
        },
        onAdd: function (map) {
            this._crs = this.options.crs || map.options.crs;

            L.TileLayer.prototype.onAdd.call(this, map);
        },
        _getFormatString: function (obj) {
            var params = [];
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    params.push(i + ':' + obj[i]);
                }
            }
            return params.join(';');
        },
        _getRequestUrl: function (_wfsParams, coords) {
            // we need to supply BBOX as part of filter, because we cannot specify both bbox and filter
            var nw = coords.getNorthWest();
            var ne = coords.getNorthEast();
            var se = coords.getSouthEast();
            var sw = coords.getSouthWest();

            var polygon = nw.lng + ',' + nw.lat + ' '
                    + ne.lng + ',' + ne.lat + ' '
                    + se.lng + ',' + se.lat + ' '
                    + sw.lng + ',' + sw.lat + ' '
                    + nw.lng + ',' + nw.lat;

            var bboxFilter = {
                type: 'XML',
                value: '<Intersects><PropertyName>' + this._geometryColumn + '</PropertyName>'
                        + '<gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>'
                        + polygon
                        + '</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></Intersects>'
            };
            var requestFilter;

            if (this._filter) {
                requestFilter = {
                    type: 'And',
                    arg1: bboxFilter,
                    arg2: this._filter
                };
            } else {
                requestFilter = bboxFilter;
            }
            _wfsParams['filter'] = OgcCommon.getFilter(requestFilter, true);

            // prepare format params
            var fo = this._getFormatString(this._wfsFormatParams);

            return this._url +
                    L.Util.getParamString(_wfsParams, this._url, this._uppercase) +
                    (this._uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + fo;
        },
        getTileUrl: function (tilePoint) {
            var map = this._map;
            var tileSize = this.options.tileSize;
            var nwPoint = tilePoint.multiplyBy(tileSize);
            var sePoint = nwPoint.add([tileSize, tileSize]);
            var nw = this._crs.project(map.unproject(nwPoint, tilePoint.z));
            var se = this._crs.project(map.unproject(sePoint, tilePoint.z));
            var sw = L.latLng(nw.y, se.x);
            var ne = L.latLng(se.y, nw.x);
            var coords = L.latLngBounds(sw, ne);

            var _wfsParams = _.cloneDeep(this._wfsParams);

            var url = this._getRequestUrl(_wfsParams, coords);

            return url;
        },
        _loadTile: function (tile, tilePoint) {
            var url = this.getTileUrl(tilePoint);

            var _this = this;
            this._httpService.jsonp(url)
                    .success(function (data) {
                        //                    var featureLayer = L.Proj.geoJson(data, this._geoJson);
                        //                            tile.addLayer(featureLayer);
                        //                    tile.addData(data);
                        // TODO: refactor to separate function
                        for (var i = 0; i < data.features.length; i++) {
                            // TODO: replace id with idColumn
                            if (!(data.features[i].id in _this._features)) {
                                _this._features[data.features[i].id] = true;
                                // add crs to feature data
                                if ("crs" in data) {
                                    data.features[i].crs = data.crs;
                                }
                                var featureLayer = L.Proj.geoJson(data.features[i], _this._geoJson);
                                tile.addLayer(featureLayer);
                            }
                        }

                        // TODO: record feature id in tile
                    });

            this.fire('tileloadstart', {
                tile: tile,
                url: url
            });
        },
        _addTile: function (tilePoint) {
            // create new tile
            // skip _getTail and _createTile since we don't reuse tiles
            var tile = L.layerGroup(); //L.Proj.geoJson([], this._geoJson);

            this._tiles[tilePoint.x + ':' + tilePoint.y] = tile;

            this._loadTile(tile, tilePoint);

            this._map.addLayer(tile);
        },
        loadFeatureData: function (latlng) {
            var filter = null;
            if (this._filter) {
                filter = OgcCommon.getFilter(this._filter, true);
            }

            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this._wfsParams['typeNames'], filter, this._wfsParams['feature_count']);
        }
    });

    return service;

    function getLayer(url, options) {
        return new WfsTileLayer(url, options);
    }
}
angular
        .module('cm.common.layer')
        .factory('LeafletTileLayerWmsFiltered', LeafletTileLayerWmsFiltered);

LeafletTileLayerWmsFiltered.$inject = ['$http', 'OgcCommon', 'LeafletWmsCommon'];

/**
 * WMS layer extended with WFS-style filtering capabilities
 */
function LeafletTileLayerWmsFiltered($http, OgcCommon, LeafletWmsCommon) {
    var service = {
        getLayer: getLayer
    };

    var WmsTileLayer = L.TileLayer.WMS.extend({
        initialize: function (url, options) {
            if(!options || !options.id) {
                throw new Error("Missing layer id option in initialization.");
            }
            
            this._id = options.id;
            this.wmsParams = L.extend(options.wms);

            if (options.filter) {
                this.wmsParams['filter'] = OgcCommon.getFilter(options.filter, true);
            }

            this._httpService = $http;

            L.TileLayer.WMS.prototype.initialize.call(this, url, this.wmsParams);

            if (options.hasOwnProperty('removeWmsParams')) {
                for (var i = 0; i < options['removeWmsParams'].length; i++) {
                    this.removeWmsParam(options['removeWmsParams'][i]);
                }
            }
        },
        
        loadFeatureData: function (latlng) {
            return LeafletWmsCommon.loadFeatureData(latlng,
                    this._url, this._uppercase, this._httpService, this._map, this._crs,
                    this.wmsParams['layers'], this.wmsParams['filter'], this.wmsParams['feature_count']).then(function (response) {
                return response;
            });
        },
        
        removeWmsParam: function (param) {
            if (this.wmsParams.hasOwnProperty(param)) {
                delete this.wmsParams[param];
            }
        }
    });

    return service;

    function getLayer(url, options) {
        return new WmsTileLayer(url, options);
    }
}
angular
        .module('cm.common.layer')
        .factory('LeafletWmsCommon', LeafletWmsCommon);

LeafletWmsCommon.$inject = [];

function LeafletWmsCommon() {
    return {
        loadFeatureData: loadFeatureData
    };

    function loadFeatureData(latlng, url, uppercase, httpservice, map, crs, layers, filter, featureCount) {
        // get location of features in pixels, map size and displayed map bounds
        // note: we can't use map.getBounds() - it doesn't returns bounds
        // of visible area
        var featurePoint = map.latLngToContainerPoint(latlng);
        var mapSize = map.getSize();
        var nwPoint = [0, 0];
        var sePoint = [mapSize.x, mapSize.y];
        var nwLatlng = map.containerPointToLatLng(nwPoint);
        var seLatlng = map.containerPointToLatLng(sePoint);
        var nw = crs.project(nwLatlng);
        var se = crs.project(seLatlng);
        // TODO this._wmsVersion ??
        var bbox = (this._wmsVersion >= 1.3 && crs === L.CRS.EPSG4326 ?
                [se.y, nw.x, nw.y, se.x] :
                [nw.x, se.y, se.x, nw.y]).join(',');

        var reqParams = [];

        reqParams['service'] = 'WMS';
        reqParams['version'] = '1.1.1';
        reqParams['request'] = 'GetFeatureInfo';
        reqParams['info_format'] = 'text/javascript';
        reqParams['x'] = featurePoint.x;
        reqParams['y'] = featurePoint.y;
        reqParams['srs'] = crs.code;
        reqParams['bbox'] = bbox;
        reqParams['width'] = mapSize.x;
        reqParams['height'] = mapSize.y;
        reqParams['layers'] = layers;
        reqParams['query_layers'] = layers;
        reqParams['feature_count'] = (featureCount ? featureCount : 1);
        if (filter) {
            reqParams['filter'] = filter;
        }

        var reqUrl = url + L.Util.getParamString(reqParams, url, uppercase) +
                (uppercase ? '&FORMAT_OPTIONS=' : '&format_options=') + "callback:JSON_CALLBACK";

        return httpservice.jsonp(reqUrl);
    }
}
angular
        .module('cm.common.layer')
        .factory('OgcCommon', OgcCommon);

OgcCommon.$inject = ['$log'];

function OgcCommon($log) {
    return {
        getFilter: getFilter
    };

    function getFilter(filterDef, toplevel) {
        var filter = '';

        if (toplevel) {
            filter += '<Filter xmlns:gml="http://www.opengis.net/gml">';
        }

        switch (filterDef.type) {
            case 'Or':
                filter += '<Or>';
                if (filterDef.args && _.isArray(filterDef.args)) {
                    _.forEach(filterDef.args, function (value) {
                        filter += getFilter(value, false);
                    });
                } else {
                    filter += getFilter(filterDef.arg1, false);
                    filter += getFilter(filterDef.arg2, false);
                }
                filter += '</Or>';
                break;
            case 'And':
                filter += '<And>';
                if (filterDef.args && _.isArray(filterDef.args)) {
                    _.forEach(filterDef.args, function (value) {
                        filter += getFilter(value, false);
                    });
                } else {
                    filter += getFilter(filterDef.arg1, false);
                    filter += getFilter(filterDef.arg2, false);
                }
                filter += '</And>';
                break;
            case 'Not':
                filter += '<Not>';
                filter += getFilter(filterDef.arg, false);
                filter += '</Not>';
                break;
            case 'PropertyIsEqualTo':
            case 'PropertyIsGreaterThan':
                filter += '<' + filterDef.type + '>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '<Literal>';
                filter += filterDef.literal;
                filter += '</Literal>';
                filter += '</' + filterDef.type + '>';
                break;
            case 'PropertyIsNull':
                filter += '<PropertyIsNull>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '</PropertyIsNull>';
                break;
            case 'BBOX':
                filter += '<BBOX>';
                filter += '<PropertyName>';
                filter += filterDef.propertyName;
                filter += '</PropertyName>';
                filter += '<gml:Box><gml:coordinates>';
                filter += filterDef.coordinates;
                filter += '</gml:coordinates></gml:Box>';
                filter += '</BBOX>';
                break;
            case 'XML':
                filter += filterDef.value;
                break;
            default:
                $log.log('Unsupported filter type: ' + filterDef.type);
        }

        if (toplevel) {
            filter += '</Filter>';
        }

        return filter;
    }
}
angular
        .module('cm.common.storage')
        .factory('CmLayersStorage', CmLayersStorage);

CmLayersStorage.$inject = ['CmStorageHelpers'];

function CmLayersStorage(CmStorageHelpers) {

    var layers = {};

    /**
     * Service CmLayersStorage
     * @namespace CmLayersStorage
     */
    var service = {
        addLayerToLayers: addLayerToLayers,
        addLayerGroupToLayers: addLayerGroupToLayers,
        getLayers: getLayers,
        getLayerById: getLayerById,
        removeLayerFromLayers: removeLayerFromLayers,
        removeLayerGroupFromLayers: removeLayerGroupFromLayers,
        resetLayers: resetLayers,
        setLayers: setLayers
    };
    
    return service;

    /////////////////////////
    
    /**
     * Adds Leaflet layer to given layers variable. Returns layers variable.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} leafletLayer
     * @returns {Array}
     */
    function addLayerToLayers(layers, leafletLayer) {
        if(!_.isArray(layers)) {
            layers = [];
        }
        
        layers.push(leafletLayer);
        
        return layers;
    }
    
    /**
     * Adds Leaflet group layer to given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} arrayOfLeafletLayers
     * @returns {Array}
     */
    function addLayerGroupToLayers(layers, arrayOfLeafletLayers) {
        return addLayerToLayers(layers, arrayOfLeafletLayers);
    }

    /**
     * Returns deep copy of stored layers.
     * @memberof CmLayersStorage
     * @param {type} mapId
     * @returns {unresolved}
     */
    function getLayers(mapId) {
        return _getLayers(mapId).then(function (layers) {
            return _.cloneDeep(layers);
        });
    }
    
    /**
     * Returns Leaflet layer object with given id.
     * @memberof CmLayersStorage
     * @param {Array} layers
     * @param {type} id
     * @returns {mixed}
     */
    function getLayerById(layers, id) {
        var layer;
        
        if(_.isArray(layers) && id) {
            _.forEach(layers, function(value) {
                if(value._id === id) {
                    layer = value;
                }
                
                if(value instanceof L.LayerGroup) {
                    value.eachLayer(function(l) {
                        if(l._id === id) {
                            layer = l;
                        }
                    });
                }
            });
        }
        
        return layer;
    }
    
    /**
     * Removes Leaflet layer from given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} layerId
     * @returns {unresolved}
     */
    function removeLayerFromLayers(layers, layerId) {
        if(_.isArray(layers) && layerId) {
            _.forEach(layers, function(value) {
                if(value instanceof L.LayerGroup) {
                    _.remove(value.getLayers(), function(v) {
                        return (v._id && v._id === layerId);
                    });
                }
            });
            
            _.remove(layers, function(v) {
                return (v._id && v._id === layerId);
            });
        }
        
        return layers;
    }
    
    /**
     * Removes Leaflet group layer from given layers variable and returns it.
     * @memberof CmLayersStorage
     * @param {type} layers
     * @param {type} layerGroupId
     * @returns {unresolved}
     */
    function removeLayerGroupFromLayers(layers, layerGroupId) {
        if(_.isArray(layers) && layerGroupId) {
            _.remove(layers, function(v) {
                return (v._id && v._id === layerGroupId);
            });
        }
        
        return layers;
    }
    
    /**
     * Resets layer storage for given map id.
     * @memberof CmLayersStorage
     * @param {type} mapId
     * @returns {undefined}
     */
    function resetLayers(mapId) {
        layers[mapId] = [];
    }
    
    /**
     * Sets layers to get layers promise.
     * @memberof CmLayersStorage
     * @param {type} c
     * @param {type} id
     * @returns {undefined}
     */
    function setLayers(c, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(layers, id);
        defer.resolve(c);
        CmStorageHelpers.setResolvedDefer(layers, id);
    }

    ///////////////////////// PRIVATE ////////////////////////////

    function _getLayers(id) {
        var defer = CmStorageHelpers.getDefer(layers, id);
        return defer.promise;
    }
}
/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.storage')
    .factory('CmMapStorage', CmMapStorage);

CmMapStorage.$inject = ['CmStorageHelpers'];

function CmMapStorage(CmStorageHelpers) {

    var map = {};

    var service = {
        setMap: setMap,
        getMap: getMap,
        resetMap: resetMap
    };
    return service;

    /////////////////////////

    function setMap(m, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(map ,id);
        defer.resolve(m);
        CmStorageHelpers.setResolvedDefer(map, id);
    }

    function getMap(id) {
        var defer = CmStorageHelpers.getDefer(map, id);
        return defer.promise;
    }

    function resetMap(id){
        map[id] = undefined;
    }

}
/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.storage')
    .factory('CmOptionsStorage', CmOptionsStorage);

CmOptionsStorage.$inject = ['CmStorageHelpers'];

function CmOptionsStorage(CmStorageHelpers) {

    var opts = {};

    var service = {
        setOptions: setOptions,
        getOptions: getOptions,
        resetOptions: resetOptions
    };
    return service;

    /////////////////////////

    function setOptions(o, id) {
        var defer = CmStorageHelpers.getUnresolvedDefer(opts, id);
        defer.resolve(o);
        CmStorageHelpers.setResolvedDefer(opts, id);
    }
    
    function getOptions(mapId){
        return _getOptions(mapId).then(function (opts) {
            return _.cloneDeep(opts);
        });
    }

    function resetOptions(id){
        opts[id] = undefined;
    }
    
    ///////////////////////// PRIVATE ////////////////////////////

    function _getOptions(id) {
        var defer = CmStorageHelpers.getDefer(opts, id);
        return defer.promise;
    }

}
/**
 * Created by ondrejzvara on 10.5.16.
 */
angular
    .module('cm.common.storage')
    .factory('CmStorageHelpers', CmStorageHelpers);

CmStorageHelpers.$inject = ['$q'];

function CmStorageHelpers($q) {

    return {
        getDefer: getDefer,
        getUnresolvedDefer: getUnresolvedDefer,
        setResolvedDefer: setResolvedDefer
    };

/////////////////////////

    function getDefer(obj, id) {
        var defer;
        if (_.isEmpty(obj[id]) || obj[id].resolvedDefer === false) {
            defer = getUnresolvedDefer(obj, id);
        } else {
            defer = obj[id].defer;
        }
        return defer;
    }

    function getUnresolvedDefer(obj, id) {
        var defer;
        if (_.isEmpty(obj[id]) || obj[id].resolvedDefer === true) {
            defer = $q.defer();
            obj[id] = {
                defer: defer,
                resolvedDefer: false
            };
        } else {
            defer = obj[id].defer;
        }
        return defer;
    }

    function setResolvedDefer(obj, id) {
        obj[id].resolvedDefer = true;
    }

}


/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.common.utils')
    .factory('CmUtilsCrs', CmUtilsCrs);

CmUtilsCrs.$inject = [];

function CmUtilsCrs() {

    var crsDefs = {
        5514: "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813975277778 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel" +
        "+units=m +towgs84=570.8,85.7,462.8,4.998,1.587,5.261,3.56"
    };

    registerProjection(5514);

    return {
        getCrs: getCrs
    };

    //////////////////////////////////////////

    function registerProjection(projId){
        try{
            proj4('urn:ogc:def:crs:EPSG::' + projId);
        } catch(e){
            proj4.defs('urn:ogc:def:crs:EPSG::' + projId, crsDefs[projId]);
        }
    }
    
    function getCrs(srid){
        switch (srid) {
            case 3857:
                return L.CRS.EPSG3857;
            case 4326:
                return L.CRS.EPSG4326;
            case 3395:
                return L.CRS.EPSG3395;
            case 5514:
                // aligned to settings of CMProxy
                var epsg5514 = new L.Proj.CRS('EPSG:5514', crsDefs[5514], {
                        resolutions: [ 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25, 0.125 ],
                        origin: [-925000, -920000],
                        bounds: L.bounds(
                            L.point(-920000, -1449288),
                            L.point(-400712, -925000))
                    });
                return epsg5514;
            default:
                return null;
        }
    }


}
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular
    .module('cm.common.utils')
    .factory('CmUtilsGeoJSON', CmUtilsGeoJSON);

CmUtilsGeoJSON.$inject = [];

function CmUtilsGeoJSON() {

    return {
        toLeafletLayer: toLeafletLayer,
        getFeature: getFeature
    };

    //////////////////////////////////////////

    function toLeafletLayer(geojson){
        return L.GeoJSON.geometryToLayer(geojson);
    }

    function getFeature(layer){
        return layer._layers ? layer._layers[Object.keys(layer._layers)[0]] : layer;
    }
    

}
/**
 * Created by ondrejzvara on 12.5.16.
 */

angular
        .module('cm.draw')
        .factory('CmDraw', CmDraw);

CmDraw.$inject = ['CM_DRAW_STYLES', 'CmMapStorage'];

function CmDraw(CM_DRAW_STYLES, CmMapStorage) {

    return {
        startPolyline: startPolyline,
        savePolyline: savePolyline,
        removePolyline: removePolyline
        // startPolygonDraw: startPolygonDraw,
        // savePolygonDraw: savePolygonDraw,
        // removePolygonDraw: removePolygonDraw
    };

/////////////////////////

    function startPolyline(mapId, opts) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            opts = opts ? opts : CM_DRAW_STYLES.polyline;
            var poly = map.editTools.startPolyline();
            poly.setStyle(opts);
            return poly;
        });
    }

    function savePolyline(poly) {
        poly.editor.disable();
        return poly;
    }

    function removePolyline(mapId, poly) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            return map.removeLayer(poly);
        });
    }

   /* function startPolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }

    function savePolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }

    function removePolygonDraw(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

        });
    }*/
}

/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
        .module('cm.edit')
        .factory('CmEdit', CmEdit);

CmEdit.$inject = ['CmUtilsGeoJSON', 'CmMapStorage'];

function CmEdit(CmUtilsGeoJSON, CmMapStorage) {

    var editLayer = null;

    return {
        cancel: cancel,
        disable: disable,
        enable: enable,
        reset: reset,
        save: save
    };

    function enable(mapId, geojson) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            hasEditLayer(map);

            geojson.leafletLayer = CmUtilsGeoJSON.toLeafletLayer(geojson.features[0]);
            editLayer.addLayer(geojson.leafletLayer);
            geojson.leafletLayer.enableEdit();
            return geojson;
        });
    }

    function disable(mapId, geojson) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            return save(map, geojson);
        });
    }

    function save(map, geojson) {
        if (!geojson.leafletLayer && !geojson.leafletLayer.editEnabled()) {
            return;
        }
        geojson.leafletLayer.disableEdit();
        return geojson;
    }

    function cancel(mapId, geojson) {
        editLayer.removeLayer(geojson.leafletLayer);
    }

    function reset(mapId, geojson) {
        editLayer.removeLayer(geojson.leafletLayer);
        enable(mapId, geojson);
    }

    function hasEditLayer(map) {
        if (!editLayer) {
            editLayer = new L.geoJson();
            editLayer.addTo(map);
        }
    }
}
/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
    .module('cm.edit')
    .factory('CmEditUtils', CmEditUtils);

CmEditUtils.$inject = [];

function CmEditUtils() {

    return {
        getFeature: getFeature
    };

    function getFeature(obj){
        return obj._layers ? obj._layers[Object.keys(obj._layers)[0]] : obj;
    }
}
/**
 * Created on 12.11.15.
 */

angular
    .module('cm.event')
    .factory('CmEvent', CmEvent);

CmEvent.$inject = [ 'CM_MAP_LAYER_TYPE', 'CmLayersStorage', 'CmMapStorage' ];

function CmEvent( CM_MAP_LAYER_TYPE, CmLayersStorage, CmMapStorage ) {

    return {
        queryLayer: queryLayer,
        removeClickEvent: removeClickEvent
    };

    /////////////////////////


    function queryLayer(mapId, layerId, callback) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            CmLayersStorage.getLayers(mapId).then(function(conf) {
                
                var layer = CmLayersStorage.getLayerById(conf, layerId);

                if(!layer){
                    throw new Error('Layre with ID: ' + layerId + ' not found.');
                }
/*

                if( layer.type !== CM_MAP_LAYER_TYPE.wms && layer.type !== CM_MAP_LAYER_TYPE.wfs ){
                    throw new Error('Layer is not of WMS or WFS type.');
                }

                if(!layer.queryable){
                    throw new Error('Layer is not queryable.');
                }
*/

                if(typeof callback !== 'function') {
                    throw new Error('No callback function provided.');
                }

                map.off('click');
                map.on('click', function(evt) {
                    layer.loadFeatureData(evt.latlng).then(function(data){
                        callback.call(this, data, evt);
                    });
                });
            });
        });
    }

    function removeClickEvent(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.off('click');
        });
    }

}

/************************************************* common *******************************/
/*


function registerMapevt() {
    Map.getMap(config.id).then(function(map) {
        map.on('click', function(e) {
            var options = MapOptions.getMapOptions(config.id);
            onMapClick(e, options, config, map);
        });
    });
}

function onMapClick(evt, options, config, map) {
    var scope = $rootScope.$new();

    // TODO remove if it's not necessary
    _.assignIn(scope, {
        options: options,
        config: config
    });

    if (options['popup'] === CM_MAP_POPUP_TYPE.static) {

        // display static popup
        LeafletPopup.get({scope: scope}, this)
            .setLatLng(evt.latlng)
            .setContent(options['popupTemplate'])
            .openOn(map);
    }

    if (options['popup'] === CM_MAP_POPUP_TYPE.dynamic) {
        // display dynamic popup
        // prepare template
        var tplComponents = [];
        var index = 0;

        scope.featureDataLoaded = [];
        scope.featureData = [];

        var someDataLoaded = false;

        var popup = LeafletPopup.get({scope: scope}, this);
        popup.setLatLng(evt.latlng);

        var highestprimaryId = MapprimaryVisibility.getHighestPriorityprimaryId(config);

        // iterate through primarys
        MapConfig.forEachprimary(config, function (primary) {
            if ((primary.type === CM_MAP_primary_TYPE.wms) || (primary.type === CM_MAP_primary_TYPE.wfs)) {

                if (primary.onprimaryClick && primary.visible) {
                    if(options['loadingSpin']) {
                        MapSpinner.initSpinner(config.id, map.getContainer());
                    }

                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            primary.onprimaryClick(evt, data.data.features, primary);
                        }
                        if(options['loadingSpin']) {
                            MapSpinner.stop(config.id);
                        }
                    });
                }

                if ((primary.featureSummaryTemplate) && primary.visible) {
                    var newTplComponent = '<div ng-show="featureDataLoaded[' + index + ']" ng-repeat="feature in featureData[' + index + ']">'
                        + primary.featureSummaryTemplate.replace(new RegExp('feature', 'g'), "feature.properties")
                        + '</div>';
                    tplComponents.push(newTplComponent);

                    var primaryIndex = index;
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        if (data.data.features && (data.data.features.length > 0)) {
                            scope.featureDataLoaded[primaryIndex] = true;
                            scope.featureData[primaryIndex] = data.data.features;

                            if (!someDataLoaded) {
                                popup.openOn(map);
                                someDataLoaded = true;
                            }
                        }
                    });

                    index++;
                }

                if (primary.infoboxTemplate && primary.visible &&
                    ((highestprimaryId > -1 && primary.infoboxTemplatePriority === highestprimaryId) ||
                    highestprimaryId === -1)
                ) {
                    primary.primaryObj.loadFeatureData(evt.latlng).then(function (data) {
                        scope.$root.$broadcast(CM_MAP_evt.openInfobox, {
                            type: primary.infoboxTemplate,
                            features: data.data.features
                        });
                    });
                }
            }
        });

        tplComponents.reverse();
        var template = options['popupTemplate'] + tplComponents.join("") + "{{featureData[" + index + "].parcis}}";

        popup.setContent(template);
    }
}

*/

/*
    //TODO: logic below belongs to different services

    // temporary disable popup
    var disablePopup = function () {
        options['popup-origvalue'] = options['popup'];
        options['popup'] = CM_MAP_POPUP_TYPE.none;
    };

    // reenable popup
    var enablePopup = function () {
        options['popup'] = options['popup-origvalue'];
    };




    map.on('draw:drawstart', disablePopup);
    map.on('edit:editstart', disablePopup);
    map.on('delete:deletestart', disablePopup);

    map.on('draw:drawstop', enablePopup);
    map.on('edit:editstop', enablePopup);
    map.on('delete:deletestop', enablePopup);
*/

angular
    .module('cm.map')
    .factory('CmLayer', CmLayer);

CmLayer.$inject = ['CmLayersStorage', 'CmLayerFactory', 'CmMapStorage'];

function CmLayer(CmLayersStorage, CmLayerFactory, CmMapStorage) {

    /**
     * Service CmLayer
     * @namespace CmLayer
     */
    return {
        addLayerToMap: addLayerToMap,
        addLayerGroupToMap : addLayerGroupToMap,
        forEachLayer: forEachLayer,
        initLayersForMap: initLayersForMap,
        removeLayerFromMap: removeLayerFromMap,
        removeLayerGroupFromMap: removeLayerGroupFromMap
    };

    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * Creates Leaflet layer from given configuration, adds it to map and
     * saves it to layers storage. Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerConf
     * @param {type} layerId
     * @returns {unresolved}
     */
    function addLayerToMap(mapId, layerConf, layerId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayerFactory.createLayer(layerConf, layerId);

            CmMapStorage.getMap(mapId).then(function(map) {
                layerLeaflet.addTo(map);
                
                // adds layer to immutable layer storage
                layers = CmLayersStorage.addLayerToLayers(layers, layerLeaflet);
                CmLayersStorage.setLayers(layers, mapId);
            });
        });
    }
    
    /**
     * Creates Leaflet layers group from given configuration, adds it to map and
     * saves all layers from group to layers storage. Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerGroupConfArray
     * @param {type} groupId
     * @returns {unresolved}
     */
    function addLayerGroupToMap(mapId, layerGroupConfArray, groupId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            if(_.isArray(layerGroupConfArray)) {
                var layerLeafletArray = [];
                
                _.forEach(layerGroupConfArray, function(layer) {
                    if(layer.type && layer.type === 'layer' && layer.id && layer.layer) {
                        var layerLeaflet = CmLayerFactory.createLayer(layer.layer, layer.id);
                        layerLeafletArray.push(layerLeaflet);
                    }
                });
                
                var layerLeafletGroup = CmLayerFactory.createLayerGroup(groupId, layerLeafletArray);
                
                CmMapStorage.getMap(mapId).then(function(map) {
                    layerLeafletGroup.addTo(map);
                    
                    // adds layer group to immutable layer storage
                    CmLayersStorage.addLayerGroupToLayers(layers, layerLeafletGroup);
                    CmLayersStorage.setLayers(layers, mapId);
                });
            }
        });
    }
    
    /**
     * Applies func for every layer.
     * @memberof CmLayer
     * @param layersStorage
     * @param func
     */
    function forEachLayer(layersStorage, func) {
        if(_.isArray(layersStorage)) {
            _.forEach(layersStorage, function(value) {
                if(value instanceof L.LayerGroup) {
                    value.eachLayer(function(layer) {
                        func(layer);
                    });
                } else {
                    func(value);
                }
            });
        }
    }
    
    /**
     * Recursively adds new layers or layers groups to map from given 
     * configuration. Recursion keeps the promise chain.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerConfiguration
     * @returns {undefined}
     */
    function initLayersForMap(mapId, layerConfiguration) {
        if(_.isArray(layerConfiguration) && layerConfiguration.length > 0) {
            var layerConf = _.head(layerConfiguration);
            layerConfiguration = _.tail(layerConfiguration);

            if(layerConf.type && layerConf.type === 'layer' && layerConf.id && layerConf.layer) {
                addLayerToMap(mapId, layerConf.layer, layerConf.id).then(function() {
                    initLayersForMap(mapId, layerConfiguration);
                });
            } else if (layerConf.type && layerConf.type === 'layerGroup' && layerConf.id && layerConf.layers) {
                addLayerGroupToMap(mapId, layerConf.layers, layerConf.id).then(function() {
                    initLayersForMap(mapId, layerConfiguration);
                });
            }
        }
    }
    
    /**
     * Removes layer from map and layer storage by given layer id. 
     * Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerId
     * @returns {unresolved}
     */
    function removeLayerFromMap(mapId, layerId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayersStorage.getLayerById(layers, layerId);
            
            CmMapStorage.getMap(mapId).then(function(map) {
                if(map.hasLayer(layerLeaflet)) {
                    map.removeLayer(layerLeaflet);
                    
                    layers = CmLayersStorage.removeLayerFromLayers(layers, layerId);
                    CmLayersStorage.setLayers(layers, mapId);
                }
            });
        });
    }
    
    /**
     * Removes layer group from map and layer storage by given group id.
     * Returns promise.
     * @memberof CmLayer
     * @param {type} mapId
     * @param {type} layerGroupId
     * @returns {unresolved}
     */
    function removeLayerGroupFromMap(mapId, layerGroupId) {
        return CmLayersStorage.getLayers(mapId).then(function(layers) {
            var layerLeaflet = CmLayersStorage.getLayerById(layers, layerGroupId);
            
            CmMapStorage.getMap(mapId).then(function(map) {
                if(map.hasLayer(layerLeaflet)) {
                    map.removeLayer(layerLeaflet);
                    
                    layers = CmLayersStorage.removeLayerGroupFromLayers(layers, layerGroupId);
                    CmLayersStorage.setLayers(layers, mapId);
                }
            });
        });
    }
}
/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('cm.map')
    .factory('CmLeafletMap', CmLeafletMap);

CmLeafletMap.$inject = ['CmUtilsCrs', 'CmMapStorage', 'CmOptionsStorage', 'CmLayersStorage', 'CmLayer', 'CmFullscreen', 'CmMap'];

function CmLeafletMap(CmUtilsCrs, CmMapStorage, CmOptionsStorage, CmLayersStorage, CmLayer, CmFullscreen, CmMap) {

    return {
        setOptionsCrs: setOptionsCrs,
        setFullscreen: setFullscreen,
        setBounds: setBounds,
        setInitMapPos: setInitMapPos,
        storeMap: storeMap,
        resetMap: resetMap,
        storeOptions: storeOptions,
        resetOptions: resetOptions,
        resetLayers: resetLayers,
        initLayers: initLayers,
        enableLoadingWatch: enableLoadingWatch
    };

    //////////////////////////////////////////

    function setOptionsCrs(options){
      if (options['srid']) {
          var crs = CmUtilsCrs.getCrs(options['srid']);
          if (crs === null) {
              throw new Error('CRS with srid ' + options['srid'] + ' not defined.');
          }
          options['crs'] = crs;
      }
    }

    function setFullscreen(map, opts) {
        if (opts.fullscreen) {
            map.fullscreenControl = CmFullscreen.getFullscreenControl({});
        }
    }

    function setBounds(mapId, opts) {
        if (opts.bounds && (opts.bounds instanceof L.LatLngBounds)) {
            CmMap.fitBounds(mapId, opts.bounds);
        }
    }

    function setInitMapPos(map) {
        map.options.initCenter = _.cloneDeep(map.getCenter());
        map.options.initZoom = _.cloneDeep(map.getZoom());
    }

    function storeMap(map, mapId) {
        CmMapStorage.setMap(map, mapId);
    }

    function resetMap(mapId) {
        CmMapStorage.resetMap(mapId);
    }

    function storeOptions(opts, mapId) {
        CmOptionsStorage.setOptions(opts, mapId);
    }

    function resetOptions(mapId) {
        CmOptionsStorage.resetOptions(mapId);
    }

    function resetLayers(mapId) {
        CmLayersStorage.resetLayers(mapId);
    }

    function initLayers(mapId, config) {
        CmLayersStorage.setLayers({}, mapId);
        CmLayer.initLayersForMap(mapId, config);
    }
    
    function enableLoadingWatch(mapId){
        CmMap.enableLoadingWatch(mapId);
    }
    

}

/**
 * Created by ondrejzvara on 11.5.16.
 */

angular
    .module('cm.map')
    .factory('CmMap', CmMap);

CmMap.$inject = ['CmMapStorage', 'CmLayersStorage', 'CmLayer', 'CM_MAP_LAYER_TYPE'];

function CmMap(CmMapStorage, CmLayersStorage, CmLayer, CM_MAP_LAYER_TYPE) {

    /**
     * Service CmMap
     * @namespace CmMap
     */
    return {
        addPositionListener: addPositionListener,
        getMap: getMap,
        toggleFullscreen: toggleFullscreen,
        fitBounds: fitBounds,
        setPositionListener: setPositionListener,
        enableLoadingWatch: enableLoadingWatch
    };

    //////////////////////////////////////////
    
    /**
     * Registers listener called with center positions and zoom every move end.
     * @memberof CmMap
     * @param {String} mapId
     * @param {function} callback
     */
    function addPositionListener(mapId, callback) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            map.on('moveend', function (event) {
                if (event && event.target) {
                    var mapTarget = event.target,
                            center = mapTarget.getCenter(),
                            zoom = mapTarget.getZoom();

                    callback.call(this, center.lat, center.lng, zoom);
                }
            });
        });
    }
    
    function getMap(mapId) {
        return CmMapStorage.getMap(mapId);
    }
    
    function fitBounds(mapId, bounds){
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.fitBounds(bounds);
        });
    }

    /**
     * If positionListener option is setted, registers position listener.
     * @memberof CmMap
     * @param {String} mapId
     * @param {object} opts
     */
    function setPositionListener(mapId, opts) {
        if (opts.positionListener) {
            addPositionListener(mapId, opts.positionListener);
        }
    }

    function toggleFullscreen(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.fullscreenControl.toggleFullScreen(map);
        });
    }

    function enableLoadingWatch(mapId){
       /* return CmMapStorage.getMap(mapId).then(function(map) {

            map.on('layeradd', function (e) {

                if (e.layer.loading) console.log('start');

                if (typeof e.layer.on != 'function') return;

                e.layer.on('data:loading', function () { console.log('start'); });
                e.layer.on('data:loaded',  function () { console.log('stop'); });

            });


        });*/

        return CmLayersStorage.getLayers(mapId).then(function(config){

            var fnc = function(conf){

                if (conf.type === CM_MAP_LAYER_TYPE.wms) {


                    //var layer = config.layerObj;
                    // console.log(layer)
                    // console.log(layer)


                }


            };

            CmLayer.forEachLayer(config, fnc);

        });

       

        // this.on('layerremove', function (e) {
            // if (e.layer.loading) this.spin(false);
            // if (typeof e.layer.on != 'function') return;
            // e.layer.off('data:loaded');
            // e.layer.off('data:loading');
        // }, this);

    }

   
}

/**
 * Created by ondrejzvara on 16.5.16.
 */

angular
        .module('cm.measure')
        .factory('CmMeasure', CmMeasure);

CmMeasure.$inject = ['CmDraw', 'CM_MAP_UNIT', 'CmMapStorage'];

function CmMeasure(CmDraw, CM_MAP_UNIT, CmMapStorage) {

    var poly;

    return {
        startLineMeasure: startLineMeasure,
        stopLineMeasure: stopLineMeasure
    };

    function startLineMeasure(mapId, callback, unitCode) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            var unit = unitCode ? CM_MAP_UNIT[unitCode] : CM_MAP_UNIT.m;

            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            CmDraw.startPolyline(mapId).then(function(polyline){
                poly = polyline;
            });

            map.on('editable:drawing:click', onMeasuremenetEnabled);
            map.on('editable:vertex:drag', onMeasuremenetEnabled);
            map.on('editable:vertex:deleted', onMeasuremenetEnabled);


            function onMeasuremenetEnabled(evt) {
                var polyline = evt.layer.toGeoJSON();
                var measurement = turf.lineDistance(polyline, CM_MAP_UNIT.km.name);

                if (unit.code === 'm') {
                    measurement = measurement * 1000;
                }

                callback.call(this, measurement, unit, evt);
            }
        });
    }

    function stopLineMeasure(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {

            map.off('editable:drawing:click');
            map.off('editable:vertex:drag');
            map.off('editable:vertex:deleted');
            CmDraw.removePolyline(mapId, poly);

        });
    }
}
/**
 * Created by ondrejzvara on 11.5.16.
 */


angular
        .module('cm.navigate')
        .factory('CmNavigate', CmNavigate);

CmNavigate.$inject = ['CmMapStorage', 'CmUtilsCrs'];

function CmNavigate(CmMapStorage, CmUtilsCrs) {

    return {
        move: move,
        home: home,
        zoomIn: zoomIn,
        zoomOut: zoomOut,
        zoomToRectangleStart: zoomToRectangleStart,
        zoomToRectangleStop: zoomToRectangleStop,
        enableMouseCoords: enableMouseCoords,
        disableMouseCoords: disableMouseCoords
    };

    //////////////////////////////////////////

    function move(mapId, x, y) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.panBy([x, y]);
        });
    }

    function home(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.setView(map.options.initCenter, map.options.initZoom);
        });
    }

    function zoomIn(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.zoomIn();
        });
    }

    function zoomOut(mapId) {
        return CmMapStorage.getMap(mapId).then(function (map) {
            map.zoomOut();
        });
    }

    function zoomToRectangleStart(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.dragging.disable();
            map.boxZoom.addHooks();
            map.on('mousedown', _zoomRectMouseDown);
            // map.boxZoom._container.style.cursor = 'zoom-in';
        });
    }

    function zoomToRectangleStop(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.boxZoom._container.style.cursor = '';
            map.off('mousedown', _zoomRectMouseDown);
            map.dragging.enable();
            map.boxZoom.removeHooks();
        });
    }

    function enableMouseCoords(mapId, callback, crsId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            if (typeof callback !== 'function') {
                throw new Error('No callback function provided.');
            }

            if (crsId && typeof crsId !== 'number') {
                throw new Error('Invalid crsId provided.');
            }

            var mapCrsId = 3857;
            crsId = crsId ? crsId : mapCrsId;
            var crsTo = CmUtilsCrs.getCrs(crsId);

            var result = {};

            map.on('mousemove', function (evt) {
                var coords = [evt.latlng.lat, evt.latlng.lng];

                if (mapCrsId !== crsId) {
                    coords = crsTo.project(evt.latlng);
                    result.lat = coords.y;
                    result.lng = coords.x;
                } else {
                    result.lat = evt.latlng.lat;
                    result.lng = evt.latlng.lng;
                }

                // 5514 example: X: -902225, y: -992104
                result.crsId = crsId;
                callback.call(this, result, evt);
            });
        });
    }

    function disableMouseCoords(mapId) {
        return CmMapStorage.getMap(mapId).then(function(map) {
            map.off('mousemove');
        });
    }
    
    ///////////////////////////////////////////////////////////////////////////
    
    function _zoomRectMouseDown(event) {
        if (event.target instanceof L.Map) {
            var map = event.target;
            map.boxZoom._onMouseDown.call(map.boxZoom, {clientX: event.originalEvent.clientX, clientY: event.originalEvent.clientY, which: 1, shiftKey: true});
            // ?? map.boxZoom._container.style.cursor = 'zoom-in';
        }
    }
}

/**
 * Created by ondrejzvara on 26.5.16.
 */

angular
    .module('cm.storage')
    .factory('CmStorage', CmStorage);

CmStorage.$inject = ['CmMapStorage', 'CmLayersStorage', 'CmOptionsStorage'];

function CmStorage(CmMapStorage, CmLayersStorage, CmOptionsStorage) {

    return {
        getMap: getMap,
        getLayers: getLayers,
        getOptions: getOptions
    };

    //////////////////////////////////////////

    function getMap(mapId) {
        return CmMapStorage.getMap(mapId);
    }

    function getLayers(mapId) {
        return CmLayersStorage.getLayers(mapId);
    }

    function getOptions(mapId) {
        return CmOptionsStorage.getOptions(mapId);
    }
}
/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmFullscreen', cmFullscreen);

cmFullscreen.$inject = ['CmMap'];

function cmFullscreen(CmMap) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-fullscreen.html',
        link: function link(scope) {

            scope.isFullscreen = false;
            scope.tooltipText = "Start fullscreen";
            scope.toggleFullScreen = toggleFullScreen;

            function toggleFullScreen(){
                if(!scope.isFullscreen){
                    scope.tooltipText = "Stop fullscreen";
                } else {
                    scope.tooltipText = "Start fullscreen";
                }

                CmMap.toggleFullscreen(scope.mapId);
                scope.isFullscreen = !scope.isFullscreen;
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmHome', cmHome);

cmHome.$inject = ['CmNavigate'];

function cmHome(CmNavigate) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-home.html',
        link: function link(scope) {

            scope.goHome = goHome;

            function goHome(){
                CmNavigate.home(scope.mapId);
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMeasureLine', cmMeasureLine);

cmMeasureLine.$inject = ['CmMeasure', '$filter'];

function cmMeasureLine(CmMeasure, $filter) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-measure-line.html',
        link: function link(scope, element) {

            var mapElement = element.parent().parent().parent();
            var holder = angular.element('<div id="coords-holder"></div>');
            mapElement.append(holder);
            holder.hide();

            scope.lineLength = 0;

            scope.isMeasuring = false;
            scope.tooltipText = "Start measure";

            scope.toggleMeasure = toggleMeasure;

            function toggleMeasure(){
                if(!scope.isMeasuring){

                    holder.show();
                    CmMeasure.startLineMeasure(scope.mapId, lineMeasureCallback);
                    mapElement.on('mousemove', onCursorMove);
                    scope.tooltipText = "Stop measure coords";

                } else {

                    holder.hide();
                    CmMeasure.stopLineMeasure(scope.mapId);
                    scope.tooltipText = "Start measure";
                    mapElement.off('mousemove', onCursorMove);
                    scope.lineLength = 0;
                }
                scope.isMeasuring = !scope.isMeasuring;
            }


            function lineMeasureCallback(distance, unit){
                console.log(distance);
                scope.lineLength = $filter('number')(distance, 3) + " " + unit.code;
            }

            function onCursorMove(e){
                scope.$apply(function() {
                    holder.css({
                        left: ( e.pageX + 35 + "px" ),
                        top: ( e.pageY - 15 + "px" )
                    });
                    var text = scope.lineLength;
                    holder.text(text);
                });
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMouseCoords', cmMouseCoords);

cmMouseCoords.$inject = ['CmNavigate', '$filter'];

function cmMouseCoords(CmNavigate, $filter) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@',
            crsId: '=?'
        },
        templateUrl: '../src/js/cm.directives/cm-mouse-coords.html',
        link: function link(scope, element) {

            var mapElement = element.parent().parent().parent();
            var holder = angular.element('<div id="coords-holder"></div>');
            mapElement.append(holder);
            holder.hide();

            scope.crsId = scope.crsId || 5514;
            scope.mouseCoords = {};

            scope.isTrackingCoords = false;
            scope.tooltipText = "Show coords";
            scope.toggleMouseCoords = toggleMouseCoords;

            function toggleMouseCoords(){
                if(!scope.isTrackingCoords){

                    holder.show();
                    CmNavigate.enableMouseCoords(scope.mapId, mouseCoordsCallback, scope.crsId);
                    mapElement.on('mousemove', onCursorMove);
                    scope.tooltipText = "Hide coords";

                } else {

                    holder.hide();
                    CmNavigate.disableMouseCoords(scope.mapId);
                    scope.tooltipText = "Show coords";
                    mapElement.off('mousemove', onCursorMove);
                }
                scope.isTrackingCoords = !scope.isTrackingCoords;
            }


            function mouseCoordsCallback(result){
                scope.mouseCoords = result;
            }

            function onCursorMove(e){
                scope.$apply(function() {
                    holder.css({
                        left: ( e.pageX + 35 + "px" ),
                        top: ( e.pageY - 15 + "px" )
                    });
                    var text = 'EPSG:' + scope.mouseCoords.crsId + ": " + $filter('number')(scope.mouseCoords.lat, 8) + " lat; " + $filter('number')(scope.mouseCoords.lng, 8)+ " lon;" ;
                    holder.text(text);
                });
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMoveBottom', cmMoveBottom);

cmMoveBottom.$inject = ['CmNavigate'];

function cmMoveBottom(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-bottom.html',
        link: function link(scope) {

            scope.moveBottom = moveBottom;

            function moveBottom() {
                CmNavigate.move(scope.mapId, 0, 500);
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMoveLeft', cmMoveLeft);

cmMoveLeft.$inject = ['CmNavigate'];

function cmMoveLeft(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-left.html',
        link: function link(scope) {

            scope.moveLeft = moveLeft;

            function moveLeft(){
                CmNavigate.move(scope.mapId, -500, 0);
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmMoveRight', cmMoveRight);

cmMoveRight.$inject = ['CmNavigate'];

function cmMoveRight(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-right.html',
        link: function link(scope) {

            scope.moveRight = moveRight;

            function moveRight(){
                CmNavigate.move(scope.mapId, 500, 0);
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 27.5.16.
 */


angular
    .module('cm.directives')
    .directive('cmMoveTop', cmMoveTop);

cmMoveTop.$inject = ['CmNavigate'];

function cmMoveTop(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-move-top.html',
        link: function link(scope) {

            scope.moveTop = moveTop;

            function moveTop() {
                CmNavigate.move(scope.mapId, 0, -500);
            }

        }
    };
    return directive;

}

/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmToolbarHolder', cmToolbarHolder);

cmToolbarHolder.$inject = ['CmMapStorage'];

function cmToolbarHolder(CmMapStorage) {
    
    var directive = {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            mapId: '@'
        },
        template: '<div class="btn-toolbar toolbar" role="toolbar" ng-transclude></div>',
        link: function link(scope, element) {

            CmMapStorage.getMap(scope.mapId).then(function (map) {

                map.on('enterFullscreen', function(){
                    element.css('z-index', 99999999999999);
                });

                map.on('exitFullscreen', function(){
                    element.css('z-index', 0);
                });

            });

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomIn', cmZoomIn);

cmZoomIn.$inject = ['CmNavigate'];

function cmZoomIn(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-in.html',
        link: function link(scope) {

            scope.zoomIn = zoomIn;
            
            function zoomIn(){
                CmNavigate.zoomIn(scope.mapId);
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 27.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomOut', cmZoomOut);

cmZoomOut.$inject = ['CmNavigate'];

function cmZoomOut(CmNavigate) {

    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-out.html',
        link: function link(scope) {

            scope.zoomOut = zoomOut;

            function zoomOut(){
                CmNavigate.zoomOut(scope.mapId);
            }

        }
    };
    return directive;

}
/**
 * Created by ondrejzvara on 13.5.16.
 */

angular
    .module('cm.directives')
    .directive('cmZoomRectangle', cmZoomRectangle);

cmZoomRectangle.$inject = ['CmNavigate'];

function cmZoomRectangle(CmNavigate) {
    
    var directive = {
        restrict: 'E',
        replace: true,
        scope: {
            mapId: '@'
        },
        templateUrl: '../src/js/cm.directives/cm-zoom-rectangle.html',
        link: function link(scope) {

            scope.isRectangularlyZooming = false;
            scope.tooltipText = "Rectangular zoom - start";
            scope.toggleRectangularZoom = toggleRectangularZoom;

            function toggleRectangularZoom(){
                if(!scope.isRectangularlyZooming){
                    CmNavigate.zoomToRectangleStart(scope.mapId);
                    scope.tooltipText = "Rectangular zoom - stop";
                } else {
                    CmNavigate.zoomToRectangleStop(scope.mapId);
                    scope.tooltipText = "Rectangular zoom - start";
                }
                scope.isRectangularlyZooming = !scope.isRectangularlyZooming;
            }

        }
    };
    return directive;

}
angular
        .module('cm.map')
        .directive('cmLeafletMap', cmLeafletMap);

cmLeafletMap.$inject = ['CmLeafletMap', '$log', 'CmMap'];

function cmLeafletMap(CmLeafletMap, $log, CmMap) {
    var directive = {
        restrict: 'E',
        scope: {
            options: '=',
            config: '='
        },
        link: function (scope, element) {
            
            try {
                var options = scope.options,
                    config = scope.config,
                    mapElement = element[0],
                    mapId = options.id;

                mapElement.setAttribute('id', mapId);
                CmLeafletMap.setOptionsCrs(options);

                var map = new L.Map(mapElement, options);
                map.invalidateSize();
                map.whenReady(function () {
                    CmLeafletMap.enableLoadingWatch(mapId);
                    CmLeafletMap.initLayers(mapId, config);
                    CmLeafletMap.setBounds(mapId, options);
                    CmLeafletMap.setInitMapPos(map);
                    CmLeafletMap.setFullscreen(map, options);
                    CmLeafletMap.storeMap(map, mapId);
                    CmLeafletMap.storeOptions(options, mapId);
                    CmMap.setPositionListener(mapId, options);
                });


                // map.on('zoomend', function() {
                //     MapLayerVisibility.checkLayersVisibility(scope.config, map);
                // });

            } catch(e){
                $log.error(e);
            }

            scope.$on('$destroy', function() {
                map.remove();
                element.remove();
                CmLeafletMap.resetMap(mapId);
                CmLeafletMap.resetOptions(mapId);
                CmLeafletMap.resetLayers(mapId);
            });
        }
    };
    
    return directive;
}

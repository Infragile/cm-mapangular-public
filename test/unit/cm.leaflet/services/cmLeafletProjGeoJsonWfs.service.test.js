
describe("Testing LeafletProjGeoJsonWfs", function() {

    var cmLeafletProjGeoJsonWfs;
    
    beforeEach(module('cm.leaflet'));
    
    beforeEach(inject(function (LeafletProjGeoJsonWfs) {
        jasmine.getJSONFixtures().fixturesPath = 'base/test/data';
        var layerConfig = window.getJSONFixture('layerWmsFilterConfig.json');
            
        cmLeafletProjGeoJsonWfs = new LeafletProjGeoJsonWfs('afdsdafd', '', layerConfig, layerConfig['filter'], {}, null, null);
    }));
    
    
    it('checks class of injected instance', function() {
        expect(cmLeafletProjGeoJsonWfs instanceof L.Proj.GeoJSON).toBeTruthy();
        expect(cmLeafletProjGeoJsonWfs instanceof LeafletProjGeoJsonWfs).toBe(false);
    });

});
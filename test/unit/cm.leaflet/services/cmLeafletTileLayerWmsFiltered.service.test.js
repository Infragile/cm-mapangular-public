
describe("Testing LeafletTileLayerWmsFiltered", function () {

    var cmLeafletTileLayerWmsFiltered;

    beforeEach(module('cm.leaflet'));

    beforeEach(inject(function (LeafletTileLayerWmsFiltered) {
        jasmine.getJSONFixtures().fixturesPath = 'base/test/data';
        var layerConfig = window.getJSONFixture('layerWmsFilterConfig.json');

        cmLeafletTileLayerWmsFiltered = new LeafletTileLayerWmsFiltered('fadssdffasdasdf', layerConfig, layerConfig['filter']);
    }));


    it('checks class of injected instance', function () {
        expect(cmLeafletTileLayerWmsFiltered instanceof L.TileLayer.WMS).toBeTruthy();
        expect(cmLeafletTileLayerWmsFiltered instanceof LeafletTileLayerWmsFiltered).toBe(false);
    });

});
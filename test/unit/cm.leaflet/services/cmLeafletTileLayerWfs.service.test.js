
describe("Testing LeafletTileLayerWfs", function() {

    var cmLeafletTileLayerWfs;
    
    beforeEach(module('cm.leaflet'));
    
    beforeEach(inject(function (LeafletTileLayerWfs) {
        jasmine.getJSONFixtures().fixturesPath = 'base/test/data';
        var layerConfig = window.getJSONFixture('layerConfig.json');
            
        cmLeafletTileLayerWfs = new LeafletTileLayerWfs('fadssdffasdasdf', layerConfig);
    }));
    
    
    it('checks class of injected instance', function() {
        expect(cmLeafletTileLayerWfs instanceof L.TileLayer).toBeTruthy();
        expect(cmLeafletTileLayerWfs instanceof LeafletTileLayerWfs).toBe(false);
    });

});

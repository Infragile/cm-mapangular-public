
describe("Testing LeafletPopup", function() {

    var cmLeafletPopup;
    
    beforeEach(module('cm.leaflet'));
    
    beforeEach(inject(function (LeafletPopup, $rootScope) {    
        cmLeafletPopup = new LeafletPopup({scope: $rootScope.$new()}, this);
    }));
    
    
    it('checks class of injected instance', function() {
        expect(cmLeafletPopup instanceof L.Popup).toBeTruthy();
        expect(cmLeafletPopup instanceof LeafletPopup).toBe(false);
    });

});

describe("Testing LayerSelectorStorage", function () {

    var layerSelectorStorage;
    var layerTree;

    beforeEach(module('cm.layerSelector'));

    beforeEach(inject(function (LayerSelectorStorage) {
        layerSelectorStorage = LayerSelectorStorage;

        jasmine.getJSONFixtures().fixturesPath = 'base/test/data';
        layerTree = window.getJSONFixture('layerTree.json');
    }));



});

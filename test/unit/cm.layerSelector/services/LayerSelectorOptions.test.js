
describe("Testing LayerSelectorOptions", function () {

    var layerSelectorOptions;
    var layerTree;

    beforeEach(module('cm.layerSelector'));

    beforeEach(inject(function (LayerSelectorOptions) {
        layerSelectorOptions = LayerSelectorOptions;

        jasmine.getJSONFixtures().fixturesPath = 'base/test/data';
        layerTree = window.getJSONFixture('layerTree.json');
    }));



});

Configurable Leaflet-based map view component with layer selector.

**Warning**: The following documentation has not yet been updated. To learn how to use the current version
check sampleProject.

# Development instructions #

* clone the repo
* npm install
* grunt deps
* grunt graph - creates graph of the dependencies between angular components of the app


# Dependencies


## Leaflet

 * [Leaflet](http://leafletjs.com/)
 * [Proj4leaflet](https://github.com/kartena/Proj4Leaflet)

 
## angular-local-storage
 
 * [GitHub](https://github.com/grevory/angular-local-storage)
 

# cm-leaflet-map directive

Directive displays a Leaflet based map.

Parameter       | Explanation
--------------- | -------------------
options         | Map options
config          | Map configuration


## Map options

Array with the same content as described in [Leaflet documenation](http://leafletjs.com/reference.html#map-options) with the following extensions:

Option                      | Explanation
--------------------------- | -------------------
srid                        | SRID number, i.e. 3857 or 5514, if set, overwrites CRS any CRS setting
popup                       | Popup type (none, static or dynamic)
popupTemplate               | Template for popup (is displayed in static or at the beginning of dynamic popup window), is in Angular template language
controls                    | Options for map controls
fullscreenControl           | Boolean allowing fullscreen mode ui control
fullscreenControlOptions    | Options for fullscreen (https://github.com/brunob/leaflet.fullscreen)
loadingSpin                 | Boolean allowing loading spin

If dynamic popup is set, the content of popup window is composed of *popupTemplate* and concatenation of *featureSummaryTemplate* of all displayed layers.

### Map controls options

Array with settings for supported map controls

Option         | Explanation
-------------- | -------------------
drawControl    | Options for Leaflet.draw. The value is passed directly to L.Control.Draw constructor



# cm-layer-selector

Directive displays a tree of layers with filtering.

Parameter       | Explanation
--------------- | -------------------
options         | Layer selector options
config          | Map configuration


## Layer selector options

Object with following attributes:

Option                          | Explanation
------------------------------- | --------------------
saveLayersVisibility            | Boolean, which indicates ability to save actual layers visibility to local storage and load them with directive initialization
saveGroupCollapseStatus         | Boolean which enables to remember whether a layer group is collapsed or not  
showCollapsedVisibleLayersCount | Boolean which activates number of visible layers in collapsed layer group  


# Map configuration

Configuration of layers is stored in a JavaScript data structure. The structure 
can be stored in an Angular service and loaded when needed. Map configuration
is referenced from directives. It serves as a shared memory to exchange state information between directives.

Map configuration is a tree with nodes and layers. Nodes can have other nodes and layers as children. Layers can only have layers as children. The top level of the configuration can be node or layer. Id of the top level is used as id of the map.


## Node configuration

Node configuration is an associative array with the following fields:

Field         | Explanation
--------------|-----------------
id            | Node id
type          | Must be **node**
selector      | Type of selector to display in cm-layer-selector
layers        | Array of child nodes or layers
comparator    | [Comparator function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) for selector: 'hidden' group layers ordering. 


## Layer configuration

Layer description is an associative array with attribute **type** and further attributes depending on the type of layer:

Attribute              | Explanation
---------------------- | ---------------------
id                     | Internal layer id
type                   | Type of layer (wms, wfs)
title                  | Layer title
titleTooltip           | Layer title tooltip, created as title attribute in DOM
visible                | Layer state
selector               | Type of selector to display in cm-layer-selector (none, switch, hidden, group)
path                   | Path to the layer in the layer tree, composed of ids of higher level groups
infoboxTemplate        | Flag for broadcasting CM_MAP_EVENT.openInfobox event with layer type and features data
infoboxTemplatePriority| Positive number setting priority for infobox - infobox of the layer with the highest priority will be showed
minZoomVisibility      | Smallest number of zoom, from which is layer visible flag true.
featureSummaryTemplate | Template used to generate a short summary for a specific feature. Is in Angular template language and uses *feature* to refer to the feature. 
comboBox               | Indicates layer or group combo box functionality. It is an object, which attributes keys are layers ids and attributes are texts in the box.
comboBoxReverse        | Flag generating popupTop class to the combo box component (combo box slides up).
onLayerClick           | Callback executed on map click. `function(event, feature_data) {} }`. 

### Switch settings

Attribute     | Explanation
------------- | ---------------------
icon          | URL of icon to display, no other switch displayed if set
selectorStyle | Style of switch (fill, border, border-fill or class)
class         | String will be the class of selector element (you can define class in your app)
fillColor     |
borderColor   |
borderWidth   |
borderStyle   |


### WMS layer

Attribute     | Explanation
------------- | ---------------------
url           | URL of WMS layer
wms           | Options for [L.TileLayer.WMS](http://leafletjs.com/reference.html#tilelayer-wms)
filter        | Filtering options
animated      | Switch for zoom and pan animation (default: map setting)

### WFS layer

Attribute     | Explanation
------------- | ---------------------
url           | URL of WFS layer
geojson       | Options for [L.GeoJSON](http://leafletjs.com/reference.html#geojson)
wfs           | Parameters for WFS request (obligatory: **typeNames**)
filter        | Filtering options
format        | WFS format options (will be translated to **format** parameter)
geometryColumn| Name of column with geometry (default value: geom)
limit         | Maximum number of elements to be displayed at once (default 2000)


## Filtering options

The component translates *filter* structure to WFS filters. Currently supported filters are:

 * PropertyIsEqualTo
 * PropertyIsGreaterThan
 * And
 * Or
 * Not

 
 
 
# Changelog

## Branch hackathon-map-edit - not released yet

  * Completely refactored code base, split into three modules: cm.map, cm.leaflet, cm.layerSelector
  * Added cm.layerEditor module with editation functions
  * Made layer selector html template load from templateCache
  * Added sample project demonstrating usage of component

## 02/11/2015 Version 0.7.0

  * Added feature to remember whether group was collapsed and a number that show how many layers are visible in a collapsed group. 

## 30/10/2015 Version 0.6.9

  * Fixed loading layer tiles with default visibility set to true, but hidden after template for layer selector is asynchronously loaded and localStorage processed

## 30/10/2015 Version 0.6.8

  * Added 'hidden' group custom sorting function support 

## 14/10/2015 Version 0.6.7

  * Added onLayerClick layer configuration 

## 14/10/2015 Version 0.6.6

  * Added new switch selectorStyle option class

## 25/09/2015 Version 0.6.5

  * Added PropertyIsGreaterThan filter option
  * Added parsing array of arguments to "And" and "Or" filter options

## 07/07/2015 Version 0.6.4

  * Changed library location from lib/ to bower_lib/
  * Added bower dependencies

## 29/06/2015 Version 0.6.3

  * Added on map click layer priority
  * Added minZoomVisibility configuration

## 14/04/2015 Version 0.6.2

  * Source code clean up, now passes jshint

## 13/04/2015 Version 0.6.1

  * Added title tooltip option to layer selector component

## 13/04/2015 Version 0.6.0

  * Added support for Leaflet.draw control

## 24/03/2015 Version 0.5.6

  * Fixed bug when displaying popup on multiple features in WMS layer

## 02/03/2015 Version 0.5.5

  * Changed configuration of zoom levels in EPSG:5514, aligned with CMProxy configuration to speed up loading of cached content

## 05/02/2015 Version 0.5.4

  * Removed versions 0.5.3 and 0.5.2 - wrong paths in bower.json
  * Added comboBox switcher to layers and groups
  * Changed directory structure, source code is now under src/js
  * Started unit test implementation

## 08/01/2015 Version 0.5.1

  * Added ability to save layers visibility into local storage and load it with directive initialization

## 08/12/2014 Version 0.5

  * Service extended with functions to save/reset map position to sessionStorage and to broadcast map events
  * Directive broadcasts movend events
  * Layers with selector: 'none' are not displayed in selector

## 20/11/2014 Version 0.4.1

  * In WFS tile extended bounds for fetching data

## 18/11/2014 Version 0.4.0

  * Dynamic popups support several features from the same layer (requires feature_count to be set in WMS layer options) 
  * WFS filters support And, Or, Not operators
  * Group titles are now clickable
  * Fixed WFS layer filtering
  * Popup windows can display information about features in WFS layers

## 16/09/2014 Version 0.3.0

  * Renamed layer type *group* in configuration to *node*
  * Implemented popups

## 12/09/2014 Version 0.2.0

  * Implemented vector layer using WFS with GeoJSON
  * Implemented basic WFS-based filtering for both WMS and WFS
  * Modified configuration structure - moved layer options to wms, wfs, filter etc.
  * Reversed order in layer selector

## 01/09/2014 Version 0.1.0

  * Implemented layer selector
  * Implemented layer groups and loading from configuration
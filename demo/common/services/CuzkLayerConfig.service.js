/**
 * Created by ondrejzvara on 21.1.16.
 */

angular
    .module('app')
    .factory('CuzkLayerConfig', CuzkLayerConfig);

CuzkLayerConfig.$inject = ['LayerConfig', 'Map'];

function CuzkLayerConfig(LayerConfig, Map) {
    return {
        createOrtofotoLayer: createOrtofotoLayer
    };

    function getCuzkAttribution() {
        return 'ČÚZK "http://www.cuzk.cz"';
    }

    function getCuzkWmsOptions(layerName) {
        return {
            layers: layerName,
            format: 'image/jpeg',
            transparent: false,
            attribution: getCuzkAttribution(),
            unloadInvisibleTiles: true,
            crs: Map.getCRS(4326)
        };
    }

    function createOrtofotoLayer(id) {
        var layer = LayerConfig.createMapProxyWmsLayer(id);

        layer.title = 'Ortofoto ČÚZK';
        layer.wms = getCuzkWmsOptions('GR_ORTFOTORGB');

        return layer;
    }
}


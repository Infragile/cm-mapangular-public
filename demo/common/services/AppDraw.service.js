/**
 * Created by ondrejzvara on 12.5.16.
 */


angular
    .module('app')
    .factory('AppDraw', AppDraw);

AppDraw.$inject = ['CmDraw'];

function AppDraw(CmDraw) {

    var poly;

    return {
        startLineDraw: startLineDraw,
        saveLineDraw: saveLineDraw,
        cancelLineDraw: cancelLineDraw,
        startPolygonDraw: startPolygonDraw,
        stopPolygonDraw: stopPolygonDraw
    };

    function startLineDraw(mId) {
        CmDraw.startPolyline(mId).then(function(data){
            poly = data;
        });
    }

    function saveLineDraw() {
        CmDraw.savePolyline(poly);
    }

    function cancelLineDraw(mapId){
        CmDraw.removePolyline(mapId, poly);
    }

    function startPolygonDraw(mapId){
        CmDraw.startPolygonDraw(mapId);
    }
    
    function stopPolygonDraw(mapId){
        CmDraw.savePolygonDraw(mapId);
    }
}
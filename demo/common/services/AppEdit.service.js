/**
 * Created by ondrejzvara on 11.5.16.
 */


angular
    .module('app')
    .factory('AppEdit', AppEdit);

AppEdit.$inject = ['CmEvent', 'CmEdit', 'AppEditUtils'];

function AppEdit(CmEvent, CmEdit, AppEditUtils) {

    var mapId = null;
    var editLayer = null;
    var backups = [];

    return {
        enableEdit: enableEdit,
        disableAllEdits: disableAllEdits,
        resetAllEdits: resetAllEdits,
        cancelAllEdits: cancelAllEdits
    };

    function enableEdit(mId, layerId){
        mapId = mId;
        CmEvent.queryLayer(mapId, layerId, dataLoadCallback);
    }

    function dataLoadCallback(data, evt) {

        var geojson = data.data;
        if(geojson && geojson.features.length && AppEditUtils.inCollection(backups, geojson.features[0])){

            CmEdit.enable(mapId, geojson).then(function(){
                backups.push(geojson);
            });

        } else {
            console.warn('Already editing.');
        }

    }

    function disableAllEdits(mapId) {

        for(var i = 0; i < backups.length; i++){

            CmEdit.disable(mapId, backups[i]).then(function (geojson) {
                console.log(geojson)
            });

        }

        CmEvent.removeClickEvent(mapId);
        backups = [];
    }
    
    function resetAllEdits(mapId){
        
        for(var i = 0; i < backups.length; i++){
            CmEdit.reset(mapId, backups[i])
        }
        
    }
    
    function cancelAllEdits(mapId){
       
        for(var i = 0; i < backups.length; i++){
            CmEdit.cancel(mapId, backups[i])
        }

        CmEvent.removeClickEvent(mapId);
        
    }
    
}
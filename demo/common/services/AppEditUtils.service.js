/**
 * Created by ondrejzvara on 11.5.16.
 */


angular
    .module('app')
    .factory('AppEditUtils', AppEditUtils);

AppEditUtils.$inject = [];

function AppEditUtils() {

    return {
        inCollection: inCollection,
        toLeafletLayer: toLeafletLayer,
        getFeature: getFeature
    };

/////////////////////////

    function inCollection(srcArr, obj){
        return _.isEmpty(_.find(srcArr, function(o){ return o.features[0].properties.id === obj.properties.id }));
    }

    function toLeafletLayer(geoJson){
        return L.GeoJSON.geometryToLayer(geoJson);
    }

    function getFeature(obj){
        return obj._layers ? obj._layers[Object.keys(obj._layers)[0]] : obj;
    }

}
/**
 * Created by ondrejzvara on 21.1.16.
 */

angular
    .module('app')
    .factory('App', App);

App.$inject = [];

function App() {
    return {
        // getMainMapConfig: getMainMapConfig,
        getDefaultMapOptions: getDefaultMapOptions
    };

    ////////////////////////////////////////////////////////////

  /*  function getMainMapConfig() {
        var mainNode = LayerConfig.createNode('main-map');
        mainNode.selector = 'hidden';
        mainNode.topLevel = true;

        var legendNode = LayerConfig.createNode('legend');
        legendNode.title = 'Legenda';

        mainNode.addChildLayer(legendNode);

        var cuzkOrtofotoLayer = CuzkLayerConfig.createOrtofotoLayer('cuzk-ortofoto');
        cuzkOrtofotoLayer.selector = 'switch';
        cuzkOrtofotoLayer.selectorStyle = 'fill';
        cuzkOrtofotoLayer.fillColor = 'blue';
        cuzkOrtofotoLayer.editable = false;

        legendNode.addChildLayer(cuzkOrtofotoLayer);

        return mainNode;
    }
*/
    function getDefaultMapOptions() {
        return {
            srid: 5514,
            center: [50.05, 12.4],
            bounds: null,
            zoom: 7,
            minZoom: 2,
            maxZoom: 14,
            zoomControl: false,
            editable: true,
            editOptions: {
                lineGuideOptions : {
                    clickable: false,
                    color: '#ABE67E',
                    weight: 2,
                    opacity: 0.7,
                    fillColor: '#ABE67E',
                    fillOpacity: 0.5,
                    lineCap: 'round',
                    lineJoin: 'round'
                }
            },
            fullscreen: true,
            popup: 'dynamic'
        };
    }
}



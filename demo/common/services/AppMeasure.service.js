/**
 * Created by ondrejzvara on 16.5.16.
 */

angular
    .module('app')
    .factory('AppMeasure', AppMeasure);

AppMeasure.$inject = ['CmEvent', 'CmEdit', 'CmMeasure', 'AppEditUtils'];

function AppMeasure(CmEvent, CmEdit, CmMeasure, AppEditUtils) {

    var backups = [];

    return {
        startLineMeasure: startLineMeasure,
        stopLineMeasure: stopLineMeasure,
        startPolygonMeasure: startPolygonMeasure,
        stopPolygonMeasure: stopPolygonMeasure
    };

    //////////////////////////////////////////

    function startLineMeasure(mapId, callback) {
        return CmMeasure.startLineMeasure(mapId, callback);
    }

    function stopLineMeasure(mapId){
        return CmMeasure.stopLineMeasure(mapId);
    }

    function startPolygonMeasure(mapId, layerId){

        CmEvent.queryLayer(mapId, layerId, dataLoadCallback);

    }

    function dataLoadCallback(data, evt){

        var geojson = data.data;
        if(geojson && geojson.features.length && AppEditUtils.inCollection(backups, geojson.features[0])){

            CmEdit.enableEdit(mapId, geojson).then(function(){
                backups.push(geojson);
            });

        } else {
            console.warn('Already editing.');
        }

    }

    function stopPolygonMeasure(mapId){

    }

}
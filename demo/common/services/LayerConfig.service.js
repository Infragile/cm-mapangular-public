/**
 * Created by ondrejzvara on 21.1.16.
 */
/**
 * Created by brasko on 27.8.2015.
 */

angular
    .module('app')
    .factory('LayerConfig', LayerConfig);

LayerConfig.$inject = ['CM_MAP_LAYER_TYPE'];

function LayerConfig(CM_MAP_LAYER_TYPE) {
    return {
        createNode: createNode,
        createWmsLayer: createWmsLayer,
        createWfsLayer: createWfsLayer,
        createMapProxyWmsLayer: createMapProxyWmsLayer,
        createGeoserverWmsLayer: createGeoserverWmsLayer,
        getFilledPolygonSldBody: getFilledPolygonSldBody
    };

    /**
     * Return empty node object with a convenience addChildLayer function
     * which will allow us to build configuration tree in a more object-oriented way
     * @param {type} id
     * @returns {undefined}
     */
    function createNode(id) {
        return {
            id: id,
            type: CM_MAP_LAYER_TYPE.node,
            path: [],
            layers: [],
            topLevel: false,
            visible: true,
            addChildLayer: function (layer) {
                layer.path = this.path.concat(this.id);
                layer.parent = this;
                this.layers.push(layer);
            }
        };
    }

    /**
     * Get empty wms layer with given id
     * @param {type} id
     * @returns {undefined}
     */
    function createWmsLayer(id) {
        return {
            id: id,
            type: CM_MAP_LAYER_TYPE.wms,
            visible: true,
            addFilter: function (filter) {
                if (!this.filter) {
                    this.filter = filter;
                }
                else {
                    this.filter = {
                        type: 'And',
                        arg1: this.filter,
                        arg2: filter
                    };
                }
            }
        };
    }

    /**
     * Get empty wms layer with given id
     * @param {type} id
     * @returns {undefined}
     */
    function createWfsLayer(id) {
        return {
            id: id,
            type: CM_MAP_LAYER_TYPE.wfs,
            visible: true,
            addFilter: function (filter) {
                if (!this.filter) {
                    this.filter = filter;
                }
                else {
                    this.filter = {
                        type: 'And',
                        arg1: this.filter,
                        arg2: filter
                    };
                }
            }
        };
    }

    /**
     * Get empty wms layer from map proxy
     * @param {type} id
     * @returns {undefined}
     */
    function createMapProxyWmsLayer(id) {
        var layer = createWmsLayer(id);
        layer.url = 'http://geoportal.cuzk.cz/WMS_ORTOFOTO_PUB/WMService.aspx';

        return layer;
    }

    function createGeoserverWmsLayer(id) {
        var layer = createWmsLayer(id);
        layer.url = "https://devel.cleverassets.cz/geoserver/wms"

        return layer;
    }

    /*function createGeoserverWfsLayer(id) {
        var layer = createWfsLayer(id);
        layer.url = BACKEND_OPTIONS.geoserverUrl + '/wfs';

        return layer;
    }*/

    function getFilledPolygonSldBody(layerName, fillColor) {
        var sldBody = '<?xml version="1.0" encoding="UTF-8"?>' +
            '<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0">' +
            '<NamedLayer>' +
            '<Name>{{layerName}}</Name>' +
            '<UserStyle>' +
            '<IsDefault>1</IsDefault>' +
            '<FeatureTypeStyle>' +
            '<Rule>' +
            '<PolygonSymbolizer>' +
            '<Fill>' +
            '<CssParameter name="fill">{{fillColor}}</CssParameter>' +
            '</Fill>' +
            '<Stroke>' +
            '<CssParameter name="stroke">#000000</CssParameter>' +
            '<CssParameter name="stroke-width">0.5</CssParameter>' +
            '</Stroke>' +
            '</PolygonSymbolizer>' +
            '</Rule>' +
            '</FeatureTypeStyle>' +
            '</UserStyle>' +
            '</NamedLayer>' +
            '</StyledLayerDescriptor>';

        return sldBody.replace('{{layerName}}', layerName).replace('{{fillColor}}', fillColor);
    }
}



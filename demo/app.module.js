/**
 * Created on 13.11.15.
 */

angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    'cm.map',
    'cm.event',
    'cm.edit',
    'cm.draw',
    'cm.navigate',
    'cm.storage',
    'cm.measure',
    'cm.directives'
]).config(config);

function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: "/home",
            views: {
                app: {
                    controller: "HomeController as HomeCtrl",
                    templateUrl: "components/home/home.html"
                }
            }
        })
        .state('map', {
            url: "/map",
            views: {
                app: {
                    controller: "MapController as MapCtrl",
                    templateUrl: "components/map/map.html"
                }
            }
        });
    
}

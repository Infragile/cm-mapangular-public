/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('app')
    .controller('HomeController', HomeController);

HomeController.$inject = ['App'];

function HomeController(App) {
    var vm = this;
    
    vm.mapId = 'home-map';
    
    vm.config = [
        {
            id: 'map-map',
            type: 'layer',
            layer: {
                type: 'wms',
                url: "https://devel.cleverassets.cz/mapproxy/service",
                wms: {
                    layers: 'cmp1_CUZK_Ortofoto',
                    format: 'image/png',
                    transparent: true,
                    attribution: 'ČÚZK "http://www.cuzk.cz"',
                    unloadInvisibleTiles: true
                }
            }
        }
    ];


    activate();

    function activate() {

        vm.opts = App.getDefaultMapOptions();
        vm.opts.positionListener = function(lat, lng, zoom) { console.log(lat, lng, zoom); }
        vm.opts.id = 'home-map';

        var bounds = {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        -894068.8,
                        -1028711.4
                    ],
                    [
                        -894068.8,
                        -1011506.4
                    ],
                    [
                        -876387.3093,
                        -1011506.4
                    ],
                    [
                        -876387.3093,
                        -1028711.4
                    ],
                    [
                        -894068.8,
                        -1028711.4
                    ]
                ]
            ],
            "crs": {
                "type": "name",
                "properties": {
                    "name": "urn:ogc:def:crs:EPSG::" + vm.opts.srid
                }
            }
        };
        
        var geoJsonLayer = new L.Proj.geoJson();
        geoJsonLayer.addData(bounds);
        // vm.opts.bounds = geoJsonLayer.getBounds();

    }
}
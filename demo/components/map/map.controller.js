/**
 * Created by ondrejzvara on 10.5.16.
 */

angular
    .module('app')
    .controller('MapController', MapController);

MapController.$inject = [
    'App', 'AppEdit', 'AppDraw', 'AppMeasure', '$scope', '$filter', 'CmEvent', 
    'CmLayer', '$compile', '$rootScope', '$templateCache'
];

function MapController(
        App, AppEdit, AppDraw, AppMeasure, $scope, $filter, CmEvent, 
        CmLayer, $compile, $rootScope, $templateCache
) {
    var vm = this;

    vm.lineLength = 0;
    vm.mapId = 'map-map';

    var layers = [
        {
            id: 'map-map',
            type: 'layer',
            layer: {
                type: 'wms',
                url: "https://devel.cleverassets.cz/mapproxy/service",
                wms: {
                    layers: 'cmp1_CUZK_Ortofoto',
                    format: 'image/jpeg',
                    transparent: false,
                    attribution: 'ČÚZK "http://www.cuzk.cz"',
                    unloadInvisibleTiles: true,
                    zIndex: 10
                }
            }
        },
        {
            id: 'LPIS',
            type: 'layer',
            layer: {
                type: 'wms',
                url: "http://cf-geo.dev.aws.clevermaps.cz:9030/geoserver/wms",
                editable: true,
                wms: {
                    layers: 'farm_cust_Agrofert_parcel',
                    format: 'image/png',
                    transparent: true,
                    unloadInvisibleTiles: true,
                    zIndex: 20
                }
            }
        }
    ];
    vm.config = [
        {
            id: 'legend',
            type: 'layerGroup',
            layers: layers
        }
    ];
    
    vm.enebleLayerAdd = false;

    vm.enableFeatureExtraction = enableFeatureExtraction;
    vm.disableFeatureExtraction = disableFeatureExtraction;
    
    vm.enableEdit = enableEdit;
    vm.disableEdit = disableEdit;
    vm.resetEdit = resetEdit;
    vm.cancelEdit = cancelEdit;

    vm.startLineDraw = startLineDraw;
    vm.saveLineDraw = saveLineDraw;
    vm.cancelLineDraw = cancelLineDraw;

    vm.startLineMeasure = startLineMeasure;
    vm.stopLineMeasure = stopLineMeasure;
    
    vm.addLayer = addLayer;
    vm.removeLayer = removeLayer;
    
    activate();

    function activate() {
        vm.opts = App.getDefaultMapOptions();
        vm.opts.id = 'map-map';
    }
    
    function addLayer() {
        vm.enebleLayerAdd = false;
        CmLayer.addLayerToMap('map-map', {
                type: 'wms',
                url: "https://devel.cleverassets.cz/mapproxy/service",
                wms: {
                    layers: 'cmp1_CUZK_Ortofoto',
                    format: 'image/jpeg',
                    transparent: false,
                    attribution: 'ČÚZK "http://www.cuzk.cz"',
                    unloadInvisibleTiles: true,
                    zIndex: 10
                }
            }, 'orto');
    }
    
    function removeLayer() {
        vm.enebleLayerAdd = true;
        CmLayer.removeLayerFromMap('map-map', 'orto');
    }

    function enableFeatureExtraction(){

        var onFeatureLoad = function(data, evt){

           /* var newScope = $rootScope.$new(true);

            newScope.geojson = data.data || {};
            var element = $compile($templateCache.get('demo/templates/popup.html'))(newScope);

            var popup = L.popup();
            popup
                .setLatLng(evt.latlng)
                .setContent(element[0])
                .openOn(evt.target);*/
            
            console.log(evt)
            console.log(data)

        };

        CmEvent.queryLayer(vm.opts.id, 'LPIS', onFeatureLoad);
    }
    
    function disableFeatureExtraction(){
        CmEvent.removeClickEvent(vm.config.id);
    }
    
    
    
    function enableEdit(){
        AppEdit.enableEdit(vm.config.id, 'LPIS');
    }

    function disableEdit(){
        AppEdit.disableAllEdits(vm.config.id);
    }

    function resetEdit(){
        AppEdit.resetAllEdits(vm.config.id);
    }
    
    function cancelEdit(){
        AppEdit.cancelAllEdits(vm.config.id);
    }
    
    
    
    function startLineDraw(){
        AppDraw.startLineDraw(vm.config.id);
    }

    function saveLineDraw(){
        AppDraw.saveLineDraw(vm.config.id);
    }
    
    function cancelLineDraw(){
        AppDraw.cancelLineDraw(vm.config.id);
    }
    
    function startLineMeasure(){
        AppMeasure.startLineMeasure(vm.config.id, lineMeasureCallback);
    }

    function stopLineMeasure(){
        AppMeasure.stopLineMeasure(vm.config.id);
    }
    
    function lineMeasureCallback(distance, unit){
        vm.lineLength = $filter('number')(distance, 3) + " " + unit.code;
        $scope.$apply();
    }

    
  
}

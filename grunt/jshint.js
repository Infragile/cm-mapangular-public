/**
 * Created by ondrejzvara on 7.6.16.
 */

module.exports = {
     options: {
          jshintrc: 'jshintrc'
     },
     all: {
          src: ['src/js/**/*.js']
     }
};

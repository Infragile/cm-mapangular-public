/**
 * Created by ondrejzvara on 7.6.16.
 */

module.exports = {
    diagram: {
        files: {
            'docs/graph': [
                'src/js/**/*.js'
            ]
        }
    }
};


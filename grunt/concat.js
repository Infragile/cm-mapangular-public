
module.exports = {
    dist: {
        src: [
            'src/js/**/*.module.js',
            'src/js/**/*.filter.js',
            'src/js/**/*.service.js',
            'src/js/**/*.directive.js',
            'src/js/**/*.controller.js'
        ],
        dest: 'dist/cm-mapangular.js'
    }
};

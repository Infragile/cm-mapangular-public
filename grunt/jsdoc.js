/**
 * Created by ondrejzvara on 9.6.16.
 */

module.exports = {
    dist: {
        src: ['src/js/**/*.js'],
        options: {
            destination : 'docs/docs',
            template : "node_modules/ink-docstrap/template",
            configure: "jsdoc.conf.json"
        }
    }
};
